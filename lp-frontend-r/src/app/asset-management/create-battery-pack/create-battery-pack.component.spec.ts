import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateBatteryPackComponent } from './create-battery-pack.component';

describe('CreateBatteryPackComponent', () => {
  let component: CreateBatteryPackComponent;
  let fixture: ComponentFixture<CreateBatteryPackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateBatteryPackComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateBatteryPackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
