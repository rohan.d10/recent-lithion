import {
  Component,
  OnInit,
  OnDestroy,
  EventEmitter,
  Output,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  ViewChild
} from '@angular/core';
import {
  AssetTable
} from '../../models';
import {
  Store
} from '@ngrx/store';
import * as models from '../../models';
import * as fromRoot from '../../store/reducers';
// import { ModalDirective, ModalContainerComponent } from 'ngx-bootstrap/modal';
import {
  NgbModal,
  ModalDismissReasons
} from '@ng-bootstrap/ng-bootstrap';

import {
  Subscription
} from 'rxjs/Subscription';
import {
  Http
} from '@angular/http';
import {
  Router
} from '@angular/router';
import {
  FetchAssetAction
} from '../../store/actions/asset-management';
import {
  ToasterService
} from 'angular2-toaster/angular2-toaster';
import {
  CheckTokenAction
} from '../../store/actions/token-management';
import {
  ServiceConstants,
  APIUrls
} from '../../constants';
import * as moment from 'moment';
import {
  Subject
} from 'rxjs';
import 'rxjs/add/operator/map';
import {
  DataTableDirective
} from 'angular-datatables';

@Component({
  selector: 'app-asset-table',
  templateUrl: './asset-table.component.html',
  styleUrls: ['./asset-table.component.scss', '../../../scss/vendors/toastr/toastr.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AssetTableComponent implements OnInit, OnDestroy {
  private __subscriptions__: Subscription[] = new Array < Subscription > ();
  public assets: AssetTable[];
  public currentAssets: models.AssetCurrentInfo[] = [];
  public require : any
  public backupAssets: models.AssetCurrentInfo[] = [];
  public isLoading: boolean;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject < any > = new Subject();

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  @Output() addAssetButtonClicked = new EventEmitter < string > ();

  assetvalue = 'not clicked';
  command = '';
  parms: any[];
  public id: string;
  public page = 1;
  mnumber = '';
  REASON_CANCEL = 'MODAL closed';
  REASON_SUCCESS = 'MODAL success';
  constructor(
    private http: Http,
    private route: Router,
    private toasterService: ToasterService,
    private modalService: NgbModal,
    private changeDetectorRef: ChangeDetectorRef,
    private store: Store < fromRoot.State >
  ) {}

  public sortingIndex = -1;
  public sortIdByAsc: number = 1;
  closeResult: string;
  current = Math.round(new Date().getTime() / 1000);

  public gettime(hms, ymd) {
    const m = moment(ymd + 'T' + hms).fromNow();
    return m;
  }

  public idAscIcon = 'fa';
  private setIdAscIcon() {
    if (this.sortIdByAsc == 1) {
      this.idAscIcon = 'fa fa-caret-down';
    } else {
      this.idAscIcon = 'fa fa-caret-up';
    }
  }
  public sortAssetId() {
    if (this.sortingIndex != 1) {
      this.sortIdByAsc = 1;
    }
    this.sortingIndex = 1;
    this.sortIdByAsc = this.sortIdByAsc * -1;
    this.setIdAscIcon();
    this.currentAssets.sort((a, b) => {
      var nameA = a.assetId.toUpperCase(); // ignore upper and lowercase
      var nameB = b.assetId.toUpperCase(); // ignore upper and lowercase
      if (nameA < nameB) {
        return -1 * this.sortIdByAsc;
      }
      if (nameA > nameB) {
        return this.sortIdByAsc;
      }

      // names must be equal
      return 0;
    });
  }

  public filterBy(index: number) {
    this.sortingIndex = -1;
    switch (index) {
      case 1:
        this.currentAssets.length = 0;
        this.backupAssets.forEach(element => {
          if (element.status === '1') {
            this.currentAssets.push(element);
          }
        });
        break;
      case 2:
        this.currentAssets.length = 0;
        this.backupAssets.forEach(element => {
          if (element.status === '-1') {
            this.currentAssets.push(element);
          }
        });
        break;
      case 3:
        this.currentAssets.length = 0;
        this.backupAssets.forEach(element => {
          if (element.status === '0') {
            this.currentAssets.push(element);
          }
        });
        break;
      case 4:
        this.currentAssets.length = 0;
        this.backupAssets.forEach(element => {
          if (element.status === '2') {
            this.currentAssets.push(element);
          }
        });
        break;
      case 5:
        this.currentAssets.length = 0;
        this.backupAssets.forEach(element => {
          this.currentAssets.push(element);
        });
        break;
    }
  }
  public sortUpdatedAt() {
    if (this.sortingIndex != 2) {
      this.sortIdByAsc = 1;
    }
    this.sortingIndex = 2;
    this.sortIdByAsc = this.sortIdByAsc * -1;
    this.setIdAscIcon();
    this.currentAssets.sort((a, b) => {
      var updatedA = this.gettime(a.updatedAtTime, a.updatedAtDate);
      var updatedB = this.gettime(b.updatedAtTime, b.updatedAtDate);
      if (updatedA < updatedB) {
        return -1 * this.sortIdByAsc;
      }
      if (updatedA > updatedB) {
        return this.sortIdByAsc;
      }

      // names must be equal
      return 0;
    });
  }
  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
    };
    this.getDataFromStore();
    this.fetchData();
  }

  private getId() {
    this.id = this.route.url.substring(22);
  }

  private showSuccesToast(assetId, command) {
    this.toasterService.pop(
      'success',
      'Command sent',
      command + ' command for Asset ID: ' + assetId + ' sent successfully.'
    );
  }

  public showModalConfirmation(content, assetid, cmd, mNumber) {
    this.modalService.open(content).result.then(
      result => {
        this.closeResult = `${result}`;
        switch (this.closeResult) {
          case this.REASON_SUCCESS:
            this.open(content, assetid, cmd, mNumber);
            if (cmd === '1') {
              this.showSuccesToast(assetid, 'Stop');
            } else if (cmd === '2') {
              this.showSuccesToast(assetid, 'Start');
            }
            break;
          case this.REASON_CANCEL:
            break;
        }
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }
  private open(content, assetid, cmd, mNumber) {
    switch (cmd) {
      case '1':
        this.command = 'tsd';
        break;
      case '2':
        this.command = 'bmson';
        break;
      default:
        this.command = 'safe';
    }

    this.mnumber = mNumber;

    this.http.get(APIUrls.SEND_COMMAND + '?number=' + this.mnumber + '&command=' + this.command).subscribe(response => {
      this.parms = response.json();
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  public fetchData(flag = 0) {
    // this.popToast();
    this.sortingIndex = -1;

    if (flag == 1) {
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        // Destroy the table first
        dtInstance.destroy();
        // Call the dtTrigger to rerender again
        this.getDataFromStore();
      });
    } else {
      this.store.dispatch(new FetchAssetAction());
    }
  }

  public checkToken() {
    this.store.dispatch(new CheckTokenAction());
  }

   icon(p: string) {
    // if (p == '1') {
    //   return require('../../../img/red-pin.png');

    // }

    // if (p == '0') {
    //   return require('../../../img/yellow-pin.png');
    // }

    // if (p == '-1') {
    //   return require('../../../img/blue-pin.png');
    // }
  }



  private getDataFromStore() {
    /* this.__subscriptions__.push(
       this.store.select(fromRoot.getAssets).subscribe(assets => {
         this.assets = assets;
         this.changeDetectorRef.detectChanges();
       })
     );*/
    this.__subscriptions__.push(
      this.store.select(fromRoot.getCurrentAssets).subscribe(currAssets => {
        this.currentAssets = currAssets;
        this.dtTrigger.next();

        this.backupAssets.length = 0;
        this.currentAssets.forEach(element => {
          if (element.lat != '0' || element.lon != '0') {
            element.lat = parseFloat(String(element.lat));
            element.lon = parseFloat(String(element.lon));
          }
          else {
            element.lat = null;
            element.lon = null;
          }
          element.capUsed = Math.abs(element.capUsed);
          element.markerLabel = {
            fontFamily: 'arial',
            fontWeight: 'bold',
            fontSize: '10',
            color: '#000000',
            text: element.assetId
          };
          this.backupAssets.push(element);
        });

        this.changeDetectorRef.detectChanges();
      })
    );
    this.__subscriptions__.push(
      this.store.select(fromRoot.getIsLoading).subscribe(isLoading => {
        this.isLoading = isLoading;
        this.changeDetectorRef.detectChanges();
      })
    );

  }

  /**
   * Cleanup all the subscriptions when component is destroyed.
   */

  ngOnDestroy() {
    this.__subscriptions__.forEach(subscription => subscription.unsubscribe());
    this.dtTrigger.unsubscribe();
  }
}
