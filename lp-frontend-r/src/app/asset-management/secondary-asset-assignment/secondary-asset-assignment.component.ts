import { Component, OnInit, OnDestroy,ViewChild } from '@angular/core';
import { FormControl,FormGroup,Validators,AbstractControl,ValidatorFn } from '@angular/forms';
import { CommonService } from '../../services/common.service';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import Swal from 'sweetalert2';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-secondary-asset-assignment',
  templateUrl: './secondary-asset-assignment.component.html',
  styleUrls: ['./secondary-asset-assignment.component.css']
})
export class SecondaryAssetAssignmentComponent implements OnInit {
  secAssignForm : FormGroup;
  lspList    = [];
  batteryList = [];
 
  public dropdownList:any = [];
  public dropdownSettings = {};
  public selectedItems    = [];

  constructor(private common: CommonService,private spinnerService: Ng4LoadingSpinnerService) { }

  ngOnInit() {
    this.partnerGivenBatteries();
    this.getLsps();
    this.initializeForm();
  }

   initializeForm(){
    this.dropDownSettings();
    this.selectedItems = [];
    this.secAssignForm = new FormGroup({
      'from'    : new FormControl('L9999499864',Validators.required),
      'toLsp'   : new FormControl('-1',[Validators.required,Validators.min(0)]),
      'battery' : new FormControl([],[Validators.required,Validators.min(1)]) 
    });
  }

  dropDownSettings(){
    this.dropdownSettings = { 
                              singleSelection    : false, 
                              text               : "Select Batteries",
                              selectAllText      : 'Select All',
                              unSelectAllText    : 'UnSelect All',
                              enableSearchFilter : true,
                              classes            : "myclass custom-class",
                              primaryKey         : "batteryPackId",
                              labelKey           : "batteryPackId",
                            }; 
                          
  }

  public partnerGivenBatteries() {
    this.common.partnerGivenBatteries({partnerAssignee:"L9999499864"}).subscribe(data => {
      if(data.response_code == 200){
        this.dropdownList = data.response;
      }
    });
  }

  public getLsps() {
    this.common.getLspData().subscribe(data => {
      if(data.response_code == 200){
        this.lspList = data.response;
      }
    });
  }

   resetForm(){
    this.secAssignForm.reset();
    this.initializeForm();
  }

    onSubmit(){
    if(this.secAssignForm.valid){
      this.spinnerService.show();
     
      this.common.batterySecondaryAssignment(this.secAssignForm.value).subscribe((data)=> {
          if(data.response_code == 200){
            this.spinnerService.hide();
            Swal('Success!',data.response,'success');  
            this.resetForm();
          }else{
            this.spinnerService.hide();
            Swal('Failure!',data.response,'error');
          }
      });
    }
  }

}
