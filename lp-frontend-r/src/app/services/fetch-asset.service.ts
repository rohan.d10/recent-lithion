import { Injectable } from '@angular/core';
import * as models from '../models';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { tap } from 'rxjs/operators';

import * as constants from '../constants';
import { APIUrls } from '../constants';

@Injectable()
export class ApiService {
  private url = APIUrls.FETCH_CURRENT_ASSETS;
  private batteriesUrl = APIUrls.BATTERY_STATUS_LIST;

  private batteryUrl = APIUrls.FETCH_BATTERY_DETAILS;
  constructor(private http: HttpClient) {}

  public getCurrentAssets(): Observable<models.AssetCurrentResponse[]> {
    return this.http.get<models.AssetCurrentResponse[]>(this.url);
  }

  public getBatteries(): Observable<models.BatteryResponse> {
    return this.http.get<models.BatteryResponse>(this.batteriesUrl);
  }
  // todo
  public getBattery(param: string): Observable<models.SingleBatteryResponse> {
    return this.http.get<models.SingleBatteryResponse>(this.batteryUrl + '?batteryPackId=' + param);
  }

  public logError() {
    console.log('error occurred');
  }
}
