import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CreateAsset } from './../models/create-Asset.model';
import * as constants from '../constants';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { tap } from 'rxjs/operators';
import { RegDataloggerResponse, Datalogger } from '../models';
import { APIUrls } from '../constants';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    Authorization: 'my-auth-token'
  })
};

@Injectable()
export class DataLoggerCreateService {
  constructor(private _http: HttpClient) {}

  private regUrl = APIUrls.REGISTER_DATALOGGER;

  public createDataLogger(DL: Datalogger): Observable<RegDataloggerResponse> {
    return this._http.post<RegDataloggerResponse>(this.regUrl, DL, httpOptions);
  }
}
