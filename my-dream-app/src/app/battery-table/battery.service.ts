import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import {BatteryTableComponent} from './battery-table.component'

@Injectable({
  providedIn: 'root'
})
export class BatteryService{

  constructor(private http: HttpClient,private route : ActivatedRoute) { }


  getData(p): Observable<any> {
    let batteryUrl = 'http://api.lithion.in/api/getdata?dlid='+ p +'&limit=10';
    return this.http.get(batteryUrl);
   }

}
