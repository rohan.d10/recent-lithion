import { Component, OnInit } from '@angular/core';
import { FormControl,FormGroup,Validators, } from '@angular/forms';
import * as models from '../../../models';
import { CommonService } from '../../../services/common.service';
import Swal from 'sweetalert2';
import { AuthenticationService } from '../../../services/authentication.service';

@Component({
  selector: 'app-lsp-report-issue',
  templateUrl: './lsp-report-issue.component.html',
  styleUrls: ['./lsp-report-issue.component.scss']
})
export class LspReportIssueComponent implements OnInit {
  issueForm: FormGroup;
  public formData = {};
  public assetTypes = ['Battery'];
  public issueTypes = ['SOC Indicator Problem','Failed to Start Battery','Charger is Not Working',
                          'Connector Issue','Physical Tampering of Battery','Others'];
  public assetNames = {};                        
  public lspId = '';

  constructor(private common: CommonService,private authService: AuthenticationService) {
    this.lspId = this.authService.getUserId();
  }

  ngOnInit() {
    this.initializeForm();
  }

   resetForm(){
    this.issueForm.reset();
    this.initializeForm();
  }

    initializeForm(){
     this.issueForm = new FormGroup({
        'assetType'   : new FormControl('-1', [Validators.required, Validators.min(0)]),
        'assetId'     : new FormControl('-1', [Validators.required,Validators.min(0)]),
        'issueType'   : new FormControl('-1', [Validators.required,Validators.min(0)]),
        'description' : new FormControl('',)
      });
  }

  selectedOption(assetType){
    if(assetType == 'Battery'){
      this.common.lspAssignedBatteries({"lspId":this.lspId}).subscribe((data) => {
          if(data.response_code == 200){
              this.assetNames = data.response;
          }
        });
    }
  }

  onSubmit(){
     if(this.issueForm.valid) {
        this.formData = {
          "assetId"       : this.issueForm.get('assetId').value,
          "assetType"     : this.issueForm.get('assetType').value,
          "description"   : this.issueForm.get('description').value,
          "issueType"     : this.issueForm.get('issueType').value,
          "lspId"         : this.lspId
      }

    this.common.reportIssueForm(this.formData).subscribe((data) => {
          if(data.response_code == 200){
              Swal(
                'Success!',
                data.response,
                'success'
              );  
              this.resetForm();
          }
          else{
            Swal(
                'Failure!',
                data.response,
                'error'
              );
          }
        });

  }
}

}
