import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MyLoginComponent } from './my-login/my-login.component';
import { Routes, RouterModule, ActivatedRoute } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { FormComponent } from './form/form.component';
import {
  ReactiveFormsModule,
  FormBuilder,
  FormsModule,
  FormGroup
} from '@angular/forms';
import { TestComponent } from './test/test.component';
import { MatToolComponent } from './mat-tool/mat-tool.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import {
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule,
  MatRadioModule,
  MatIconModule,
  MatTableModule,
  MatPaginatorModule,
  MatSortModule,
  MatCheckboxModule,
  MatCardModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatToolbarModule
} from '@angular/material';
import { MatTableComponent } from './mat-table/mat-table.component';
import { HttpClientModule } from '@angular/common/http';
import { MatTableService } from './mat-table/mat-table.service';
import { ApiComponent } from './api/api.component';
import { ApiService } from './api/api.service';
import { GoogleMapsComponent } from './google-maps/google-maps.component';
import { AgmCoreModule } from '@agm/core';
import { MapDataComponent } from './map-data/map-data.component';
import { MapDataService } from './map-data/map-data.service';
import { ReactFormComponent } from './react-form/react-form.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { VehicleRegistrationComponent } from './vehicle-registration/vehicle-registration.component';
import { AssetStatusComponent } from './asset-status/asset-status.component';
import { AssetService } from './asset-status/asset.service';
import { BatteryTableComponent } from './battery-table/battery-table.component';

const appRoutes: Routes = [
 // { path: 'home', component: HomeComponent },
  { path: '', component: AssetStatusComponent },
 // { path: 'form', component: FormComponent },
  {path: 'battery/:assetId', component: BatteryTableComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    MyLoginComponent,
    HomeComponent,
    FormComponent,
    TestComponent,
    MatToolComponent,
    MatTableComponent,
    ApiComponent,
    GoogleMapsComponent,
    MapDataComponent,
    ReactFormComponent,
    VehicleRegistrationComponent,
    AssetStatusComponent,
    BatteryTableComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes),
    MatFormFieldModule,
    MatInputModule,
    BrowserAnimationsModule,
    BrowserModule,
    BrowserAnimationsModule,
    MatSelectModule,
    HttpClientModule,
    FormsModule,
    MatRadioModule,
    MatIconModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatCardModule,
    MatCheckboxModule,
    HttpClientModule,
    MatDatepickerModule,
    MatToolbarModule,
    MatNativeDateModule,
    NgbModule.forRoot(),
    // BsDatepickerModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDd91n_BIQk7CBMOGq3rgW_lLXIl_LdxBQ'
    })
  ],
  providers: [MatTableService, ApiService, MapDataService, AssetService],
  bootstrap: [AppComponent]
})
export class AppModule {}
