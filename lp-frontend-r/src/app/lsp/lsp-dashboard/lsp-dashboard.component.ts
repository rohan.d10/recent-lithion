import { Component, OnInit, ChangeDetectorRef,ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ToasterService } from 'angular2-toaster';
import { Store } from '@ngrx/store';
import * as fromRoot from '../../store/reducers';
import { LspFetchService, AuthenticationService, CommonService } from '../../services';
import { LspDetails } from '../../models';
import { DataTableDirective } from 'angular-datatables';

@Component({
  selector: 'app-lsp-dashboard',
  templateUrl: './lsp-dashboard.component.html',
  styleUrls: ['./lsp-dashboard.component.scss']
})
export class LspDashboardComponent implements OnInit {
  loadedfeature                   = '';
  addAssetView                    = false;
  private lspId: string           = null;
  public url: string;
  parameters: any;
  reviewAssetCreatedTable         = false;
  public lspDetails: LspDetails[] = [];
  public totalAssets              = 0;
  public assignedAssets           = 0;
  public chargingAssets           = 0;
  public idleAssets               = 0;

  constructor(
    private route             : Router,
    private lspDetailsService : LspFetchService,
    private authService       : AuthenticationService,
    private common            : CommonService
  ) {
    this.lookForUrl();
    /*if (this.authService.isLoggedOut() || this.authService.getUserType() != 'Lsp') {
      this.authService.checkAndRedirect();
    }*/
    this.lspId = this.authService.getUserId();

    this.fetchLspDetails();
  }

  ngOnInit() {}

  private fetchLspDetails() {
    this.totalAssets    = 0;
    this.assignedAssets = 0;
    this.chargingAssets = 0;
    this.idleAssets     = 0;

    this.common.lspListedAssets({"lspId":this.lspId}).subscribe(response => {
      if (response.response_code === 200) {
        this.lspDetails = response.response;
        this.totalAssets = this.lspDetails.length;
        this.lspDetails.forEach(element => {
          if (element.actorId === this.lspId) {
            switch (element.state) {
              case '0':
                this.idleAssets = this.idleAssets + 1;
                break;
              case '1':
                this.chargingAssets = this.chargingAssets + 1;
                break;
            }
          } else {
            this.assignedAssets = this.assignedAssets + 1;
          }
        });
      }
    });
  }
  onNavigate(feature: string) {
    this.loadedfeature = feature;
    this.addAssetView = true;
  }

  lookForUrl() {
    this.url = this.route.url;
  }

  onReviewButtonClicked(event) {}
}
