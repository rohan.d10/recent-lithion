import { Component, OnInit, ViewChild } from '@angular/core';
import {
  FormControl,
  FormGroup,
  Validators,
  AbstractControl,
  ValidatorFn
} from '@angular/forms';
import * as models from '../../models';
import { CommonService } from '../../services/common.service';
import Swal from 'sweetalert2';
import * as moment from 'moment';
import { BsDatepickerDirective } from 'ngx-bootstrap/datepicker';

@Component({
  selector   : 'app-register-lsp',
  templateUrl: './register-lsp.component.html',
  styleUrls  : ['./register-lsp.component.scss']
})
export class RegisterLspComponent implements OnInit {
  registerLspForm: FormGroup;

  radioOptions    = [{ label: 'Yes', value: 'true' },{ label: 'No', value: 'false' }];
  transOptions    = [{ label: 'CASH', value: 'Cash' },{ label: 'CREDIT', value: 'Credit' },
                    { label: 'DEBIT', value: 'Debit' }];
  public formData = {};
  public maxDate  = new Date();
  @ViewChild(BsDatepickerDirective) datepicker: BsDatepickerDirective;
  constructor(private common: CommonService) {}

  ngOnInit() {
    this.initializeForm();
  }

  initializeForm() {
    this.registerLspForm = new FormGroup({
      name            : new FormControl('', [Validators.required]),
      partnerName     : new FormControl('', [Validators.required]),
      address         : new FormControl('', [Validators.required]),
      phone           : new FormControl('', [Validators.required,Validators.pattern('^[0-9]*$'),this.exactLength(10)]),
      gst             : new FormControl(''),
      accNum          : new FormControl('', [Validators.required]),
      bankName        : new FormControl('', [Validators.required]),
      bankAddress     : new FormControl('', [Validators.required]),
      ifsc            : new FormControl('', [Validators.required]),
      pan             : new FormControl('', [Validators.required,Validators.pattern('[A-Za-z]{5}[0-9]{4}[A-z]{1}')]),
      dateOfSetup     : new FormControl(),
      transactionType : new FormControl('-1', [Validators.required,Validators.min(0)]),
      totalArea       : new FormControl(''),
      sourceOfPow     : new FormControl(''),
      numOfBattery    : new FormControl('', Validators.pattern('^[0-9]*$')),
      numOfCharger    : new FormControl('', Validators.pattern('^[0-9]*$')),
      secDeposit      : new FormControl('', Validators.pattern('[0-9]+(.[0-9][0-9]?)?')),
      secDepCollected : new FormControl(''),
      depositedBy     : new FormControl(''),
      isPartialDep    : new FormControl(''),
      depAmount       : new FormControl('', Validators.pattern('[0-9]+(.[0-9][0-9]?)?')),
      emiTenure       : new FormControl('',Validators.pattern('[0-9]+(.[0-9][0-9]?)?')),
      emiAmount       : new FormControl('',Validators.pattern('[0-9]+(.[0-9][0-9]?)?')),
      noOfOperator    : new FormControl('', Validators.pattern('^[0-9]*$'))
    });
  }

  resetForm() {
    this.registerLspForm.reset();
    this.initializeForm();
  }

  onSubmit() {
    if (this.registerLspForm.valid) {
      if (this.registerLspForm.value.dateOfSetup) {
        this.registerLspForm.value.dateOfSetup = moment(this.registerLspForm.value.dateOfSetup).format('YYYY-MM-DD');
      }

      this.formData = this.registerLspForm.value;

      this.common.registerLsp(this.formData).subscribe(data => {
        if (data.response_code == 200) {
          Swal(data.data, data.response, 'success');
          this.resetForm();
        } else Swal('Failure!', data.response, 'error');
      });
    }
  }

  changeDateFormat(date) {
    var year  = date['year'];
    var month = date['month'];
    var day   = date['day'];

    if (month < 10) {
      month = '0' + month;
    }
    if (day < 10) {
      day = '0' + day;
    }

    return year + '-' + month + '-' + day;
  }

  exactLength(limit: Number): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } | null => {
      let phone = control.value;
      
      if (phone && phone.length!=limit) {
          return { 'exactLength': true };
      }    
      return null;
    };
  }
}
