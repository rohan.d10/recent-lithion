import { Component, OnInit } from '@angular/core';
import { FormControl,FormGroup,Validators, } from '@angular/forms';
import * as models from '../../models';
import { CommonService } from '../../services/common.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-register-cell',
  templateUrl: './register-cell.component.html',
  styleUrls: ['./register-cell.component.css']
})
export class RegisterCellComponent implements OnInit {
  registerCellForm: FormGroup;
  public formData = {};
  public vendors  = ['CALB'];
  
  constructor(private common: CommonService) {}

  ngOnInit() {
  	this.initializeForm();
  }

  initializeForm(){
     this.registerCellForm = new FormGroup({
		'internalResistance' : new FormControl('', Validators.required),
		'cellBarcode' 		   : new FormControl('', [Validators.required,Validators.minLength(15),
														Validators.maxLength(15)]),
		'ocv' 				       : new FormControl('', Validators.required),
		'capacity' 			     : new FormControl('', Validators.required),
		'vendor' 			       : new FormControl('-1', [Validators.required, Validators.min(0)]),
      });
  }

  resetForm(){
    this.registerCellForm.reset();
    this.initializeForm();
  }

  onSubmit(){
     if(this.registerCellForm.valid) {
        this.formData = this.registerCellForm.value;

        this.common.registerCellForm(this.formData).subscribe((data) => {
          if(data.response_code == 200){
              Swal(data.data,data.response,'success');  
              this.resetForm();
          }
          else{
            Swal('Failure!',data.response,'error');
          }
        });
      }
  }

}
