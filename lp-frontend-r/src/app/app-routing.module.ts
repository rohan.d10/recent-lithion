import { NgModule } from '@angular/core';
import { Route, Routes, RouterModule, PreloadingStrategy } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

export class CustomPreloadStrategy implements PreloadingStrategy {
  preload(route: Route, load: Function): Observable<any> {
    return route.data && route.data.preload ? load() : of(null);
  }
}

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: './login/login.module#LoginModule'
  },

  {
    path: 'register',
    data: {
      title: 'Register',
      data: { preload: true }
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'newasset'
      },
      {
        path: 'newasset',
        data: {
          title: 'Create Asset'
        },
        loadChildren: './dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'registerdriver',
        data: {
          title: 'Register Driver'
        },
        loadChildren: './dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'registerlsp',
        data: {
          title: 'Register LSP'
        },
        loadChildren: './dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'registerbatterypack',
        data: {
          title: 'Register Battery Pack'
        },
        loadChildren: './dashboard/dashboard.module#DashboardModule'
      },
      {
        path: '**',
        redirectTo: 'newasset',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: 'check',
    data: {
      title: 'Battery Checklist',
      data: { preload: true }
    },
    children: [
      {
        path: 'checklist',
        data: {
          title: 'Out Checklist'
        },
        loadChildren: './dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'receive-checklist',
        data: {
          title: 'In Checklist'
        },
        loadChildren: './dashboard/dashboard.module#DashboardModule'
      }
    ]
  },

  {
    path: 'edit',
    data: {
      title: 'Edit',
      data: { preload: true }
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'editbatterypack'
      },
      {
        path: 'editbatterypack',
        data: {
          title: 'Edit Battery Pack'
        },
        loadChildren: './dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'editdatalogger',
        data: {
          title: 'Edit Data Logger'
        },
        loadChildren: './dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'editbms',
        data: {
          title: 'Edit Bms'
        },
        loadChildren: './dashboard/dashboard.module#DashboardModule'
      },
      {
        path: '**',
        redirectTo: 'newasset',
        pathMatch: 'full'
      }
    ]
  },

  {
    path: 'driver-profile',
    data : {
      title: 'Driver Profile',
    },
    loadChildren: './dashboard/dashboard.module#DashboardModule'
  },
   {
    path: 'payment',
    data: {
      title: 'Driver Payments',
      data: { preload: true }
    },
    children: [
      {
        path: 'add',
        data: {
          title: 'Add New Payment'
        },
        loadChildren: './dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'view',
        data: {
          title: 'View Payments'
        },
        loadChildren: './dashboard/dashboard.module#DashboardModule'
      }
    ]
  },
  {
    path: 'user-registration',
    data : {
      title: 'User Registration',
    },
    loadChildren: './dashboard/dashboard.module#DashboardModule'
  },
  {
    path: 'cell-registration',
    data : {
      title: 'Cell Registration',
    },
    loadChildren: './dashboard/dashboard.module#DashboardModule'
  },
  {
    path: 'charger-registration',
    data : {
      title: 'Charger Registration',
    },
    loadChildren: './dashboard/dashboard.module#DashboardModule'
  },
  {
    path: 'earning-feedback',
    data : {
      title: 'Earning Feedback',
    },
    loadChildren: './dashboard/dashboard.module#DashboardModule'
  },
  {
    path: 'asset-status',
    data : {
      title: 'Battery Status',
    },
    loadChildren: './dashboard/dashboard.module#DashboardModule'
  },
  {
    path: 'customer',
    data: {
      title: 'Customer Interaction',
      data: {preload: true}
    },
    children: [
      {
        path: 'customer-interaction',
        data : {
          title: 'Add',
        },
        loadChildren: './dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'view-customer-interaction',
        data : {
          title: 'View',
        },
        loadChildren: './dashboard/dashboard.module#DashboardModule'
      }
    ]
  },
  {
    path: 'datalogger',
    data: {
      title: 'Data-Logger',
      data: { preload: true }
    },
    loadChildren: './dashboard/dashboard.module#DashboardModule'
  },

  {
    path: 'view',
    data: {
      title: 'View Checklist',
      data: { preload: true }
    },
    children: [
      {
        path: 'sendingOut',
        data: {
          title: 'View Sending Out Checklist'
        },
        loadChildren: './dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'receivingIn',
        data: {
          title: 'View Receiving In Checklist'
        },
        loadChildren: './dashboard/dashboard.module#DashboardModule'
      }
    ]
  },

  {
    path: 'assignment',
    data: {
      title: 'Battery Allocation',
      data: { preload: true }
    },
    children: [
      {
        path: 'primary',
        data: {
          title: 'Primary Battery Allocation'
        },
        loadChildren: './dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'secondary',
        data: {
          title: 'Secondary Battery Allocation'
        },
        loadChildren: './dashboard/dashboard.module#DashboardModule'
      }
    ]
  },

  {
    path: 'dashboard',
    data: {
      title: 'Dashboard',
      data: { preload: true }
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'allasset'
      },
      {
        path: 'recentpath/:id',
        data: {
          title: 'Recent Path'
        },
        loadChildren: './dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'allasset',
        data: {
          title: 'All Asset'
        },
        loadChildren: './dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'newasset',
        data: {
          title: 'Create Asset'
        },
        loadChildren: './dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'trackasset',
        data: {
          title: 'Track Asset'
        },
        children: [
          {
            path: ':id',
            loadChildren: './dashboard/dashboard.module#DashboardModule'
          }
        ]
      },
      {
        path: 'assetcheck',
        data : {
          title: 'Asset Health Check',
        },
        children : [
        {
          path : ':id',
          loadChildren: './dashboard/dashboard.module#DashboardModule'
        }
        ]

      },
      {
        path: 'batteryStatus',
        data : {
          title: 'Battery Status',
        },
        children : [
        {
          path : ':id',
          loadChildren: './dashboard/dashboard.module#DashboardModule'
        }
        ]

      },
      {
        path: 'editasset',
        data: {
          title: 'Edit Asset'
        },
        children: [
          {
            path: ':id',
            loadChildren: './dashboard/dashboard.module#DashboardModule'
          }
        ]
      },
      {
        path: '**',
        redirectTo: 'allasset',
        pathMatch: 'prefix'
      }
    ]
  },
  {
    path: 'lspdashboard',
    data: {
      title: 'LSP Dashboard',
      data: { preload: true }
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'allasset'
      },
      {
        path: 'allasset',
        data: {
          title: 'All Asset'
        },
        loadChildren: './lsp/lsp-dashboard/lsp-dashboard.module#LspDashboardModule'
      },
      {
        path: 'collectasset',
        data: {
          title: 'Collect Asset'
        },
        loadChildren: './lsp/lsp-dashboard/lsp-dashboard.module#LspDashboardModule'
      },
      {
        path: 'allocateasset',
        data: {
          title: 'Allocate Asset'
        },
        loadChildren: './lsp/lsp-dashboard/lsp-dashboard.module#LspDashboardModule'
      },
      {
        path: 'reportissue',
        data: {
          title: 'Report Issue'
        },
        loadChildren: './lsp/lsp-dashboard/lsp-dashboard.module#LspDashboardModule'
      },
        {
        path: 'lspPayment',
        data: {
          title: 'Pay to Lsp'
        },
        loadChildren: './lsp/lsp-dashboard/lsp-dashboard.module#LspDashboardModule'
      },
      {
        path: 'trackasset',
        data: {
          title: 'Track Asset'
        },
        loadChildren: './lsp/lsp-dashboard/lsp-dashboard.module#LspDashboardModule'
      },
      {
        path: 'chargeasset',
        data: {
          title: 'Charge Asset'
        },
        loadChildren: './lsp/lsp-dashboard/lsp-dashboard.module#LspDashboardModule'
      },
      {
        path: 'paytm',
        data: {
          title: 'Paytm'
        },
        loadChildren: './lsp/lsp-dashboard/lsp-dashboard.module#LspDashboardModule'
      },
      {
        path: '**',
        redirectTo: 'allasset',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: 'field',
    data: {
      title: 'field Dashboard',
      data: { preload: true }
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'Payment'
      },
      {
        path: 'Payment',
        data: {
          title: 'All Asset'
        },
        loadChildren: './dashboard/dashboard.module#DashboardModule'
      }
    ]
  },
  {
    path: '**',
    redirectTo: 'login',
    pathMatch: 'full'
  }
  //  {
  //   path: 'newasset',
  //   data: {
  //     title: 'Create Asset',
  //   },
  //   loadChildren: './dashboard/dashboard.module#DashboardModule'
  //   },
  //  {
  //   path: 'trackasset',
  //   data: {
  //     title: 'Track Asset',
  //   },
  //   children: [
  //     {
  //       path: ':id',
  //       loadChildren: './dashboard/dashboard.module#DashboardModule'
  //     }
  //   ]
  // }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: CustomPreloadStrategy })],
  exports: [RouterModule],
  providers: [CustomPreloadStrategy]
})
export class AppRoutingModule {}
