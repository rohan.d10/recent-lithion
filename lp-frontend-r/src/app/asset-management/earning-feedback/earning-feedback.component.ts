import { Component, OnInit } from '@angular/core';
import { FormControl,FormGroup,Validators, } from '@angular/forms';
import * as models from '../../models';
import { CommonService } from '../../services/common.service';
import Swal from 'sweetalert2';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';


@Component({
  selector: 'app-earning-feedback',
  templateUrl: './earning-feedback.component.html',
  styleUrls: ['./earning-feedback.component.css']
})
export class EarningFeedbackComponent implements OnInit {
  earningForm: FormGroup;
  public formData    = {};
  public driverList = [];
  filteredOptions: Observable<string[]>;

  constructor(private common: CommonService) { }

  ngOnInit() {
  	this.initializeForm();
    this.getAllDrivers();
    // this.filterData();
  }

  initializeForm(){
     this.earningForm = new FormGroup({
		'driverId' : new FormControl('', Validators.required),
       'amount': new FormControl('', [Validators.required, Validators.min(1), Validators.pattern('^[0-9]*$')]),
      });
  }

  getAllDrivers(){
  	this.common.getDriverProfileData().subscribe((data) => {
  		if(data.response_code == 200)
  			this.driverList = data.response;
  	});
  }

  resetForm(){
    this.earningForm.reset();
    this.initializeForm();
  }

  // filterData() {
  //   this.filteredOptions = this.earningForm.controls.driverId.valueChanges
  //     .pipe(
  //       startWith(''),
  //       map(value => this._filter(value))
  //     );
  // }
  //  _filter(value: string): string[] {
  //   const filterValue = value.toLowerCase();
  //   return this.driverList.filter(option => option.toLowerCase().includes(filterValue));
  // }


  onSubmit(){
     if(this.earningForm.valid) {
        this.formData = this.earningForm.value;

        this.common.earningFeedback(this.formData).subscribe((data) => {
          if(data.response_code == 200){
              Swal('Success!',data.response,'success');
              this.resetForm();
          }
          else
            Swal('Failure!',data.response,'error');
        });
      }
  }


}
