import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import * as models from '../../models';
import { AuthenticationService } from '../../services';
import { RestService } from '../../services';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import 'rxjs/add/operator/map';


@Component({
	selector: 'app-driver-profile',
	templateUrl: './driver-profile.component.html',
	styleUrls: ['./driver-profile.component.scss']
})

export class DriverProfileComponent implements OnDestroy,OnInit {
    dtOptions: DataTables.Settings = {};
	public driverInfo: models.TestInfo[] = [];
	dtTrigger: Subject<any> = new Subject();

	@ViewChild(DataTableDirective)
	dtElement: DataTableDirective;

	constructor(private route: Router, private authService: AuthenticationService, 
		private restService: RestService) {
		/*if (this.authService.isLoggedOut() || this.authService.getUserType() != 'ADMIN') {
			this.authService.checkAndRedirect();
		}*/
	}

	ngOnInit() {
		this.dtOptions = {
	      pagingType: 'full_numbers',
	      pageLength: 10,
	    };
		this.getDriverData();
	}

	ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

	public getDriverData() {
		this.driverInfo = [];
		this.restService.getDriverProfileData().subscribe((data) => {
			this.driverInfo = data;
			this.dtTrigger.next();
		});
	}

	rerender() {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.getDriverData();
    });
  }
  
	getData(){
		console.log(this.driverInfo[0].driver_name)
	}

}
