import { Component, OnInit, OnDestroy, EventEmitter, ViewChild, Output, ElementRef, ChangeDetectorRef } from '@angular/core';
import { LspAssetTable, LspDetails } from '../../../models';
import { Store } from '@ngrx/store';
import * as fromRoot from '../../../store/reducers';
import { Subscription } from 'rxjs/Subscription';
import { LspFetchService, AuthenticationService, CommonService } from '../../../services';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import 'rxjs/add/operator/map';
import { DataTableDirective } from 'angular-datatables';

@Component({
  selector: 'app-lsp-asset-table',
  templateUrl: './lsp-asset-table.component.html',
  styleUrls: ['./lsp-asset-table.component.scss']
})

export class LspAssetTableComponent implements OnInit, OnDestroy {
  private __subscriptions__: Subscription[] = new Array<Subscription>();
  public assets: LspAssetTable[];
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  @Output() addAssetButtonClicked = new EventEmitter<string>();

  public lspId: string = null;
  public lspDetails    = [];
  assetvalue           = 'not clicked';
  isLoading: boolean   = false;
  defaultLatitude      = 28.717012;
  defaultLongitude     = 77.11383;

  constructor(private changeDetectorRef: ChangeDetectorRef,private authService: AuthenticationService,
                private lspDetailsService: LspFetchService,private store: Store<fromRoot.State>,
                private common: CommonService) {

    this.lspId = this.authService.getUserId();
    this.fetchLspDetails();
    this.lspDefaultLocation();
  }

  ngOnInit() {
    this.dtOptions = {
        pagingType: 'full_numbers',
        pageLength: 10,
      };
    this.getDataFromStore();
  }

  lspDefaultLocation(){
    this.common.lspDefaultLocation({ lspId:this.lspId }).subscribe(data => {
       if(data.response_code == 200){
         this.defaultLatitude = data.response.coordinates.x;
         this.defaultLongitude = data.response.coordinates.y;
       }
    });  
  }

  onAddAsset(feature: string) {
    this.addAssetButtonClicked.emit(feature);
  }

  private getDataFromStore() {
    this.__subscriptions__.push(
      this.store.select(fromRoot.getLspAssets).subscribe(assets => {
        this.assets = assets;
        this.changeDetectorRef.detectChanges();
      })
    );
  } 
  
  private fetchLspDetails() {
    this.isLoading = true;
    this.common.lspListedAssets({"lspId":this.lspId}).subscribe(response => {
      
      if (response.response_code == 200) {
        this.lspDetails = response.response;
        this.dtTrigger.next();

        this.lspDetails.forEach(element => {
          element.lat = parseFloat(element.lat);
          element.lon = parseFloat(element.lon);
          element.markerLabel = {
            fontFamily: 'arial',
            fontWeight: 'bold',
            fontSize: '10',
            color: '#ffffff',
            text: element.batteryPackId
          };
           if (element.actorId === this.lspId) {
              element.moneyToCollect = 'N/A';
          }
        });
      }
      this.isLoading = false;
    });
  }

  rerender() {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.fetchLspDetails();
    });
  }
  /**
   * Cleanup all the subscriptions when component is destroyed.
   */

  ngOnDestroy() {
    this.__subscriptions__.forEach(subscription => subscription.unsubscribe());
    this.dtTrigger.unsubscribe();
  }
}
