import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import * as constants from '../constants';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { tap } from 'rxjs/operators';
import { Lsp, LspDetailsResponse, LspResponse } from '../models';
import { APIUrls } from '../constants';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    Authorization: 'my-auth-token'
  })
};

export const postOption = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable()
export class LspFetchService {
  constructor(private _http: HttpClient) {}

  private regUrl = APIUrls.FETCH_LSPS;

  private lspFetchUrl = APIUrls.FETCH_LSP_DETAILS;

  public fetchLsp(): Observable<LspResponse> {
    return this._http.get<LspResponse>(this.regUrl);
  }

  public fetchLspDetails(lspId: string): Observable<LspDetailsResponse> {
    return this._http.get<LspDetailsResponse>(this.lspFetchUrl + '?lspId=' + lspId);
  }
}
