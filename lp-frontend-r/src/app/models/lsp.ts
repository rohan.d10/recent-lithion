export class Lsp {
  public lspId: string;
  public name: string;
  public partner_name: string;
  public phone: string;
  public address: string;
}

export class LspResponse {
  public response: Lsp[];
  public response_code: number;
}
export class AllocateAsset {
  public assetid: string;
  public driverid: string;
  public lspid: string;
}

export class AllocateAssetResponse {
  response_code: string;
  AssignedBatteries: string[];
  response: string;
}

export class AllocateAssetPostParam {
  public assigntoid: string;
  public vals: { batterypackid: string }[];
}

export class CollectAsset {
  public assetid: string;
  public driverid: string;
  public kmDriven: string;
  public ahUsed: string;
  public lspid: string;
}

export class DeassignParam {
  batteryPackId: string;
  driverid: string;
  lspid: string;
}

export class LspDetails {
  batteryPackId: string;
  actorName: string;
  actorId: string;
  capLeft: string;
  kmLeft: string;
  state: string;
  moneyToCollect: any;
  lat : string;
  lon : string;
  markerLabel: string;
}
export class LspDetailsResponse {
  response_code: number;
  response: LspDetails[];
}
