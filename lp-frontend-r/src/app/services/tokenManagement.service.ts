import { Injectable } from '@angular/core';
import * as models from '../models';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { tap, catchError } from 'rxjs/operators';

import * as constants from '../constants';
import { APIUrls } from '../constants';

@Injectable()
export class TokenManagementService {
  private checkUrl = APIUrls.CHECK_TOKEN;

  constructor(private http: HttpClient) {}

  public checkToken(): Observable<models.CheckJwtResponse> {
    return this.http.get<models.CheckJwtResponse>(this.checkUrl).pipe(catchError(this.logError));
  }

  public logError(error: Response) {
    console.log('error occurred');
    return Observable.throw(error || 'server error');
  }
}
