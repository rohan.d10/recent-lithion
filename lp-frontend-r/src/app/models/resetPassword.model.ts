export class RequestOtpResponse {
  response_code: number;
  response: {
    message: string;
    phone: string;
  };
}
export class CheckOtpResponse {
  response_code: number;
  response: any;
}
export class ConfirmOtpResponse {
  response_code: number;
  response: any;
}
