import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AgmCoreModule } from '@agm/core';
import { CommonModule } from '@angular/common';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { LaddaModule } from 'angular2-ladda';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ToasterModule, ToasterService } from 'angular2-toaster/angular2-toaster';
import { AddAssetTableComponent } from '../asset-management/add-asset-table/add-asset-table.component';
import { AssetTableComponent } from '../asset-management/asset-table/asset-table.component';
import { EditAssetTableComponent } from '../asset-management/edit-asset-table/edit-asset-table.component';
import { TrackAssetComponent } from '../asset-management/track-asset/track-asset.component';

import { AssetAssignmentComponent } from '../asset-management/asset-assignment/asset-assignment.component';
import { AccountComponent } from '../asset-management/account/account.component';
import { ChecklistComponent } from '../asset-management/checklist/checklist.component';
import { RegisterDriverComponent } from '../asset-management/register-driver/register-driver.component';
import { RecentpathComponent } from '../asset-management/recentpath/recentpath.component';
import { EditDataLoggerComponent } from '../asset-management/edit-data-logger/edit-data-logger.component';
import { RegisterUserComponent } from '../asset-management/register-user/register-user.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { DriverProfileComponent } from '../components/driver-profile/driver-profile.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MainModule } from '../main.module';
import { DataTablesModule } from 'angular-datatables';

import {
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule,
  MatRadioModule,
  MatIconModule,
  MatTableModule,
  MatPaginatorModule,
  MatSortModule,
  MatCheckboxModule,
  MatCardModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatAutocompleteModule,
  MatButtonModule,
  MatToolbarModule
} from '@angular/material';

import { MaterialModule } from '../material.module';
import { RegisterLspComponent } from '../asset-management/register-lsp/register-lsp.component';
import { CreateBatteryPackComponent } from '../asset-management/create-battery-pack/create-battery-pack.component';
import { RegisterCellComponent } from '../asset-management/register-cell/register-cell.component';
import { RegisterChargerComponent } from '../asset-management/register-charger/register-charger.component';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { EarningFeedbackComponent } from '../asset-management/earning-feedback/earning-feedback.component';
import { AssetHealthCheckComponent } from '../asset-management/asset-health-check/asset-health-check.component';
import { ViewChecklistComponent } from '../asset-management/view-checklist/view-checklist.component';
import { ReceiveChecklistComponent } from '../asset-management/receive-checklist/receive-checklist.component';
import { EditBmsComponent } from '../asset-management/edit-bms/edit-bms.component';
import { ViewReceiveinChecklistComponent } from '../asset-management/view-receivein-checklist/view-receivein-checklist.component';
import { AccountViewComponent } from '../asset-management/account-view/account-view.component';
import { SecondaryAssetAssignmentComponent } from '../asset-management/secondary-asset-assignment/secondary-asset-assignment.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { AssetStatusComponent } from '../asset-management/asset-status/asset-status.component';
import { BatteryTableComponent } from '../asset-management/battery-table/battery-table.component';
import { CustomerInteractionComponent } from '../asset-management/customer-interaction/customer-interaction.component';
import { ViewCustomerInteractionComponent } from '../asset-management/view-customer-interaction/view-customer-interaction.component';
import { DataLoggerBatteryComponent } from '../asset-management/data-logger-battery/data-logger-battery.component';

@NgModule({
  imports: [
    MainModule,
    ChartsModule,
    MaterialModule,
    CommonModule,
    RouterModule,
    FormsModule,
    NgxChartsModule,
    ReactiveFormsModule,
    TabsModule.forRoot(),
    BsDropdownModule.forRoot(),
    ToasterModule.forRoot(),
    LaddaModule,
    NgbModule.forRoot(),
    DashboardRoutingModule,
    DataTablesModule,
    AngularMultiSelectModule,
    BsDatepickerModule.forRoot(),

    Ng4LoadingSpinnerModule.forRoot(),
    AgmCoreModule.forRoot({
      // dummy API key for testing
      apiKey: 'AIzaSyDd91n_BIQk7CBMOGq3rgW_lLXIl_LdxBQ'
    }),
    MatInputModule,
    MatSelectModule,
  MatFormFieldModule,
    MatRadioModule,
  MatToolbarModule,
  MatIconModule,
  MatTableModule,
  MatPaginatorModule,
  MatSortModule,
  MatCheckboxModule,
  MatCardModule,
  MatDatepickerModule,
    MatNativeDateModule,
    MatAutocompleteModule,
  MatButtonModule
  ],
  declarations: [
    DashboardComponent,
    // UserRegisterComponent,
    // UserRegisterFormComponent,
    AssetTableComponent,
    AddAssetTableComponent,
    CustomerInteractionComponent,
    EditAssetTableComponent,
    AssetAssignmentComponent,
    TrackAssetComponent,
    RegisterDriverComponent,
    RegisterLspComponent,
    RecentpathComponent,
    DataLoggerBatteryComponent,
    CreateBatteryPackComponent,
    DriverProfileComponent,
    EditDataLoggerComponent,
    AssetStatusComponent,
    BatteryTableComponent,
    AccountComponent,
    ChecklistComponent,
    RegisterUserComponent,
    RegisterCellComponent,
    EarningFeedbackComponent,
    RegisterChargerComponent,
    AssetHealthCheckComponent,
    ViewChecklistComponent,
    ReceiveChecklistComponent,
    EditBmsComponent,
    ViewReceiveinChecklistComponent,
    AccountViewComponent,
    SecondaryAssetAssignmentComponent,
    ViewCustomerInteractionComponent

  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DashboardModule {}
