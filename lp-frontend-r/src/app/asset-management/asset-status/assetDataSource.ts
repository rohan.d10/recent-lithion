import { Observable, BehaviorSubject, of } from 'rxjs';
import { AssetService } from './asset.service';
import { DataSource } from '@angular/cdk/collections';
import { CommonService } from '../../services/common.service';
import { User } from './asset.model';


export class AssetDataSource extends DataSource<any> {
  constructor(private common: CommonService) {
    super();
  }
  connect(): Observable<User[]> {
    return this.common.getAssetList();
  }
  disconnect() {}
}
