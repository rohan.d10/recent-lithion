import { Component, OnInit} from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';




@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  constructor() { }
  leaveForm: FormGroup;

  ngOnInit() {
    this.leaveForm = new FormGroup({
    'name' : new FormControl(null, Validators.required),
    'email' : new FormControl(null,[Validators.required , Validators.email]),
    'number' : new FormControl(null),
  });
}

onSubmit(){
    console.log(this.leaveForm);
}
  
}

