export const navigationDash = [
  {
    name: 'Dashboard',
    url: '/dashboard/allasset',
    icon: 'icon-speedometer',
    badge: {
      variant: 'info'
    }
  },

  {
    name: 'Registrations',
    url: '/register',
    icon: 'fa fa-registered',
    children: [
      {
        name: 'Asset Registration',
        url: '/register/newasset',
        icon: 'fa fa-registered'
      },
      {
        name: 'Add Driver',
        url: '/register/registerdriver',
        icon: 'fa fa-user'
      },
      {
        name: 'Add LSP',
        url: '/register/registerlsp',
        icon: 'fa fa-home',
        badge: {
          variant: 'info'
        }
      },
      {
        name: 'Create Battery Pack',
        url: '/register/registerbatterypack',
        icon: 'fa fa-bolt'
      }
    ]
  },
  {
    name: 'Edit Components',
    url: '/edit',
    icon: 'icon-note',
    children: [
      {
        name: 'Edit BatteryPack',
        url: '/edit/editbatterypack',
        icon: 'fa fa-bolt'
      },
      {
        name: 'Edit DataLogger',
        url: '/edit/editdatalogger',
        icon: 'fa fas fa-microchip'
      },
      {
        name: 'Edit Bms',
        url: '/edit/editbms',
        icon: 'fa fa-registered'
      }
    ]
  },
  {
    name: 'Battery Checklist',
    url: '/check',
    icon: 'fa fa-check',
    children: [
      {
        name: 'Sending Out',
        url: '/check/checklist',
        icon: 'fa fa-check'
      },
      {
        name: 'Receiving In',
        url: '/check/receive-checklist',
        icon: 'fa fa-check'
      }
    ]
  },
  {
    name: 'View Checklist',
    url: '/view',
    icon: 'fa fa-eye',
    children: [
      {
        name: 'Sending Out',
        url: '/view/sendingOut',
        icon: 'fa fa-check-square-o'
      },
      {
        name: 'Receiving In',
        url: '/view/receivingIn',
        icon: 'fa fa-check-square-o'
      }
    ]
  },
  {
    name: 'Battery Allocation',
    url: '/assignment',
    icon: 'fa fa-battery-full',
    children: [
      {
        name: 'Primary',
        url: '/assignment/primary',
        icon: 'fa fa-product-hunt'
      },
      {
        name: 'Secondary',
        url: '/assignment/secondary',
        icon: 'fa fa-scribd'
      }
    ]
  },

  {
    name: 'Driver Payments',
    url: '/payment',
    icon: 'fa fa-dollar',
    children: [
      {
        name: 'Add',
        url: '/payment/add',
        icon: 'fa fa-money'
      },
      {
        name: 'View',
        url: '/payment/view',
        icon: 'fa fa-eye'
      }
    ]
  },
  {
    name: 'User Registration',
    url: '/user-registration',
    icon: 'fa fa-user-plus',
    badge: {
      variant: 'info'
    }
  },
  {
    name: 'Cell Registration',
    url: '/cell-registration',
    icon: 'fa fa-battery-empty',
    badge: {
      variant: 'info'
    }
  },
   {
    name: 'Charger Registration',
    url: '/charger-registration',
    icon: 'fa fa-plug',
    badge: {
      variant: 'info'
    }
  },
  {
    name: 'Payment Confirmation',
    url: '/earning-feedback',
    icon: 'fa fa-money',
    badge: {
      variant: 'info'
    }
  },
  {
    name: 'Battery Status',
    url: '/asset-status',
    icon: 'fa fa-battery-half',
    badge: {
      variant: 'info'
    }
  },

  {
    name: 'Customer Interaction',
    url: '/customer',
    icon: 'fa fa-user-circle',
    children: [
      {
        name: 'Add',
        url: '/customer/customer-interaction',
        icon: 'fa fa-plus',
        badge: {
          variant: 'info'
        }
      },
      {
        name: ' View',
        url: '/customer/view-customer-interaction',
        icon: 'fa fa-eye',
        badge: {
          variant: 'info'
        }
      }

    ]
  },
  {
    name: 'Data-Logger',
    url: '/datalogger',
    icon: 'fa fa-microchip',
    badge: {
      variant: 'info'
    }
  }
]
export const navigationLsp = [
  {
    name: 'Dashboard',
    url: '/lspdashboard/allasset',
    icon: 'icon-speedometer',
    badge: {
      variant: 'info'
    }
  },

  {
    name: 'Collect Assets',
    url: '/lspdashboard/collectasset',
    icon: 'icon-arrow-down-circle',
    badge: {
      variant: 'info'
    }
  },
  {
    name: 'Allocate Assets',
    url: '/lspdashboard/allocateasset',
    icon: 'icon-share-alt',
    badge: {
      variant: 'info'
    }
  },
  {
    name: 'Report Issue',
    url: '/lspdashboard/reportissue',
    icon: 'icon-bubbles',
    badge: {
      variant: 'info'
    }
  },
  {
    name: 'Pay to Lsp',
    url: '/lspdashboard/lspPayment',
    icon: 'icon-bubbles',
    badge: {
      variant: 'info'
    }
  }
];

export const navigationField = [
  {
    name: 'Payment',
    url: '/field/Payment',
    icon: 'icon-speedometer',
    badge: {
      variant: 'info'
    }
  }
];
