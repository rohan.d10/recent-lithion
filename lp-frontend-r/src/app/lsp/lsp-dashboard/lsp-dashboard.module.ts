import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AgmCoreModule } from '@agm/core';
import { CommonModule } from '@angular/common';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToasterModule, ToasterService } from 'angular2-toaster';
import { LspAssetTableComponent } from '../lsp-asset-management/lsp-asset-table/lsp-asset-table.component';
import { LspCollectAssetComponent } from '../lsp-asset-management/lsp-collect-asset/lsp-collect-asset.component';
import { LspAllocateAssetComponent } from '../lsp-asset-management/lsp-allocate-asset/lsp-allocate-asset.component';
import { LspChargeAssetComponent } from '../lsp-asset-management/lsp-charge-asset/lsp-charge-asset.component';
import { LspReportIssueComponent } from '../lsp-asset-management/lsp-report-issue/lsp-report-issue.component';
import { LspTrackAssetComponent } from '../lsp-asset-management/lsp-track-asset/lsp-track-asset.component';
import { LspDashboardRoutingModule } from './lsp-dashboard-routing.module';
import { LspDashboardComponent } from './lsp-dashboard.component';
import { MainModule } from '../../main.module';
import { DataTablesModule } from 'angular-datatables';
import { LspPaymentComponent } from '../lsp-asset-management/lsp-payment/lsp-payment.component';
import { PaytmComponent } from '../lsp-asset-management/paytm/paytm.component';

// import { UserRegisterFormComponent } from '../user/user-register-form/user-register-form.component';

@NgModule({
  imports: [
    MainModule,
    ChartsModule,
    CommonModule,
    RouterModule,
    FormsModule,
    DataTablesModule,
    ToasterModule.forRoot(),
    ReactiveFormsModule,
    TabsModule.forRoot(),
    BsDropdownModule.forRoot(),
    LspDashboardRoutingModule,
  
    AgmCoreModule.forRoot({
      // dummy API key for testing
      apiKey: 'AIzaSyDd91n_BIQk7CBMOGq3rgW_lLXIl_LdxBQ'
    })
  ],
  declarations: [
    LspDashboardComponent,
    // UserRegisterComponent,
    // UserRegisterFormComponent,
    LspAssetTableComponent,
    LspCollectAssetComponent,
    LspAllocateAssetComponent,
    LspChargeAssetComponent,
    LspTrackAssetComponent,
    LspReportIssueComponent,
    LspPaymentComponent,
    PaytmComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class LspDashboardModule {}
