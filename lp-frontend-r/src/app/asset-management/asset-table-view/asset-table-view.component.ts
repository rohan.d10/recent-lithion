import { Component, OnInit, Input } from '@angular/core';

import { AssetDataService } from '../../services';

@Component({
  selector: 'app-asset-table-view',
  templateUrl: './asset-table-view.component.html',
  styleUrls: ['./asset-table-view.component.scss'],
  providers: [AssetDataService]
})
export class AssetTableViewComponent implements OnInit {
  // @Input() assetReview: createAsset;
  // comment added

  // assetReviewTable: createAsset[] = [];
  // @Input() assetview :{ ssetvendorID: string, assetType: string, assetTypeInfo: string, assetName: string}

  // assetvendorID: string = '123434';
  // assetType: string = 'Battery';
  // assetTypeInfo: string = '16';
  // // assetName: string = 'BP01';

  constructor(private assetReviewService: AssetDataService) {}

  ngOnInit() {
    // this.assetReviewTable = this.assetReviewService.viewAssetFunction();
  }

  getassetName() {
    // return this.assetReview.name;
  }

  getassetType() {
    // return this.assetType;
  }
  getassetVendorId() {
    // return this.assetvendorID;
  }
  getassetTypeInfo() {
    // return this.assetTypeInfo;
  }
}
