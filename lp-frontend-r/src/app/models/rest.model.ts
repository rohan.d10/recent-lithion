
export class TestInfo {
  driver_name: string;
  driverId: string;
  assetId: string;
  KM_driven: number;
  startAh: number;
  usedAh: number;
  AH_left: number;
  KM_left: number;
  pEarn: number;
  earnedCurrCycle: number;
}

export class fetchDlDetails{
  data_logger_id: string;
  vendor_id: string;
  version: string;
  m_number: string;
  s_number: string;
}