export class BatteryPack {
  batteryPackId: string;
  assignmentStatus: string;
}

export class RegisterBatteryPost {
  batteryPackId: string;
  associatedBmsId: string;
  associatedDataloggerId: string;
  cellVendorName: string;
  cabinetVersion: string;
  updatedBy: string;
  update: string;
}

export class RegisterBatteryResponse {
  response_code: any;
  response: any;
}

export class BatteryResponse {
  response_code: number;
  response: BatteryPack[];
}

export class SingleBatteryResponse {
  response_code: number;
  response: SingleBattery;
}

export class SingleBattery {
  battery_pack_id: string;
  associated_bms_id: string;
  associated_datalogger_id: string;
  cell_vendor_name: string;
  cabinet_version: string;
  is_active: string;
  is_assigned: string;
  created_at: string;
  last_update_at: string;
  updated_by: string;
}
