
export class DriverInfo {
  driver_name: string;
  driverId: string;
}

export class LspInfo {
  name: string;
  lspId: string;
}

export class DataLoggerInfo{
  dataLoggerId: string
}

export class repopulateBatteryData{
	dataLoggerId:string;
}