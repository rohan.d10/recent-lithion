import {
  Component,
  ViewChild,
  OnInit,
  OnDestroy,
  Output,
  EventEmitter
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { CreateAsset, Bms, Datalogger } from '../../models';
import { Subscription } from 'rxjs/Subscription';
import { DataLoggerCreateService } from '../../services/dataLoggerCreate.service';
import { ToasterService } from 'angular2-toaster';
import { BmsRegService } from '../../services';
import { CommonService } from '../../services/common.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-add-asset-table',
  templateUrl: './add-asset-table.component.html',
  styleUrls: ['./add-asset-table.component.scss']
})
export class AddAssetTableComponent implements OnInit, OnDestroy {
  @ViewChild('dlForm') public dlForm: NgForm;
  @ViewChild('bmsForm') public bmsForm: NgForm;

  private __subscriptions__: Subscription[] = new Array<Subscription>();
  @Output() reviewButtonClicked = new EventEmitter<string>();

  public showAlert            = false;
  addCell                     = true;
  defaultAssetTypeSelect      = 'Data Logger';
  defaultAssetInfoSelect      = '16S';
  public DL: Datalogger       = new Datalogger();
  public assetName: String;
  public assetTypes: string[] = ['Data Logger', 'BMS'];
  bmsTypes                    = [{ label: 'WBMS', value: 1 }, { label: 'BMS NQ', value: 2 }];
  public bms: Bms             = new Bms();
  public assetType            = this.assetTypes[0];
  public labelClass           = 'show';
  public spinner              = 'hide';

  constructor(
    private DataLoggerService: DataLoggerCreateService,
    private toasterService   : ToasterService,
    private bmsRegistration  : BmsRegService,
    private common           : CommonService
  ) {}

  ngOnInit() {
    this.assetType = this.assetTypes[0];
  }

  public popupAlert() {
    this.showAlert = true;
    setTimeout(() => {
      this.showAlert = false;
    }, 5000);
  }

  resetDlForm() {
    this.dlForm.reset();
  }

  resetBmsForm() {
    this.bmsForm.reset();
  }

  onReview1(): void {
    switch (this.assetType) {
      case this.assetTypes[0]:
        this.common.registerDataLogger(this.dlForm.value).subscribe(data => {
          if (data.response_code === 200) {
            Swal(data.data, data.response, 'success');
            this.resetDlForm();
          } else {
            Swal('Failure!', data.response, 'error');
          }
        });
        break;

      case this.assetTypes[1]:
        this.common.registerBms(this.bmsForm.value).subscribe(data => {
          if (data.response_code === 200) {
            Swal(data.data, data.response, 'success');
            this.resetBmsForm();
          } else {
            Swal('Failure!', data.response, 'error');
          }
        });
        break;
    }
  }

  private showSuccesToast(id, type) {
    this.toasterService.pop(
      'success',
      type + ' registration success',
      type + ' ' + id + ' successfully registered.'
    );
  }

  private showFailureToast(id, type, error) {
    this.toasterService.pop(
      'error',
      type + ' ' + id + ' registration Failed',
      error
    );
  }

  ngOnDestroy() {
    this.__subscriptions__.forEach(subscription => subscription.unsubscribe());
  }
}
