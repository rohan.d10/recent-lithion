import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl,FormGroup,Validators } from '@angular/forms';
import { CommonService } from '../../services/common.service';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-data-logger-battery',
  templateUrl: './data-logger-battery.component.html',
  styleUrls: ['./data-logger-battery.component.css']
})
export class DataLoggerBatteryComponent implements OnInit {

  constructor(private common: CommonService) { }

  public data = [];
  public dataLoggerId;
  public BId;
  tableHeaders = [
    'Data Logger ID',
    'Battery ID'
  ];
  dLoggerForm: FormGroup;

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  @ViewChild(DataTableDirective) dtElement: DataTableDirective;

  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10
    };
    this.initializeForm();
    this.getBatteryData();
  }


  initializeForm(){
    this.dLoggerForm = new FormGroup({
      'dataLoggerId': new FormControl('')
     });
 }
  getBatteryData() {
    this.common. DlId_BId({dataLoggerId : this.dataLoggerId}).subscribe(res => {

        this.data = res.data;
      this.dtTrigger.next();
    })
  }
  onSubmit(){
    if(this.dLoggerForm.valid){
      this.dataLoggerId  = this.dLoggerForm.get('dataLoggerId').value;


         this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          // Destroy the table first
          dtInstance.destroy();
          // Call the dtTrigger to rerender again
        });

      this.getBatteryData();
    }
  }

  ngOnDestroy() {
    this.dtTrigger.unsubscribe();
  }
}

