import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import * as constants from '../constants';
import { APIUrls } from '../constants';

@Injectable()
export class TrackAssetService {
  _url = APIUrls.TRACK_ASSETS;

  batteryData = APIUrls.BATTERY_DATA;

	httpOptions = {
		headers: new HttpHeaders({
			'Content-Type':  'application/json'
		})
	};


  constructor(private http: HttpClient) {}

  trackAsset(asset) {}

  private extractData(res: Response) {
		let body = res;
		return body || { };
	}

  getBatteryData(body): Observable<any> {
		return this.http.post(this.batteryData,body,this.httpOptions).pipe(
			map(this.extractData));
	}

}
