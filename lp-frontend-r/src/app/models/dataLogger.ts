export class DataLogger {
  constructor(
  public id: string,
  public type: string,
  public assetName: string,
  public venderId: string,
  public description: string,
  public mNumber: string,
  public sNumber: string,
  public createdDate: string ) {}
}

export class CreateAsset {

  // public vendorId: string;
  // public asserID: string;
  // public name: string;
  // public type: string;
  // public info: string;
  // public description: string;
  // public createdOn: string;

  constructor(public vendorId: string,
              public assetID: string,
              public name: string,
              public type: string,
              public info: string,
              public description: string,
              // public mnumber: string,
              // public snumber: string,
              public assetVendorIdCell1: string,
              public assetVendorIdCell2: string,
              public assetVendorIdCell3: string,
              public assetVendorIdCell4: string,
              public assetVendorIdCell5: string,
              public assetVendorIdCell6: string,
              public assetVendorIdCell7: string,
              public assetVendorIdCell8: string,
              public assetVendorIdCell9: string,
              public assetVendorIdCell10: string,
              public assetVendorIdCell11: string,
              public assetVendorIdCell12: string,
              public assetVendorIdCell13: string,
              public assetVendorIdCell14: string,
              public assetVendorIdCell15: string,
              public assetVendorIdCell16: string,
              public createdOn: string) {}
// for optional arguments we can add '?' in front of the variable name
}
