import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl,FormGroup,Validators, } from '@angular/forms';
import * as models from '../../../models';
import { CommonService } from '../../../services/common.service';
import { AuthenticationService } from '../../../services/authentication.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-lsp-allocate-asset',
  templateUrl: './lsp-allocate-asset.component.html',
  styleUrls: ['./lsp-allocate-asset.component.scss']
})
export class LspAllocateAssetComponent implements OnInit, OnDestroy {
  allocateAssetForm: FormGroup;
  public driverUnAssigned = [];
  public formData         = {};
  public lspId            = '';
  public batteries        = [];

  constructor(private common: CommonService, private auth: AuthenticationService,
                    private router: Router) {
    this.lspId  = this.auth.getUserId();
  }

  initializeForm(){
     this.allocateAssetForm = new FormGroup({
        'driverId'    : new FormControl(''    , [Validators.required]),
        'batteryId'   : new FormControl(''    , [Validators.required]),
        'capacity'    : new FormControl(''    , [Validators.required]),
        'kmEstimated' : new FormControl(''    , [Validators.required])
      });
  }

  ngOnInit() {
    this.initializeForm();
    this.fetchBatteries();
    this.fetchUnDriverAssigned();
  }

  valuechange(){
    var batteryId    = this.allocateAssetForm.controls.batteryId.value;
    var withoutSpace = batteryId.replace(/ /g,"");
    var length       = withoutSpace.length;

    if(length >= 4){
      for(let battery of this.batteries){
        if(batteryId == battery.batteryPackId){
            this.allocateAssetForm.patchValue({
              'capacity'    : battery.capLeft,
              'kmEstimated' : Math.round(battery.kmLeft)
            });
          break;
        }
      }
    }
  }

  fetchBatteries(){
    this.common.lspListedUnassignedAssets({"lspId":this.lspId}).subscribe(data => {
      if(data.response_code == 200){
        this.batteries = data.response;
      }
    });
  }

  private fetchUnDriverAssigned() {
    this.common.fetchUnDriverAssigned().subscribe(data => {
      if(data.response_code == 200)
        this.driverUnAssigned = data.response;
    });
  }

    resetForm(){
     this.allocateAssetForm.reset();
     this.initializeForm();
    }

  onSubmit(){
     if(this.allocateAssetForm.valid) {
        this.formData = {
          "assetId"  : this.allocateAssetForm.get('batteryId').value,
          "driverId" : this.allocateAssetForm.get('driverId').value,
          "lspId"    : this.lspId
        }
      this.common.assetAssignment(this.formData).subscribe((data) => {
        if(data.response_code == 200){
            Swal('Success!','Battery Assigned Successfully','success');  
            this.resetForm();
        }
        else{
          Swal('Failure!',data.response,'error');
        }
      });
    }
    else
      Swal('Failure!','Something went wrong.Please try again later','error');
  }

  ngOnDestroy() {
//    this.__subscriptions__.forEach(subscription => subscription.unsubscribe());
  }
}
