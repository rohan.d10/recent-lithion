import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import { Observable, BehaviorSubject, of } from 'rxjs';
import { ApiService } from 'src/app/api/api.service';
import { User } from 'src/app/api/api.model';
import { MatPaginator } from '@angular/material';
import { catchError, finalize } from 'rxjs/operators';

export class ApiDataSource extends DataSource<any> {
  constructor(private apiService: ApiService) {
    super();
  }
  connect(): Observable<User[]> {
    return this.apiService.getData();
  }
  disconnect() {}
}
