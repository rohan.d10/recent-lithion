import { CreateAsset } from '../models';
import { Injectable } from '@angular/core';
import { Http, Headers, Response, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import * as constant from '../constants';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class AssetDataService {
  private assets: CreateAsset[] = [];

  constructor(private http: Http) {}

  EditAssetFunction(asset: CreateAsset) {
    this.assets.push(asset);
    const headers = new Headers();
    headers.append('content-type', 'application/x-www-form-urlencoded');
    const body = new URLSearchParams();
    body.set('type', this.assets[0]['type']);
    body.set('name', this.assets[0]['name']);
    body.set('desc', this.assets[0]['desc']);
    body.set('vid', this.assets[0]['vid']);
    body.set('info', this.assets[0]['info']);
    body.set('mnumber', this.assets[0]['mnumber']);
    body.set('snumber', this.assets[0]['snumber']);
    body.set('createdOn', this.assets[0]['createdOn']);
    body.set('assetVendorIdCell1', this.assets[0]['assetVendorIdCell1']);
    body.set('assetVendorIdCell2', this.assets[0]['assetVendorIdCell2']);
    body.set('assetVendorIdCell3', this.assets[0]['assetVendorIdCell3']);
    body.set('assetVendorIdCell4', this.assets[0]['assetVendorIdCell4']);
    body.set('assetVendorIdCell5', this.assets[0]['assetVendorIdCell5']);
    body.set('assetVendorIdCell6', this.assets[0]['assetVendorIdCell6']);
    body.set('assetVendorIdCell7', this.assets[0]['assetVendorIdCell7']);
    body.set('assetVendorIdCell8', this.assets[0]['assetVendorIdCell8']);
    body.set('assetVendorIdCell9', this.assets[0]['assetVendorIdCell9']);
    body.set('assetVendorIdCell10', this.assets[0]['assetVendorIdCell10']);
    body.set('assetVendorIdCell11', this.assets[0]['assetVendorIdCell11']);
    body.set('assetVendorIdCell12', this.assets[0]['assetVendorIdCell12']);
    body.set('assetVendorIdCell13', this.assets[0]['assetVendorIdCell13']);
    body.set('assetVendorIdCell14', this.assets[0]['assetVendorIdCell14']);
    body.set('assetVendorIdCell15', this.assets[0]['assetVendorIdCell15']);
    body.set('assetVendorIdCell16', this.assets[0]['assetVendorIdCell16']);
    return this.http
      .post(constant.ServiceConstants.DOMAIN + '/api/v1/create', body, { headers: headers })
      .map((response: Response) => {})
      .catch(this.handelError);
  }

  private handelError(error: Response) {
    return Observable.throw(error.json() || 'server error');
  }
}
