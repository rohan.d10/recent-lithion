import { Component, OnInit } from '@angular/core';
// import { AgmCoreModule } from '@agm/core';

import { MapDataService } from './map-data.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-map-data',
  templateUrl: './map-data.component.html',
  styleUrls: ['./map-data.component.scss']
})
export class MapDataComponent implements OnInit {
  public places: Array<any>        = [];
  public _url                      = 'http://api.lithion.in/api/getdata?dlid=B010&limit=100';
  public SnappedPoints             = [];
  public SnappedLatLon: Array<any> = [];
  public str                       = '';
  public snap_url                  = '';
  public Ilatitude                 = 28.457766;
  public Ilongitude                = 77.0712333;
  public zoom                      = 12;
  public index                     = 0;
  public paths: Array<any>         = [];
  public len                       = 0;
  public pic_url: Array<any>       = [];

  constructor(
    private _mapDataService: MapDataService,
    private http: HttpClient
  ) { }

  ngOnInit() {
    this.getLithionData();
  }

  getLithionData() {
    this._mapDataService.getData(this._url).subscribe(data => {
      this.places = data;

      this.places.forEach(i => {
        this.paths.push({
          lat: i.lat,
          lon: i.lon
        });
      });
      this.filterLatLon();
    });
  }

  filterLatLon() {
    for (let i = 1; i < this.paths.length; i++) {
      if (
        this.paths[i].lat == this.paths[i - 1].lat &&
        this.paths[i].lon == this.paths[i - 1].lon
      ) {
        this.paths.splice(i, 1);
        i = i - 1;
      }
    }
    // console.log(this.paths);
    this.makeSnapToRoadURL();
  }

  makeSnapToRoadURL() {
    for (let i = 0; i < this.paths.length; i++) {
      if (i == this.paths.length - 1) {
        this.str = this.str + this.paths[i].lat + ',' + this.paths[i].lon;
      } else {
        this.str = this.str + this.paths[i].lat + ',' + this.paths[i].lon + '|';
      }
    }

    this.snap_url =
      'https://roads.googleapis.com/v1/snapToRoads?path=' +
      this.str +
      '&interpolate=true&key=AIzaSyDd91n_BIQk7CBMOGq3rgW_lLXIl_LdxBQ';
    // console.log(this.snap_url);
    this.getSnappedPoints();
  }
  getSnappedPoints() {
    this._mapDataService.getData(this.snap_url).subscribe(data => {
      this.SnappedPoints = data;
      for (let j = 0; j < data.snappedPoints.length; j++) {
        if (j == 0) {
          this.SnappedLatLon.push({
            latitude: data.snappedPoints[j].location.latitude,
            longitude: data.snappedPoints[j].location.longitude,
            url:
              'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=' +
              (j + 1) +
              '|32CD32|000000'
          });
        }
        if (j == data.snappedPoints.length - 1) {
          this.SnappedLatLon.push({
            latitude: data.snappedPoints[j].location.latitude,
            longitude: data.snappedPoints[j].location.longitude,
            url:
              'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=' +
              (j + 1) +
              '|FF0000|000000'
          });
        }
        if (j != data.snappedPoints.length - 1 && j != 0) {
          this.SnappedLatLon.push({
            latitude: data.snappedPoints[j].location.latitude,
            longitude: data.snappedPoints[j].location.longitude
            // url:
            //   'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=' +
            //   (j + 1) +
            //   '|FFFF33|000000'
          });
        }
      }
      // console.log(this.SnappedLatLon);
      this.len = this.SnappedLatLon.length;
    });
  }

  // startIconUrl() {
  //   return require('src/assets/images/greenPin.png');
  // }

  // stopIconUrl() {
  //   return require('src/assets/images/redPin.png');
  // }

  convert(c1: string) {
    return parseFloat(c1);
  }
}
