import { Component, OnInit, Input } from '@angular/core';
import { BatteryDataSource } from './batteryDataSource';
import { ActivatedRoute } from '@angular/router';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { CommonService } from '../../services/common.service';

@Component({
  selector: 'app-battery-table',
  templateUrl: './battery-table.component.html',
  styleUrls: ['./battery-table.component.scss']
})
export class BatteryTableComponent implements OnInit {
  public id;
  public Dl_Id = null;


  constructor(private common: CommonService, private route: ActivatedRoute, private spinnerService: Ng4LoadingSpinnerService) { }

  ngOnInit() {

    this.id = this.route.snapshot.params['id']
    this.getBatteries();
  }

  displayedColumns = ["loggedAt", "DlTimestamp", "state", "current", "batRemain", "cap", "vol", "lat", "lon", "kminterval", "v1", "v2", "v3", "v4", "v5", "v6", "v7", "v8", "v9", "v10", "v11", "v12", "v13", "v14", "v15", "v16", "extTemp1", "extTemp2", "extTemp3", "extTemp4", "extTemp5", "powerTemp", "ambientTemp", "sysStatus", "batStatus"]
  dataSource = new BatteryDataSource(this.common, this.id);



  getBatteries() {
    this.spinnerService.show();
    this.common.getBatteryDetails({ battery: this.id, limit: 10 }).subscribe(data => {
      if (data.response_code == 200) {
        this.spinnerService.hide();
        for (let i = 0; i < data.response.length; i++) {
          data.response[i].loggedAt = new Date(data.response[i].loggedAt).getTime();
          data.response[i].DlTimestamp = new Date(data.response[i].DlTimestamp).getTime();
        }
        this.Dl_Id = data.response[0].DL_id;
        this.dataSource = data.response;
      }
      });

  }
  checkDlId(p) {
    if (p != null) {
      return true;
    }
    else {
      return false;
    }
  }
}

