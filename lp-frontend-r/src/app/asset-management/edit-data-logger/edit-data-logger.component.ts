import { Component, OnInit } from '@angular/core';
import { RestService } from '../../services';
import { FormControl,FormGroup,Validators } from '@angular/forms';
import * as models from '../../models';
import { CommonService } from '../../services/common.service';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-edit-data-logger',
  templateUrl: './edit-data-logger.component.html',
  styleUrls: ['./edit-data-logger.component.css']
})
export class EditDataLoggerComponent implements OnInit {
  public fetchdataLoggerId: string;
  dlForm  : FormGroup;
  public dlInfo   = [];
  public formData = {};

  constructor(private restService: RestService,private common: CommonService) { }

  ngOnInit() {
  }

  fetchDLDetails(){
  	this.restService.getDLData({ dlId:this.fetchdataLoggerId }).subscribe((data) => {
     
        if(data.response_code == 200){
          this.dlInfo = data.response[0];
          this.dlForm = new FormGroup({
            'dataLoggerId'    : new FormControl(this.dlInfo['dataLoggerId'], Validators.required),
            'softwareVersion' : new FormControl(this.dlInfo['softwareVersion']),
            'hardwareVersion' : new FormControl(this.dlInfo['hardwareVersion']),
            'vendorName'      : new FormControl(this.dlInfo['vendorName']),
            'simVendorName'   : new FormControl(this.dlInfo['simVendorName']),
            'mNum'            : new FormControl(this.dlInfo['mNumber'], Validators.required),
            'sNum'            : new FormControl(this.dlInfo['sNumber'], Validators.required),
          });
        }
	    });
  }

  resetForm(){
    this.dlForm.reset();
    //this.initializeForm();
  }

  public onSubmit() {
    if(this.dlForm.valid) {
      this.formData = this.dlForm.value;
      this.common.editDataLogger(this.formData).subscribe((data) => {
        if(data.response_code == 200){
              this.dlInfo = [];
              Swal('Success!',data.response,'success');  
              this.resetForm();
        }
        else{
            Swal('Failure!',data.response,'error');
        }
      });
    }
  }

}
