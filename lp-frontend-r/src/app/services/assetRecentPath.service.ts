import * as constants from '../constants';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AssetRecentpath } from '../models';
import { tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { APIUrls } from '../constants';

@Injectable()
export class AssetRecentPathService {
  constructor(private http: HttpClient) {}

  private recentLocationsFetchUrl = 'https://app.lithion.in/V2/api/location/recent';

  public fetchRecentLocations(batteryPackId: string, date: string): Observable<AssetRecentpath> {
    return this.http.get<AssetRecentpath>(
      this.recentLocationsFetchUrl + '?assetid=' + batteryPackId + '&date=' + date
    );
  }
}
