import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/skip';
import 'rxjs/add/operator/takeUntil';
import * as tokenMangement from '../actions/token-management';
import * as fromRoot from '../reducers';
import { TokenManagementService } from '../../services/tokenManagement.service';

/**
 * Effects offer a way to isolate and easily test side-effects within your
 * application. StateUpdates is an observable of the latest state and
 * dispatched action. The `toPayload` helper function returns just
 * the payload of the currently dispatched action, useful in
 * instances where the current state is not necessary.
 *
 * A simple way to think of it is that ngrx/effects is an event listener of sorts.
 * It listens for actions being dispatched to the store. You can then tell `ngrx/effects`
 * that when a particular action is dispatched, to take another, new action as a result.
 * At the end, what’s really happening is `ngrx/effects` is an `action generator` that dispatches
 * a `new action` as a result of a different action.
 */

// todo

@Injectable()
export class TokenManagementEffects {
  @Effect()
  checkTokenEffect$ = this.actions$
    .ofType(tokenMangement.ActionTypes.CHECK_TOKEN)
    .switchMap(action => this.tokenService.checkToken())
    .map((response: any, error: any) => {
      if (response.code === '200') {
        return new tokenMangement.CheckTokenActionSuccess({ roles: response.roles, username: response.username });
      } else {
        // do something
      }

      if (error) {
      }
    })
    .catch(e => {
      return Observable.of<tokenMangement.CheckTokenActionSuccess>(
        new tokenMangement.CheckTokenActionSuccess({ roles: [], username: '' })
      );
    });

  constructor(
    private actions$: Actions,
    private tokenService: TokenManagementService,
    private store$: Store<fromRoot.State>
  ) {}
}
