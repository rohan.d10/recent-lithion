import { Action } from '@ngrx/store';

/**
 * For each action type in an action group, make a simple
 * enum object for all of this group's action types.
 *
 * The 'actionTypeCheck' utility function coerces strings into string
 * literal types and runs a simple check to guarantee all
 * action types in the application are unique.
 */
export const ActionTypes = {
  TRACK_ASSET: 'Track Asset',
  TRACK_ASSET_COMPLETE_SUCCESS: '[Api] Track asset success',
  TRACK_ASSET_COMPLETE_FAIL: '[Api] Track asset fail'
};

/**
 * Every action is comprised of at least a type and an optional
 * payload. Expressing actions as classes enables powerful
 * type checking in reducer functions.
 *
 * See Discriminated Unions: https://www.typescriptlang.org/docs/handbook/advanced-types.html#discriminated-unions
 */

export class TrackAssetAction implements Action {
  type = ActionTypes.TRACK_ASSET;

  constructor(public payload: number) {  }
}

export class TrackAssetSuccessAction implements Action {
  type = ActionTypes.TRACK_ASSET_COMPLETE_SUCCESS;

  constructor(public payload: any) {  }
}

export class TrackAssetFailAction implements Action {
  type = ActionTypes.TRACK_ASSET_COMPLETE_FAIL;

  constructor(public payload: any) {  }
}

/**
 * Export a type alias of all actions in this action group
 * so that reducers can easily compose action types
 */
export type Actions
  = TrackAssetAction
  | TrackAssetSuccessAction
  | TrackAssetFailAction;
