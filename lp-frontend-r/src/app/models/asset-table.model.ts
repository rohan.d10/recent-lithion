export class AssetTable {

  public index: string;
  public vendorId: string;
  public name: string;
  public type: string;
  public info: string;
  public status: string;
  public createdOn: string;

constructor(index: string, vendorId: string, name: string, type: string, info: string, status: string, createdOn: string) {
  this.index = index;
  this.vendorId = vendorId;
  this.name = name;
  this.type = type;
  this.info = info;
  this.status = status;
  this.createdOn = createdOn;
}
}
