import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatToolComponent } from './mat-tool.component';

describe('MatToolComponent', () => {
  let component: MatToolComponent;
  let fixture: ComponentFixture<MatToolComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatToolComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatToolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
