import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import * as constants from '../constants';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { tap } from 'rxjs/operators';
import { ReportIssue, ReportIssueResponse } from '../models';
import { APIUrls } from '../constants';

const postOption = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable()
export class ReportIssueService {
  constructor(private _http: HttpClient) {}

  private reportUrl = APIUrls.REPORT_ISSUE;
  // private regUrl = 'http://localhost:8000/api/dev/registerDriver';
  public reportIssue(issue: ReportIssue): Observable<ReportIssueResponse> {
    return this._http.post<ReportIssueResponse>(this.reportUrl, issue, postOption);
  }
}
