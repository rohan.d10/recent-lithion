import { Component, OnInit } from '@angular/core';
import { HttpEventType,HttpEvent } from '@angular/common/http';
import { FormControl,FormGroup,Validators,AbstractControl,ValidatorFn } from '@angular/forms';
import * as models from '../../models';
import { CommonService } from '../../services/common.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-receive-checklist',
  templateUrl: './receive-checklist.component.html',
  styleUrls: ['./receive-checklist.component.css']
})
export class ReceiveChecklistComponent implements OnInit {
  checklistForm: FormGroup;
  public formData            = {};
  public batteryList         = [];
  public actorsList          = [];
  public selectedFile : File = null;
  public maxFileSize         = 10*1024*1024; // 10MB 
  public fileAllowedTypes    = ['jpeg','png','jpg'];
  showProgessBar             = false;
  disableUploadBtn           = false;
  progPercentage             = 0;
  radioOptions               = [{'label':'Yes','value':'true'},{'label':'No','value':'false'}];

  constructor(private common: CommonService) { }

  ngOnInit() {
  	this.initializeForm();
  	this.associatedBatteries();
  	this.actors();
  }

  initializeForm(){
     this.checklistForm = new FormGroup({
    		'batteryId'        : new FormControl('', Validators.required),
    		'receivedFrom'     : new FormControl('', [Validators.required]),
    		'receivedAt'       : new FormControl('', [Validators.required]),
        'type'             : new FormControl('-1',[Validators.required,Validators.min(0)]),
    		'description'      : new FormControl(''),
        'receivedBy'       : new FormControl('', [Validators.required]),
        'image'            : new FormControl(null),
        'imageUrl'         : new FormControl(''),
    		'tampered'         : new FormControl('', [Validators.required]),
    		'smartLockWorking' : new FormControl('', [Validators.required]),
    		'batteryTopWindow' : new FormControl('', [Validators.required]),
    		'socMeterWorking'  : new FormControl('', [Validators.required]),
    		'connectorOK'      : new FormControl('', [Validators.required])
      });
  }

  onFileSelected(event){
    if(event.target.files.length > 0)
    {
      this.disableUploadBtn = true;
      const checkImageExt = this.checkImageExt(event.target.files[0].name);
    
      if(checkImageExt){
        if(event.target.files[0].size > this.maxFileSize){
            this.patchImageValue();
            this.disableUploadBtn = false;
            Swal('Failure!','Please selected file which is less than 10MB','error');
        }
        else
          this.selectedFile = <File>event.target.files[0];  
      }
      else{
        this.patchImageValue();
        this.disableUploadBtn = false;
        Swal('Failure!','Invalid MIME Type,only JPEG,JPG and PNG files are allowed','error');
      }
    }
  }

  
  checkImageExt(fileName){
    const fileExt = fileName.split('.').pop();
    return this.fileAllowedTypes.includes(fileExt);
  }

  patchImageValue(){
    this.checklistForm.patchValue({
            'image'    : null,
            'imageUrl' :''
          });
  }

  zoomImage(){
     Swal({
        title: 'Image Selected !',
        text: 'Recently Uploaded Image',
        imageUrl: this.checklistForm.get('imageUrl').value,
        imageWidth: 500,
        imageHeight: 250,
        imageAlt: 'Battery image',
        animation: false
        });
  }

  uploadFile(){
    if(this.selectedFile){
      const imageData = new FormData();
      imageData.append('image',this.selectedFile,this.selectedFile.name);

      this.common.imageUpload(imageData).subscribe(event => {
        if(event.type === HttpEventType.UploadProgress){
          this.showProgessBar = true;
          this.progPercentage = Math.round(event.loaded / event.total * 100);
        } else if (event.type === HttpEventType.Response){
          this.showProgessBar = false;
          if(event.status == 200 && event.body.response_code == 200){
             this.checklistForm.patchValue({
                'imageUrl' : event.body.response.imageUrl
              });
            Swal({ position: 'center', type: 'success', title: 'File uploaded successfully',
                    showConfirmButton: false,timer: 1500 });
          }
          else{
            Swal('Failure!','Error uploading file','error');
          }
        }
      });
    }
  }

  associatedBatteries(){
  	this.common.associatedBatteries().subscribe((data) => {
  		if(data.response_code == 200){
  		   this.batteryList = data.response;
 		     this.checklistForm.get('batteryId').setValidators(this.validateBattery(this.batteryList));
  		}
  	});
  }

  actors(){
  	this.common.actors().subscribe((data) => {
  		if(data.response_code == 200){
  		   this.actorsList = data.response;
  		   this.checklistForm.get('receivedFrom').setValidators(this.validateActor(this.actorsList));
  		}
  	});
  }

  validateBattery(batteryList: any): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } | null => {
     
       let battery = control.value;
       let flag = 0;

       batteryList.forEach(element => {
         if(battery == element.batteryPackId && flag == 0){
           flag = 1;
         }
       });
        if (flag == 0) {
            return { 'validateBattery': true };
        }
        return null;
    };
  }

  validateActor(actorsList: any): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } | null => {
     
       let actor = control.value;
       let flag = 0;

       actorsList.forEach(element => {
         if(actor == element.actorId && flag == 0){
           flag = 1;
         }
       });
        if (flag == 0) {
            return { 'validateActor': true };
        }
        return null;
    };
  }

  resetForm(){
    this.checklistForm.reset();
    this.initializeForm();
  }

	onSubmit(){
	 if(this.checklistForm.valid) {
	    this.formData = this.checklistForm.value;
      this.common.receiveInChecklist(this.formData).subscribe((data) => {
	      if(data.response_code == 200){
	          Swal('Success!',data.response,'success');  
	          this.resetForm();
	      }
	      else{
	        Swal('Failure!',data.response,'error');
	      }
	    });
	  }
	}

}
