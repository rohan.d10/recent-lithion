export class LspAssetTable {

  public name: string;
  public driverName: string;
  public status: string;
  public starting: string;

constructor(driverName: string, name: string, status: string, starting: string) {

  this.name = name;
  this.driverName = driverName;
  this.status = status;
  this.starting = starting;
}
}
