import { Component } from '@angular/core';
import { AuthenticationService } from '../../services';
import { Router } from '@angular/router';

@Component({
  selector: 'app-aside',
  templateUrl: './app-aside.component.html'
})
export class AppAsideComponent {
  constructor(private authService: AuthenticationService, private router: Router) {}

  public logout() {
    this.authService.logout();
  }
}
