import { Component, OnInit } from '@angular/core';
import { FormControl,FormGroup,Validators, } from '@angular/forms';
import * as models from '../../models';
import { CommonService } from '../../services/common.service';
import Swal from 'sweetalert2';
import { ConfirmPasswordValidator } from './confirm-password.validator';

@Component({
  selector: 'app-register-user',
  templateUrl: './register-user.component.html',
  styleUrls: ['./register-user.component.css']
})
export class RegisterUserComponent implements OnInit {
  registerForm: FormGroup;
  public formData = {};
  public userTypes = ['ADMIN','DRIVER','LSP','OWNER','PARTNER','LSPOP','LITHION','FIELD',
                    'ANALYTICS','MANAGER', 'GUEST'];

  constructor(private common: CommonService) {
   }

  ngOnInit() {
  	this.initializeForm();
  }

  initializeForm() {
    this.registerForm = new FormGroup({
      'contactNumber': new FormControl('', [Validators.required, Validators.pattern("^[0-9]*$"),
      Validators.minLength(10), Validators.maxLength(10)]),
      'userType': new FormControl('-1', [Validators.required, Validators.min(0)]),
      'userName': new FormControl('', Validators.required),
      //'pwdData'           : new FormGroup({
      'password': new FormControl('', [Validators.required]),
      'confirmPassword': new FormControl('', [Validators.required]),
      //	 },ConfirmPasswordValidator.MatchPassword)

    })
  }

  resetForm(){
    this.registerForm.reset();
    this.initializeForm();
  }
  // console.log(this.registerForm.get('pwdData.confirmPassword').hasError('required'));

  onSubmit(){

     if(this.registerForm.valid) {
        this.formData={
          "contactNumber": this.registerForm.get('contactNumber').value,
          "userType"     : this.registerForm.get('userType').value,
          "userName"     : this.registerForm.get('userName').value,
          "password"     : this.registerForm.get('pwdData.password').value,
          "confirm"      : this.registerForm.get('pwdData.confirmPassword').value
      }

        this.common.userRegisterForm(this.formData).subscribe((data) => {
          if(data.response_code == 200){
              Swal('Success!','User Created Successfully! ','success');
              this.resetForm();
          }
          else
            Swal('Failure!',data.response,'error');
        });
      }
  }


}
