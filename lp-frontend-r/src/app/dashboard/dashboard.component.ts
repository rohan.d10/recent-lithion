import { Component, OnInit,ViewChild, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import * as models from '../models';
import * as fromRoot from '../store/reducers';
import { AuthenticationService } from '../services';
import { DataTableDirective } from 'angular-datatables';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent implements OnInit, AfterViewInit {
  loadedfeature = '';
  addAssetView = false;
  public url: string;
  parameters: any;
  reviewAssetCreatedTable = false;
  public currentAssets: models.AssetCurrentInfo[] = [];
  public charging: number;
  public discharging: number;
  public idle: number;
  public length: number;
  public new:number;

  constructor(private route: Router, private store: Store<fromRoot.State>, private authService: AuthenticationService) {
    if (this.authService.isLoggedOut() || this.authService.getUserType() == 'GUEST') {
      this.authService.checkAndRedirect();
    }
    // this.authService.checkAndRedirect();
    this.charging = 0;
    this.discharging = 0;
    this.idle = 0;
    this.length = 0;
    this.new=0;
  }


  ngOnInit() {
    this.lookForUrl();

    this.store.select(fromRoot.getCurrentAssets).subscribe(currAssets => {
      this.currentAssets = currAssets;
      this.charging      = 0;
      this.discharging   = 0;
      this.idle          = 0;
      this.length        = 0;
      
      this.currentAssets.forEach(element => {
        if(String(element.status) != '2'){
          this.length++;
        }
        switch (String(element.status)) {
          case '-1':
          this.discharging++;
          break;
          case '0':
          this.idle++;
          break;
          case '1':
          this.charging++;
          break;
        }
      });
    });
  }

  onNavigate(feature: string) {
    this.loadedfeature = feature;
    this.addAssetView = true;
  }

  lookForUrl() {
    this.url = this.route.url;
  }

  ngAfterViewInit() {}
  onReviewButtonClicked(event) {}
}
