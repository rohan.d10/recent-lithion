import { Injectable } from '@angular/core';
import { HttpClientModule,HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { APIUrls } from '../constants';

@Injectable({
	providedIn: 'root'
})
export class CommonService {
	private driverListFetchUrl          = APIUrls.DRIVER_LIST;
	private dataLoggerUrl               = APIUrls.DATA_LOGGER;
	private lspUrl                      = APIUrls.LSP_LIST;
	private batteryDetails              = APIUrls.BATTERY_DETAIL;
	private saveChecklistUrl            = APIUrls.SAVE_CHECKLIST;
	private saveAccountUrl              = APIUrls.SAVE_PAYMENT;
	private adminRegisterUrl            = APIUrls.REGISTER_ADMIN;
	private cellRegisterUrl             = APIUrls.REGISTER_CELL;
	private chargerRegisterUrl          = APIUrls.REGISTER_CHARGER;
	private reportIssueUrl              = APIUrls.REPORT_ISSUE;
	private lspBatteryAssignedUrl       = APIUrls.REGISTERED_BATTERIES_LSP;
	private editDataLoggerUrl           = APIUrls.EDIT_DATA_LOGGER;
	private lspListedAssetUrl           = APIUrls.FETCH_LSP_DETAILS;
	private lspListedUnassignedAssetUrl = APIUrls.UNASSIGNED_BATTERIES;
	private dataLoggerRegisUrl          = APIUrls.REGISTER_DATALOGGER;
	private dataBmsRegisUrl             = APIUrls.REGISTER_BMS;
	private assignedDriversUrl          = APIUrls.DRIVER_FETCH_ASSIGNED;
	private UnAssignedDriversUrl        = APIUrls.DRIVER_FETCH_UNASSIGNED
	private assetDeassignmentUrl        = APIUrls.ASSET_DEASSIGNMENT;
	private assetAssignmentUrl          = APIUrls.ASSET_ASSIGNMENT;
	private unAssociatedBmsUrl          = APIUrls.UNASSOCIATED_BMS;
	private unAssociatedDlUrl           = APIUrls.UNASSOCIATED_DL;
	private unmappedCellslUrl           = APIUrls.UNMAPPED_CELLS;
	private registerBatteryPackUrl      = APIUrls.REGISTER_BATTERYPACK;
	private earningFeedbackUrl          = APIUrls.EARNING_FEEDBACK;
	private batteryDetailsUrl           = APIUrls.FETCH_BATTERY_DETAILS;
	private removeBmsUrl                = APIUrls.REMOVE_BMS;
	private removeDataloggerUrl         = APIUrls.REMOVE_DATALOGGER;
	private editBatteryPackUrl          = APIUrls.EDIT_BATTERY_PACK;
	private assetHistoryInfoUrl         = APIUrls.ASSET_HISTORY_INFO;
	private thresholdInfoUrl            = APIUrls.THRESHOLD_INFO;
	private lspDefaultLocationUrl       = APIUrls.LSP_LOCATION;
	private getChecklistDataUrl         = APIUrls.VIEW_CHECKLIST;
	private associatedBatteriesUrl      = APIUrls.ASSOCIATED_BATTERIES;
	private actorsUrl                   = APIUrls.ACTORS;
	private receiveInChecklistUrl       = APIUrls.RECEIVE_IN_CHECKLIST;
	private getBmsDataUrl               = APIUrls.BMS_DETAILS;
	private editBmsUrl                  = APIUrls.EDIT_BMS;
	private registerDriverUrl           = APIUrls.REGISTER_DRIVER;
	private registerLspUrl              = APIUrls.REGISTER_LSP;
	private getReceiveChecklistDataUrl  = APIUrls.VIEW_RECEIVE_IN_CHECKLIST;
	private downloadCSVUrl              = APIUrls.DOWNLOAD_CSV;
	private imageUploadUrl              = APIUrls.IMAGE_UPLOAD;
	private getAccountListlUrl          = APIUrls.ACCOUNT_VIEW;
	private fetchNewBatteriesUrl        = APIUrls.GET_NEW_BATTERIES;
	private batteryPrimaryAssignmentUrl = APIUrls.BATTERY_PRIMARY_ASSIGN;
	private partnerGivenBatteriesUrl    = APIUrls.PARTNER_GIVEN_BATTERIES;
	private batterySecondaryAssignmentUrl = APIUrls.BATTERY_SECONDARY_ASSIGN;
	private generateChecksumHashUrl    = APIUrls.GENERATE_CHECKSUM;
  private verifyChecksumUrl = APIUrls.VERIFY_CHECKSUM;
  private customerInteractionUrl = APIUrls.GET_BATTERY_LSP;
  private registerCustomerUrl = APIUrls.REGISTER_CUSTOMER;
  private viewCustomerUrl = APIUrls.VIEW_CUSTOMER;
  private DlId_BId_Url = APIUrls.DLID_BID;
  private getAssetUrl = APIUrls.ASSETLIST;
  private getBatteryDetailsUrl = APIUrls.BATTERYDETAILS;
	private httpOptions;
	private imgOptions;

	constructor(private http: HttpClient) {
	this.httpOptions = {
		headers: new HttpHeaders({
			'Content-Type':  'application/json',
			'Authorization': 'Bearer '+ localStorage.getItem('auth_token')
			})
		};

	  this.imgOptions = {
	  	reportProgress : true,
	  	observe        : 'events'
	  };
	 }

	private extractData(res: Response) {
		let body = res;
		return body || { };
	}

	getDriverProfileData(): Observable<any> {
		return this.http.get(this.driverListFetchUrl).pipe(
			map(this.extractData));
	}

	getLspData(): Observable<any> {
		return this.http.get(this.lspUrl).pipe(
			map(this.extractData));
	}

	fetchAssignedDrivers(): Observable<any> {
		return this.http.get(this.assignedDriversUrl).pipe(
			map(this.extractData));
	}

	fetchUnDriverAssigned(): Observable<any> {
		return this.http.get(this.UnAssignedDriversUrl).pipe(
			map(this.extractData));
	}

	getDataLoggerData(): Observable<any> {
		return this.http.get(this.dataLoggerUrl).pipe(
			map(this.extractData));
	}

	saveChecklist(body): Observable<any> {
		return this.http.post(this.saveChecklistUrl,body,this.httpOptions).pipe(
			map(this.extractData));
	}

	saveAccountForm(body): Observable<any> {
		return this.http.post(this.saveAccountUrl,body,this.httpOptions).pipe(
			map(this.extractData));
	}

	userRegisterForm(body): Observable<any> {
		return this.http.post(this.adminRegisterUrl,body,this.httpOptions).pipe(
			map(this.extractData));
	}

	lspAssignedBatteries(body): Observable<any> {
		return this.http.post(this.lspBatteryAssignedUrl,body,this.httpOptions).pipe(
			map(this.extractData));
	}

	registerCellForm(body): Observable<any> {
		return this.http.post(this.cellRegisterUrl,body,this.httpOptions).pipe(
			map(this.extractData));
	}

	registerChargerForm(body): Observable<any> {
		return this.http.post(this.chargerRegisterUrl,body,this.httpOptions).pipe(
			map(this.extractData));
	}

	registerDataLogger(body): Observable<any> {
		return this.http.post(this.dataLoggerRegisUrl,body,this.httpOptions).pipe(
			map(this.extractData));
	}

	registerBms(body): Observable<any> {
		return this.http.post(this.dataBmsRegisUrl,body,this.httpOptions).pipe(
			map(this.extractData));
	}

	getBatteryData(body): Observable<any> {
		return this.http.post(this.batteryDetails,body,this.httpOptions).pipe(
			map(this.extractData));
	}

	reportIssueForm(body): Observable<any> {
		return this.http.post(this.reportIssueUrl,body,this.httpOptions).pipe(
			map(this.extractData));
	}

	editDataLogger(body): Observable<any> {
		return this.http.post(this.editDataLoggerUrl,body,this.httpOptions).pipe(
			map(this.extractData));
	}

	editBatteryPack(body): Observable<any> {
		return this.http.post(this.editBatteryPackUrl,body,this.httpOptions).pipe(
			map(this.extractData));
	}

	assetDeassignment(body): Observable<any> {
		return this.http.post(this.assetDeassignmentUrl,body,this.httpOptions).pipe(
			map(this.extractData));
	}

	assetAssignment(body): Observable<any> {
		return this.http.post(this.assetAssignmentUrl,body,this.httpOptions).pipe(
			map(this.extractData));
	}

	lspListedUnassignedAssets(body): Observable<any> {
		return this.http.post(this.lspListedUnassignedAssetUrl,body,this.httpOptions).pipe(
			map(this.extractData));
	}

	lspListedAssets(body): Observable<any> {
		return this.http.post(this.lspListedAssetUrl,body,this.httpOptions).pipe(
			map(this.extractData));
	}
	fetchUnassociatedBms() : Observable<any> {
		return this.http.get(this.unAssociatedBmsUrl).pipe(
			map(this.extractData));
	}

	fetchUnassociatedDL() : Observable<any> {
		return this.http.get(this.unAssociatedDlUrl).pipe(
			map(this.extractData));
	}

	unmappedCells() : Observable<any> {
		return this.http.get(this.unmappedCellslUrl).pipe(
			map(this.extractData));
	}

	getAccountList(body) : Observable<any> {
		return this.http.post(this.getAccountListlUrl,body,this.httpOptions).pipe(
			map(this.extractData));
	}

	registerBatteryPack(body): Observable<any> {
		return this.http.post(this.registerBatteryPackUrl,body,this.httpOptions).pipe(
			map(this.extractData));
	}

	earningFeedback(body): Observable<any> {
		return this.http.post(this.earningFeedbackUrl,body,this.httpOptions).pipe(
			map(this.extractData));
	}

	fetchBatteryDetails(body): Observable<any> {
		return this.http.post(this.batteryDetailsUrl,body,this.httpOptions).pipe(
			map(this.extractData));
	}

	removeBms(body): Observable<any> {
		return this.http.post(this.removeBmsUrl,body,this.httpOptions).pipe(
			map(this.extractData));
	}

	removeDatalogger(body): Observable<any> {
		return this.http.post(this.removeDataloggerUrl,body,this.httpOptions).pipe(
			map(this.extractData));
	}

	assetHistoryInfo(body): Observable<any> {
		return this.http.post(this.assetHistoryInfoUrl,body,this.httpOptions).pipe(
			map(this.extractData));
	}

	thresholdInfo(body): Observable<any> {
		return this.http.post(this.thresholdInfoUrl,body,this.httpOptions).pipe(
			map(this.extractData));
	}

	lspDefaultLocation(body): Observable<any> {
		return this.http.post(this.lspDefaultLocationUrl,body,this.httpOptions).pipe(
			map(this.extractData));
	}

	getChecklistData(body): Observable<any> {
		return this.http.post(this.getChecklistDataUrl,body,this.httpOptions).pipe(
			map(this.extractData));
	}

	getReceiveChecklistData(body): Observable<any> {
		return this.http.post(this.getReceiveChecklistDataUrl,body,this.httpOptions).pipe(
			map(this.extractData));
	}

	associatedBatteries() : Observable<any> {
		return this.http.get(this.associatedBatteriesUrl).pipe(
			map(this.extractData));
	}

	actors() : Observable<any> {
		return this.http.get(this.actorsUrl).pipe(
			map(this.extractData));
	}

	fetchNewBatteries() : Observable<any> {
		return this.http.get(this.fetchNewBatteriesUrl).pipe(
			map(this.extractData));
	}

	receiveInChecklist(body): Observable<any> {
		return this.http.post(this.receiveInChecklistUrl,body,this.httpOptions).pipe(
			map(this.extractData));
	}

	getBmsData(body): Observable<any> {
		return this.http.post(this.getBmsDataUrl,body,this.httpOptions).pipe(
			map(this.extractData));
	}

	editBms(body): Observable<any> {
		return this.http.post(this.editBmsUrl,body,this.httpOptions).pipe(
			map(this.extractData));
	}

	registerDriver(body): Observable<any> {
		return this.http.post(this.registerDriverUrl,body,this.httpOptions).pipe(
			map(this.extractData));
	}

	registerLsp(body): Observable<any> {
		return this.http.post(this.registerLspUrl,body,this.httpOptions).pipe(
			map(this.extractData));
	}

	downloadCSV(body): Observable<any> {
		return this.http.post(this.downloadCSVUrl,body,this.httpOptions).pipe(
			map(this.extractData));
	}

	imageUpload(body): Observable<any> {
		return this.http.post(this.imageUploadUrl,body,this.imgOptions).pipe(
			map(this.extractData));
	}

	batteryPrimaryAssignment(body): Observable<any> {
		return this.http.post(this.batteryPrimaryAssignmentUrl,body,this.httpOptions).pipe(
			map(this.extractData));
	}

	batterySecondaryAssignment(body): Observable<any> {
		return this.http.post(this.batterySecondaryAssignmentUrl,body,this.httpOptions).pipe(
			map(this.extractData));
	}

	partnerGivenBatteries(body): Observable<any> {
		return this.http.post(this.partnerGivenBatteriesUrl,body,this.httpOptions).pipe(
			map(this.extractData));
	}

	generateChecksumHash(body): Observable<any> {
		return this.http.post(this.generateChecksumHashUrl,body,this.httpOptions).pipe(
			map(this.extractData));
	}

	verifyChecksum(body): Observable<any> {
		return this.http.post(this.verifyChecksumUrl,body,this.httpOptions).pipe(
			map(this.extractData));
  }
  getCustomerInteractionData(body): Observable<any> {
		return this.http.post(this.customerInteractionUrl,body,this.httpOptions).pipe(
			map(this.extractData));
  }
  registerCustomerData(body): Observable<any> {
		return this.http.post(this.registerCustomerUrl,body,this.httpOptions).pipe(
			map(this.extractData));
  }
  viewCustomerData(body): Observable<any> {
		return this.http.post(this.viewCustomerUrl,body,this.httpOptions).pipe(
			map(this.extractData));
  }

  DlId_BId(body): Observable<any> {
		return this.http.post(this.DlId_BId_Url,body,this.httpOptions).pipe(
			map(this.extractData));
  }

  getAssetList() : Observable<any> {
		return this.http.get(this.getAssetUrl).pipe(
			map(this.extractData));
	}

  // getBatteryDetails(body): Observable<any> {
	// 	return this.http.post(this.getBatteryDetailsUrl,body,this.httpOptions).pipe(
	// 		map(this.extractData));
  // }

  getBatteryDetails(body): Observable<any> {
    return this.http.post(this.getBatteryDetailsUrl, body, this.httpOptions).pipe(
      map(this.extractData));
  }

}
