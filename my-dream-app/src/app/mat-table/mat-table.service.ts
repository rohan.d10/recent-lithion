import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PeriodicElement } from 'src/app/mat-table/element';
import { Observable } from 'rxjs';



@Injectable()

export class MatTableService {

  private _url: string = "/assets/data/elements.json";
  constructor(private http : HttpClient) { }

 getElements(): Observable<PeriodicElement[]>{
 return this.http.get<PeriodicElement[]>(this._url)}


}
 