import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl,FormGroup,Validators } from '@angular/forms';
import { CommonService } from '../../../services/common.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-lsp-payment',
  templateUrl: './lsp-payment.component.html',
  styleUrls: ['./lsp-payment.component.css']
})
export class LspPaymentComponent implements OnInit {
  paymentForm: FormGroup;
  public formData       = {};

  constructor(private common: CommonService) { }

  initializeForm(){
     this.paymentForm = new FormGroup({
        'amount'    : new FormControl(''    , [Validators.required])
      });
  }

  ngOnInit() {
    this.initializeForm();
  }

  onSubmit(){
  	
  }

}
