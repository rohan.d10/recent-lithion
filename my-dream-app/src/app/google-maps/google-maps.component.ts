import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-google-maps',
  templateUrl: './google-maps.component.html',
  styleUrls: ['./google-maps.component.scss']
})
export class GoogleMapsComponent implements OnInit {
  lat: number = 30.6960291;
  lng: number = 76.7534779;
  zoom: number = 16;

  onChoseLocation(event) {
    this.lat = event.coords.lat;
    this.lng = event.coords.lng;
  }
  // 'https://maps.google.com/mapfiles/kml/shapes/parking_lot_maps.png'
  // latt: number = 28.737324;
  // lngg: number = 77.090981;
  ngOnInit() {}
  constructor() {}
}

//   markerIconUrl()

//     {
//     return require('src/assets/images/battery32.png');}
// }
