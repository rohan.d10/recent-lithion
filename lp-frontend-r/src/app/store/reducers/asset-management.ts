import * as AssetManagementAction from '../actions/asset-management';
import { AssetTable } from '../../models';
import { AssetCurrentInfo } from '../../models';

/**
 * Each reducer module must import the local `State` which it controls.
 *
 * Here the `State` contains two properties:
 * @prop [asset: AssetTable[]] Adds Asset Table
 *
 */
export interface State {
  assets: AssetTable[];
  currentAssets: AssetCurrentInfo[];
  isLoading: boolean;
}

/**
 * There is always a need of initial state to be passed onto the store.
 *
 * @prop: asset: Set of Asset Table quantities
 */
export const initialState: State = {
  assets: [
    new AssetTable('1', 'A06', 'BP04', 'BAT', '16S', 'Not Working', '03-03-2018'),
    new AssetTable('2', 'A07', 'N004', 'BAT', '16S', 'Working', '03-03-2018'),
    new AssetTable('3', 'A08', 'BP05', 'BAT', '16S', 'Working', '03-03-2018'),
    new AssetTable('4', 'A09', 'NC01', 'BAT', '16S', 'Working', '03-03-2018')
  ],
  currentAssets: [],
  isLoading: false
};

/**
 * The actual reducer function. Reducers can be thought of as the tables in the DataBase.
 * These are the functions which are responsible for maintaing, and updating the
 * `State` of the application they control.
 *
 * Here the reducer cotrols that part of the state which is shows the state of the application
 * wheather it is searching and what is it searching for.
 */
export function reducer(state = initialState, action: AssetManagementAction.Actions): State {
  switch (action.type) {
    case AssetManagementAction.ActionTypes.CREATE_ASSET: {
      const asset = action.payload;
      return {
        ...state,
        assets: [...state.assets, asset]
      };
    }
    case AssetManagementAction.ActionTypes.FETCH_ASSET_SUCCESS: {
      const currentAsset = action.payload;
      return {
        ...state,
        currentAssets: currentAsset,
        isLoading: false
      };
    }

    case AssetManagementAction.ActionTypes.FETCH_ASSET: {
      const currentAsset = action.payload;
      return {
        ...state,
        isLoading: true
      };
    }
    default: {
      return state;
    }
  }
}

/**
 * Because the data structure is defined within the reducer it is optimal to
 * locate our selector functions at this level. If store is to be thought of
 * as a database, and reducers the tables, selectors can be considered the
 * queries into said database. Remember to keep your selectors small and
 * focused so they can be combined and composed to fit each particular
 * use-case.
 */

export const getAssets = (state: State) => state.assets;
export const getCurrentAssets = (state: State) => state.currentAssets;
export const getIsLoading = (state: State) => state.isLoading;
