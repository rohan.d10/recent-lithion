import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RegisterDriver } from './../models/register-driver.model';

import * as constants from '../constants';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { tap } from 'rxjs/operators';
import { APIUrls } from '../constants';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    Authorization: 'my-auth-token'
  })
};

@Injectable()
export class RegisterDriverService {
  constructor(private _http: HttpClient) {}

  private regUrl = APIUrls.REGISTER_DRIVER;
  // private regUrl = 'http://localhost:8000/api/dev/registerDriver';
  public registerDriver(driver: RegisterDriver): Observable<RegisterDriver> {
    return this._http.post<RegisterDriver>(this.regUrl, driver, httpOptions);
  }
}
