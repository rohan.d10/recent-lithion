export class DriverAssign {
  assetid: string;
  driverid: string;
}
export class UnAssignedBatteriesResponse {
  response_code: number;
  response: UnAssignedBatteries[];
}
export class UnAssignedBatteries {
  lspId: string;
  batteryPackId: string;
  driverAssignmentStatus: number;
}
