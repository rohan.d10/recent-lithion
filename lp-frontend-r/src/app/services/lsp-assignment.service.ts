import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import * as constants from '../constants';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { tap } from 'rxjs/operators';
import { AllocateAssetPostParam, AllocateAssetResponse } from '../models';
import { APIUrls } from '../constants';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    Authorization: 'my-auth-token'
  })
};

const postOption = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable()
export class AllocateAssets {
  constructor(private _http: HttpClient) {}

  private postUrl = APIUrls.PRIMARY_ASSIGNMENT;

  public AllocateAssetsMultiple(postParam: AllocateAssetPostParam): Observable<AllocateAssetResponse> {
    return this._http.post<AllocateAssetResponse>(this.postUrl, postParam, postOption);
  }
}
