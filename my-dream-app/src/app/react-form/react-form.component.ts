import { Component, OnInit } from '@angular/core';
import {
  NgbDatepickerConfig,
  NgbCalendar,
  NgbDate,
  NgbDateStruct
} from '@ng-bootstrap/ng-bootstrap';
import {
  FormGroup,
  FormControl,
  FormBuilder,
  Validators
} from '@angular/forms';

@Component({
  selector: 'app-react-form',
  templateUrl: './react-form.component.html',
  providers: [NgbDatepickerConfig],
  styleUrls: ['./react-form.component.scss']
})
export class ReactFormComponent implements OnInit {
  registerForm: FormGroup;
  model: NgbDateStruct;
  public minDate;
  public val: boolean;
  public oldFromDate;
  public today = new Date();
  myDate = new Date();
  constructor(private formBuilder: FormBuilder, calendar: NgbCalendar) {}

  ngOnInit() {
    this.registerForm = new FormGroup({
      firstName: new FormControl(null, Validators.required),
      lastName: new FormControl(null, Validators.required),
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [
        Validators.required,
        Validators.minLength(6)
      ]),
      fromDate: new FormControl(null, Validators.required),
      toDate: new FormControl(null, Validators.required)
    });

    this.minDate = { year: 2019, month: 1, day: 2 };
    this.val = true;
  }
  onSubmit() {
    console.log(this.registerForm);
  }

  setToDate() {
    this.registerForm.controls.toDate.setValue(null);
    this.minDate = this.registerForm.controls.fromDate.value;
    this.val = false;
  }

  getEmailErrorMessage() {
    return this.registerForm.get('email').hasError('required')
      ? 'You must enter e-mail'
      : this.registerForm.get('email').hasError('email')
      ? 'Not a valid email'
      : '';
  }
}
