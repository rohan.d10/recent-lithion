import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { tap } from 'rxjs/operators';

import * as constants from '../constants';
import { DriverCurrentInfoResponse, DriverCurrentInfoSingleResponse } from '../models';
import { APIUrls } from '../constants';

@Injectable()
export class FetchDriverService {
  private DriverFetchUrl = APIUrls.FETCH_DRIVER_CURRENT_CYCLE_DETAILS;

  constructor(private http: HttpClient) {}

  public fetchDriverCurrentInfo(driverid: string): Observable<DriverCurrentInfoSingleResponse> {
    return this.http.get<DriverCurrentInfoSingleResponse>(this.DriverFetchUrl + '?driverid=' + driverid);
  }

  public logError() {
    console.log('error occurred');
  }
}
