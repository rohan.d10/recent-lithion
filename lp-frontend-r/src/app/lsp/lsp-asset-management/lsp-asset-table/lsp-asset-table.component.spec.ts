import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LspAssetTableComponent } from './lsp-asset-table.component';

describe('LspAssetTableComponent', () => {
  let component: LspAssetTableComponent;
  let fixture: ComponentFixture<LspAssetTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LspAssetTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LspAssetTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
