export class RegisterDriver {

  public name: String;
  public driverId: string ;
  public addressPerm: string;
  public addressCurr: string;
  public phone: string;
  public adharNumber: string;
  public licenseNum: string;
  public accNumber: string;
  public bankName: string;
  public bankAddress: string;
  public bankIFSC: string;
  public pan: string;
  public dob: string;
  public fatherName: string;
  public motherName: string;
  public spouseName: string;
  public noOfChildren: string;
  public vType: string;
  public financeBy: string;
  public financeAmount: string;
  public dailySameVehicle: string;
  public vehicleNo: string;
  public motor: string;
  public make: string;
  public controller: string;
  public ageOfVehicle: string;
  public batteryType: string;
  public batterySupplier: string;
  public lastPurchasedDate: string;
  public genDrivingTime: string;
  public whenCharged: string;
  public whereCharged: string;
  public dailyKm: string;
  public expDriving: string;
  public estDailyEarning: string;
// for optional arguments we can add '?' in front of the variable name
}
