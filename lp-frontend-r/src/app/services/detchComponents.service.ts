import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import * as constants from '../constants/serviceConstants';
import { Observable } from 'rxjs';
import { detachResponse } from '../models';

const postOption = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable()
export class DetachComponentService {
  private url = constants.APIUrls.DETACH_COMPONENT;
  constructor(private httpClient: HttpClient) {}
  public detachDatalogger(batteryId: string, dlId: string): Observable<detachResponse> {
    return this.httpClient.post<detachResponse>(
      this.url,
      {
        batteryPackId: batteryId,
        component_type: 'dl',
        component_id: dlId
      },
      postOption
    );
  }

  public detachBms(batteryId: string, bmsId: string): Observable<detachResponse> {
    return this.httpClient.post<detachResponse>(
      this.url,
      {
        batteryPackId: batteryId,
        component_type: 'bms',
        component_id: bmsId
      },
      postOption
    );
  }
}
