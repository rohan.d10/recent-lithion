export class ReportIssue {
  public issueType: string;
  public issue: string;
  public createdBy: string;
}

export class ReportIssueResponse {
  public status_code: number | string;
  public issue_token: string;
}
