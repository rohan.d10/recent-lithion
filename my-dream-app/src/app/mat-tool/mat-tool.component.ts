import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from '@angular/forms';

@Component({
  selector: 'app-mat-tool',
  templateUrl: './mat-tool.component.html',
  styleUrls: ['./mat-tool.component.scss']
})
export class MatToolComponent implements OnInit {
  ngOnInit() {}

  email = new FormControl('', [Validators.required, Validators.email]);
  name = new FormControl('', [Validators.required]);
  username = new FormControl('', [Validators.required]);
  password = new FormControl('', [Validators.required]);

  getErrorMessage() {
    return this.email.hasError('required')
      ? 'You must enter a value'
      : this.email.hasError('email')
      ? 'Not a valid email'
      : this.name.hasError('required')
      ? 'You must enter a value'
      : this.username.hasError('required')
      ? 'You must enter a value'
      : this.password.hasError('required')
      ? 'You must enter a value'
      : '';
  }
  hide = true;

  gender: FormGroup;
  constructor(fb: FormBuilder) {
    this.gender = fb.group({
      formGender: 'male'
    });
  }
}
