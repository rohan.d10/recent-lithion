import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthenticationService, ResetPasswordService } from '../services';
import { Title } from '@angular/platform-browser';
import { Store } from '@ngrx/store';
import * as fromRoot from '../store/reducers';
import { RemoveTokenActionSuccess } from '../store/actions/token-management';
import { NgForm } from '@angular/forms';
import { ToasterService } from 'angular2-toaster';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  @ViewChild('newPasswordForm') public newPasswordForm: NgForm;
  username: string;
  password: string;
  otp: string;
  otpPhoneNum: string;
  usernameFgt: string;
  newPassword: string;
  ReNewPassword: string;

  public fgtUserLoading: boolean = false;
  public otpLoading: boolean = false;
  public invalidUsername: boolean = false;
  public passwordMatches: boolean = true;
  public otpMatches: boolean = true;
  public newPwdLoading: boolean = false;

  public showLogin = true;
  public showConfirmOtp = false;
  public showResetUsername = false;
  public showNewPassword = false;

  error: string;
  public isLoading: Boolean;

  public spinner: string = 'hide';
  public labelClass: string = 'show';
  constructor(
    private titleService: Title,
    private authenticationService: AuthenticationService,
    private resetPasswordService: ResetPasswordService,
    private store: Store<fromRoot.State>,
    private toasterService: ToasterService,
  ) {
    
    this.store.dispatch(new RemoveTokenActionSuccess());
  }

  // check double reload
  ngOnInit() {
    this.spinner = 'hide';
    this.labelClass = 'show';

    this.authenticationService.log_saved();
    if (this.authenticationService.isLoggedIn) {
      this.authenticationService.checkAndRedirect();
    }
    this.titleService.setTitle('Lithion - Login');
  }

  public otpConfirmation() {
    this.otpLoading = true;
    this.otpMatches = true;
    this.resetPasswordService.CheckOtp(this.usernameFgt, this.otp).subscribe(data => {
      if (data.response_code === 200) {
        this.showConfirmOtp = false;
        this.showNewPassword = true;
      } else {
        this.otpMatches = false;
      }
      this.otpLoading = false;
    });
  }

  private resetParams() {
    this.password = '';
    this.otp = '';
    this.username = '';
    this.usernameFgt = '';
    this.newPassword = '';
    this.ReNewPassword = '';
  }
  public resetPassword() {
    this.newPwdLoading = true;
    this.passwordMatches = true;
    if (this.newPasswordForm.valid) {
      this.resetPasswordService.ConfirmOtp(this.usernameFgt, this.otp, this.newPassword).subscribe(data => {
        if (data.response_code === 200) {
          this.toasterService.pop(
            'success',
            'Password reset successfull',
            'Password is changed for user' + this.usernameFgt
          );
        } else {
          this.toasterService.pop('error', 'password reset failed', 'Password is not changed!!');
        }
        this.showNewPassword = false;
        this.newPwdLoading = false;
        this.showLogin = true;
      });
    } else if (this.newPassword != this.ReNewPassword) {
      this.passwordMatches = false;
      this.newPwdLoading = false;
    } else {
      this.newPwdLoading = false;
    }
  }
  sendOtp() {
    this.fgtUserLoading = true;
    this.invalidUsername = false;
    this.resetPasswordService.RequestOtp(this.usernameFgt).subscribe(data => {
      if (data.response_code === 200) {
        this.showConfirmOtp = true;
        this.showResetUsername = false;
        this.fgtUserLoading = false;
        this.otpPhoneNum = data.response.phone.substr(0, 2) + 'XXXXXX' + data.response.phone.substr(8, 2);
      }
      this.invalidUsername = true;
      this.fgtUserLoading = false;
    });
  }

  // todo

  login() {
    this.spinner = 'show';
    this.labelClass = 'hide';
    this.authenticationService.login(this.username, this.password).subscribe(
      data => {
        this.spinner    = 'hide';
        this.labelClass = 'show';
        this.authenticationService.checkAndRedirect();
        console.log(data);
      },
      LoginError => {
        if (LoginError.error.message) {
          this.error = LoginError.error.message + ' : verify your username or password !';
        } else {
          this.error = "Can't connect to network";
        }
        this.spinner = 'hide';
        this.labelClass = 'show';
      }
    );
  }
}
