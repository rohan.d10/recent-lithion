export class RegisterLSP {

  public lspId: string ;
  // Compant Details
  public name: String;
  public partnerName: String;
  public address: string;
  public phone: string;
  public gst: string;
  public accNum: string;
  public ifsc: string;
  public pan: string;
  public dateOfSetup: string;
  public transactionType: string;
  public totalArea: string;
  public sourceOfPow: string;
  public lat: string;
  public lon: string;
  // Asset Details
  public numOfBattery: string;
  public numOfCharger: string;
  // security Details
  public secDeposit: string;
  public secDepCollected: string;
  public depositedBy: string;
  public isPartialDep: string;
  public depAmount: string;
  public emiTenure: string;
  public emiAmount: string;
  //  LSP operator details
  public noOfOperator: string;
  public nameOfOperators: string;
// for optional arguments we can add '?' in front of the variable name
}
