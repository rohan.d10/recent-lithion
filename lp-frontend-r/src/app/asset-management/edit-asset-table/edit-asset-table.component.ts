import { Component, OnInit } from '@angular/core';
import { RestService } from '../../services';
import { FormControl,FormGroup,Validators,AbstractControl,ValidatorFn } from '@angular/forms';
import * as models from '../../models';
import { CommonService } from '../../services/common.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-edit-asset-table',
  templateUrl: './edit-asset-table.component.html',
  styleUrls: ['./edit-asset-table.component.scss']
})
export class EditAssetTableComponent implements OnInit {
  public fetchBatteryId: string;
  batteryForm  : FormGroup;
  public batteryInfo = [];
  public bmsList     = [];
  public dlList      = [];
  public formData    = {};
  public showBmsList = false;
  public showDlList  = false;

  constructor(private common: CommonService) {}
  ngOnInit() {}
  
  fetchBatteryDetails(){
      this.common.fetchBatteryDetails({ batteryId:this.fetchBatteryId }).subscribe((data) => {
          if(data.response_code == 200){
            this.batteryInfo = data.response[0];
              this.batteryForm = new FormGroup({
                  'batteryId'              : new FormControl(this.fetchBatteryId, Validators.required),
                  'associatedBmsId'        : new FormControl(this.batteryInfo['associatedBmsId'],
                                                      [Validators.required]),
                  'associatedDataloggerId' : new FormControl(this.batteryInfo['associatedDataloggerId'],
                                            [Validators.required]),
                  'cabinetVersion'         : new FormControl(this.batteryInfo['cabinetVersion']),
                  'vendor'                 : new FormControl(this.batteryInfo['cellVendorName']),
                  'updatedBy'              : new FormControl(this.batteryInfo['updatedBy'], Validators.required),
                  'updatedAt'              : new FormControl(this.batteryInfo['lastUpdateAt'], Validators.required),
            });
            this.showBmsList = false;
            this.showDlList = false;
            if(!this.batteryInfo['associatedBmsId']){
              this.fetchUnassociatedBms();
            }  

            if(!this.batteryInfo['associatedDataloggerId']){
              this.fetchUnassociatedDL();
            }
          }
        });
    }

  ValidateDl(dlList: any): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } | null => {
     
       let datalogger = control.value;
       let flag = 0;

       dlList.forEach(element => {
         if(datalogger == element.dataLoggerId && flag == 0){
           flag = 1;
         }
       });
        if (flag == 0) {
            return { 'validateDlList': true };
        }
        return null;
    };
  }

  ValidateBms(bmsList: any): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } | null => {
     
       let bms  = control.value;
       let flag = 0;

       bmsList.forEach(element => {
         if(bms == element.bmsId && flag == 0){
           flag = 1;
         }
       });
        if (flag == 0) {
            return { 'validateBmsList': true };
        }
        return null;
    };
  }


  fetchUnassociatedBms(){
    this.common.fetchUnassociatedBms().subscribe((data) => {
       if(data.response_code == 200){
          this.showBmsList = true;
          this.bmsList     = data.response;
          this.batteryForm.patchValue({
              'associatedBmsId': '',
            });
          this.batteryForm.get('associatedBmsId').setValidators(this.ValidateBms(this.bmsList));
       }
    });
  }

  fetchUnassociatedDL(){
    this.common.fetchUnassociatedDL().subscribe((data) => {
       if(data.response_code == 200){
          this.showDlList = true;
          this.dlList     = data.response;

          this.batteryForm.patchValue({
              'associatedDataloggerId': '',
            });
          this.batteryForm.get('associatedDataloggerId').setValidators(this.ValidateDl(this.dlList));
       }
    });
  }

  removeBms(bmsId){
    this.common.removeBms({batteryId:this.fetchBatteryId,bmsId:bmsId}).subscribe((data) => {
      if(data.response_code == 200){
         Swal('Success!',data.response,'success'); 
         this.fetchUnassociatedBms(); 
      }
    });
  }

  removeDatalogger(dataLoggerId){
     this.common.removeDatalogger({batteryId:this.fetchBatteryId,dataLoggerId:dataLoggerId}).subscribe((data) => {
      if(data.response_code == 200){
         Swal('Success!',data.response,'success'); 
         this.fetchUnassociatedDL(); 
      }
    });
  }

   resetForm(){
    this.batteryForm.reset();
  }

  onSubmit(){
    if(this.batteryForm.valid) {
        this.formData = this.batteryForm.value;
        this.common.editBatteryPack(this.formData).subscribe((data) => {
          if(data.response_code == 200){
              this.batteryInfo = this.bmsList = this.dlList = [];
              Swal('Success!',data.response,'success');  
              this.resetForm();
          }
          else{
            Swal('Failure!',data.response,'error');
          }
        });
      }
  }
 

}
