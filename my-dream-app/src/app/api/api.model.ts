export interface TotalUser {
  response: User[];
  response_code: number;
}

export interface User {
  _id: string;
  driver_name: string;
  assetId: string;
  driverId: string;
  KM_driven: number;
  startAh: number;
  usedAh: number;
  AH_left: number;
  KM_left: number;
  pEarn: number;
  earnedCurrCycle: number;
  createdAt: string;
  updatedAt: string;
}
