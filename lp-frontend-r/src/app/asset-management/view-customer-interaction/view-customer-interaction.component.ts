import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { FormControl,FormGroup,Validators } from '@angular/forms';
import { CommonService } from '../../services/common.service';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
@Component({
  selector: 'app-view-customer-interaction',
  templateUrl: './view-customer-interaction.component.html',
  styleUrls: ['./view-customer-interaction.component.css']
})
export class ViewCustomerInteractionComponent implements OnInit, OnDestroy {

  constructor(private common: CommonService) { }
  public customerData = [];
  public maxDate = new Date();
  public date;
  public driverId;
  public customerForm : FormGroup;
  tableHeaders = [
    'Driver',
    'LSP',
    'Battery ID',
    'Date',
    'Hours Driven',
    'KM Driven',
    'Earnings',
    'No. of Times Charged',
    'Attended By',
    'Issues'
  ];


  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  @ViewChild(DataTableDirective) dtElement: DataTableDirective;

  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10
    };
    this.initializeForm();
    this.getCustomerData();
  }

  initializeForm(){
    this.customerForm = new FormGroup({
       'date'   : new FormControl(''),
       'driverId' : new FormControl('')
     });
 }
  getCustomerData() {
    this.common.viewCustomerData({ driverId:this.driverId , date:this.date}).subscribe(res => {

     this.customerData = res.data;
      this.dtTrigger.next();

    })
  }

  onSubmit(){
    if(this.customerForm.valid){
      this.date   = this.customerForm.get('date').value;
      this.driverId = this.customerForm.get('driverId').value;


      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.destroy();
     });

      this.getCustomerData();
    }
  }

  ngOnDestroy() {
    this.dtTrigger.unsubscribe();
  }
}
