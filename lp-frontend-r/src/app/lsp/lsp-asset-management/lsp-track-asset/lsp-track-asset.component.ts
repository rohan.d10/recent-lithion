import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lsp-track-asset',
  templateUrl: './lsp-track-asset.component.html',
  styleUrls: ['./lsp-track-asset.component.scss']
})
export class LspTrackAssetComponent implements OnInit {

  public title: string;
  public id: string;
  public lng: string;
  public lat: string;
  public zoom: string;
  public assets: string;
  constructor() { }

  ngOnInit() {
  }

}
