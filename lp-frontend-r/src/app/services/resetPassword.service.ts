import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { APIUrls } from '../constants';
import { Observable } from 'rxjs';
import { RequestOtpResponse, CheckOtpResponse, ConfirmOtpResponse } from '../models';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    Authorization: 'my-auth-token'
  })
};

@Injectable()
export class ResetPasswordService {
  constructor(private httpClient: HttpClient) {}

  private requestOtp = APIUrls.REQUEST_OTP;
  private checkOtp = APIUrls.CHECK_OTP;
  private confirmOtp = APIUrls.CONFIRM_OTP;

  public RequestOtp(actorid: string): Observable<RequestOtpResponse> {
    return this.httpClient.post<RequestOtpResponse>(this.requestOtp, { actorid: actorid }, httpOptions);
  }

  public CheckOtp(actorid: string, otp: string): Observable<CheckOtpResponse> {
    return this.httpClient.post<CheckOtpResponse>(this.checkOtp, { actorid: actorid, otp: otp }, httpOptions);
  }

  public ConfirmOtp(actorid: string, otp: string, password: string): Observable<ConfirmOtpResponse> {
    return this.httpClient.post<ConfirmOtpResponse>(
      this.confirmOtp,
      { actorid: actorid, password: password, otp: otp },
      httpOptions
    );
  }
}
