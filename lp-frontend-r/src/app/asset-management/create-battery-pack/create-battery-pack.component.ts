import { Component, OnInit } from '@angular/core';
import { FormControl,FormGroup,Validators } from '@angular/forms';
import * as models from '../../models';
import { CommonService } from '../../services/common.service';
import Swal from 'sweetalert2';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-create-battery-pack',
  templateUrl: './create-battery-pack.component.html',
  styleUrls: ['./create-battery-pack.component.css']
})
export class CreateBatteryPackComponent implements OnInit {
  batteryForm             : FormGroup;
  public cellVendors      = ['Kelvin', 'CALB', 'Nancy'];
  public numberofCells    = [16,8,4];
  public formData         = {};
  public bmsList          = [];
  public dlList           = [];
  public dropdownList:any = [];
  public dropdownSettings = {};
  public selectedItems    = [];
  public limit = 16;
  public numb_of_cells = null;


  constructor(private common: CommonService,private spinnerService: Ng4LoadingSpinnerService) {

  }

  ngOnInit() {
    this.initializeForm();
  }

  initializeForm(){
    this.fetchUnassociatedDL();
    this.fetchUnassociatedBms();
    this.unmappedCells();
    this.dropDownSettings();
    this.selectedItems = [];
    this.batteryForm = new FormGroup({
        'cellVendor'     : new FormControl('-1', [Validators.required,Validators.min(0)]),
        'bms'            : new FormControl('-1', [Validators.required,Validators.min(0)]),
        'dataLogger'     : new FormControl('-1', [Validators.required,Validators.min(0)]),
        'cabinetVersion' : new FormControl('', Validators.required),
        'numOfCells'     : new FormControl('', Validators.required),
        'cells'          : new FormControl([],[Validators.required,Validators.min(1), Validators.minLength(this.limit)])
      });
  }

  dropDownSettings(){
    this.dropdownSettings = {
                              singleSelection    : false,
                              text               : "Select Cells",
                              selectAllText      : 'Select All',
                              unSelectAllText    : 'UnSelect All',
                              enableSearchFilter : true,
                              classes            : "myclass custom-class",
                              primaryKey         : "cellId",
                              labelKey           : "cellId",
                              limitSelection     : this.limit
                            };

  }

  private unmappedCells(){
    this.common.unmappedCells().subscribe(data => {
      if(data.response_code == 200)
        this.dropdownList = data.response;
      else
        this.dropdownList = [];
    });
  }

  private fetchUnassociatedBms(){
    this.common.fetchUnassociatedBms().subscribe(data => {
      if(data.response_code == 200)
        this.bmsList = data.response;
      else
        this.bmsList = [];
    });
  }

  private fetchUnassociatedDL(){
    this.common.fetchUnassociatedDL().subscribe(data => {
      if(data.response_code == 200)
        this.dlList = data.response;
      else
       this.dlList = [];
    });
  }

  private changedNumber(){
    this.selectedItems = [];
    this.limit         = this.batteryForm.get('numOfCells').value;
    this.batteryForm.get('cells').setValidators(Validators.minLength(this.limit));
    this.dropDownSettings();
  }

  resetForm(){
    this.batteryForm.reset();
    this.initializeForm();
  }


  onSubmit() {
    if (this.batteryForm.valid) {
     this.spinnerService.show();
      this.common.registerBatteryPack(this.batteryForm.value).subscribe(data => {
           if(data.response_code == 200){
              this.spinnerService.hide();
              Swal(data.data,data.response,'success');
              this.resetForm();
           }
           else{
             this.spinnerService.hide();
             Swal('Failure!',data.response,'error');
           }
      });
    }
  }

  ngOnDestroy() {}
}
