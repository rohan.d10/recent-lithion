import { Observable } from 'rxjs';
import { DataSource } from '@angular/cdk/collections';
import { User } from './battery.model';
import { CommonService } from '../../services/common.service';
import { ActivatedRoute } from '@angular/router';

export class BatteryDataSource extends DataSource<any> {
  constructor(private common: CommonService, private p: string) {
    super();
  }
  public id;
  connect(): Observable<User[]> {
    return this.common.getBatteryDetails({battery: this.p , limit:10});
  }
  disconnect() {}
}
