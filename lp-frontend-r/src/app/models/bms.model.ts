export class Bms {
  bmsId: string;
  version: string;
}
export class BmsResponse {
  response_code: any;
  response: any;
  error: string;
}

export class FetchBmsResponse {
  response_code: number;
  response: SingleBmsResponse[];
}

export class SingleBmsResponse {
  bmsId: string;
  version: string;
  isAssociated: string;
  isActive: string;
  createdAt: string;
}
