import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MapDataService } from '../../services/mapData.service';
import { AssetRecentPathService } from '../../services/assetRecentPath.service';
import { AssetRecentPathLocation } from '../../models';
import { HttpClient } from '@angular/common/http'
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';


@Component({
  selector: 'app-recentpath',
  templateUrl: './recentpath.component.html',
  styleUrls: ['./recentpath.component.scss']
})
export class RecentpathComponent implements OnInit {
  batteryPackId: string;
  openedWindow = -1;
  public locations: AssetRecentPathLocation[];
  public isLocationsAvailable: boolean;
  private lat: any;
  private lon: any;
  public countArr = [];
  public places = [];
  public _url = '';
  public SnappedPoints = [];
  public SnappedLatLon: Array<any> = [];
  public str = '';
  public snap_url = '';
  public Ilatitude = 28.457766;
  public Ilongitude = 77.0712333;
  public zoom = 12;
  public index = 0;
  public comSum = 0;
  public snapUrlJoin: string;
  public paths: Array<any> = [];
  public dataLen = 0;
  public finalArray = [];
  public sum = 0;
  public pic_url: Array<any> = [];
  constructor(private route: ActivatedRoute,private spinnerService: Ng4LoadingSpinnerService, private assetPath: AssetRecentPathService, private _mapDataService: MapDataService,
    private http: HttpClient) { }
  ngOnInit() {
    this.batteryPackId = this.route.snapshot.params['id'];
    this.getLithionData();

  }
  getLithionData() {
    this.spinnerService.show();
   // this._url = this._url + '?assetId=' + this.batteryPackId + '&date=2019-01-24';
    this._url = 'https://app.lithion.in/V2/api/location/recent?assetId=' + this.batteryPackId + '&date=2019-01-23';
    this._mapDataService.getData(this._url).subscribe(data => {
      this.spinnerService.hide();
      if (data.response_code == 200) {
        console.log(data.response.length)
        for (let i = 0; i < data.response.length; i++){
          if (i == 0) {
            this.paths.push({
              latitude: parseFloat(data.response[i].latitude),
              longitude: parseFloat(data.response[i].longitude),
              url: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=' +
                (1) +
                '|32CD32|000000'
            });
          }
         else if (i != 0 && i != data.response.length - 1) {
            this.paths.push({
              latitude: parseFloat(data.response[i].latitude),
              longitude: parseFloat(data.response[i].longitude)
            });
          }
          else {
            this.paths.push({
              latitude: parseFloat(data.response[i].latitude),
              longitude: parseFloat(data.response[i].longitude),
              url: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=' +
                (i+1)+
                '|FF0000|000000'
            });
          }
        }

        // this.paths[this.paths.length - 1].push({
        //   url: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=' +
        //               (this.paths.length) +
        //               '|FF0000|000000'
      //  this.filterLatLon();
      console.log(this.paths);
      }
      });
}

  // filterLatLon() {
  //   for (let i = 1; i < this.paths.length; i++) {
  //     if (this.paths[i].lat == 0 || this.paths[i].lon == 0) {
  //       this.paths.splice(i, 1);
  //       i = i - 1;
  //     }
  //     if (
  //       this.paths[i].lat == this.paths[i - 1].lat &&
  //       this.paths[i].lon == this.paths[i - 1].lon
  //     ) {
  //       this.paths.splice(i, 1);
  //       i = i - 1;
  //     }
  //   }
  //   this.makeSnapToRoadURL();
  // }

  // makeSnapToRoadURL() {
  //   let i = 0;
  //   let z = 0;

  //   while (z != this.paths.length) {

  //     for (; i < z + 99; i = i + 1) {
  //       if (i < this.paths.length - 1) {
  //         if (i == z + 98) {
  //           this.str = this.str + this.paths[i].lat + ',' + this.paths[i].lon;
  //         } else {
  //           this.str = this.str + this.paths[i].lat + ',' + this.paths[i].lon + '|';
  //         }
  //       }
  //       if (i == this.paths.length - 1) {
  //         this.str = this.str + this.paths[i].lat + ',' + this.paths[i].lon;
  //         break;
  //       }
  //     }
  //     this.snap_url =
  //       'https://roads.googleapis.com/v1/snapToRoads?path=' +
  //       this.str +
  //       '&interpolate=true&key=AIzaSyDd91n_BIQk7CBMOGq3rgW_lLXIl_LdxBQ';
  //     //  console.log(this.snap_url);
  //     this.getSnappedPoints();
  //     this.str = '';
  //     this.snap_url = '';
  //     z = i + 1;
  //   }
  // }
  // getSnappedPoints() {
  //   this._mapDataService.getData(this.snap_url).subscribe(data => {
  //     this.SnappedLatLon = [];
  //     this.comSum= this.sum
  //     this.sum = data.snappedPoints.length + this.sum;

  //     for (let j = 0; j < data.snappedPoints.length; j++) {

  //       if (j == 0) {
  //         this.SnappedLatLon.push({
  //           latitude: data.snappedPoints[j].location.latitude,
  //           longitude: data.snappedPoints[j].location.longitude,
  //           url:
  //           'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=' +
  //             (this.comSum+j+1) +
  //             '|32CD32|000000'
  //         });
  //       }
  //       if (j == data.snappedPoints.length - 1) {
  //         this.SnappedLatLon.push({
  //           latitude: data.snappedPoints[j].location.latitude,
  //           longitude: data.snappedPoints[j].location.longitude,
  //           url:
  //             'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=' +
  //             this.sum +
  //             '|FF0000|000000'
  //         });
  //       }
  //       if (j != data.snappedPoints.length - 1 && j != 0) {
  //         this.SnappedLatLon.push({
  //           latitude: data.snappedPoints[j].location.latitude,
  //           longitude: data.snappedPoints[j].location.longitude,
  //           url:
  //             'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=' +
  //             (this.comSum + j+1) +
  //             '|FFFF33|000000'
  //         });
  //       }
  //     }
  //     console.log(this.SnappedLatLon);

  //     for (let j = 0; j < this.SnappedLatLon.length; j++) {
  //       if (j == 0) {
  //         this.finalArray.push({
  //           lat: this.SnappedLatLon[j].latitude,
  //           lon: this.SnappedLatLon[j].longitude,
  //           url: this.SnappedLatLon[j].url
  //         })
  //       }
  //       if(j!=0 && j!= this.SnappedLatLon.length-1)
  //       this.finalArray.push({
  //         lat: this.SnappedLatLon[j].latitude,
  //         lon: this.SnappedLatLon[j].longitude,
  //         url: this.SnappedLatLon[j].url
  //       })
  //       if (j == this.SnappedLatLon.length - 1)
  //       this.finalArray.push({
  //         lat: this.SnappedLatLon[j].latitude,
  //         lon: this.SnappedLatLon[j].longitude,
  //         url: this.SnappedLatLon[j].url
  //       })
  //     }
  //      console.log(this.finalArray);

  //   });
  // }

}
