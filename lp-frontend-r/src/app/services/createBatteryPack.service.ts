import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
import * as constants from '../constants';
import { RegisterBatteryPost, RegisterBatteryResponse, FetchBmsResponse, FetchDLResponse } from '../models';
import { Observable } from 'rxjs';
import { APIUrls } from '../constants';

const postOption = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable()
export class CreateBatteryPackService {
  private url = APIUrls.REGISTER_BATTERY_PACK;
  private bmsurl = APIUrls.FETCH_UNASSOCIATED_BMS;
  private dlurl = APIUrls.FETCH_UNASSOCIATED_DL;

  constructor(private httpClient: HttpClient) {}

  public registerBatteryPack(postparm: RegisterBatteryPost): Observable<RegisterBatteryResponse> {
    return this.httpClient.post<RegisterBatteryResponse>(this.url, postparm, postOption);
  }

  public fetchUnassociatedBmsId(): Observable<FetchBmsResponse> {
    return this.httpClient.get<FetchBmsResponse>(this.bmsurl);
  }

  public fetchUnassociatedDlId(): Observable<FetchDLResponse> {
    return this.httpClient.get<FetchDLResponse>(this.dlurl);
  }
}
