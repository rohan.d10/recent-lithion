import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LspDashboardComponent } from './lsp-dashboard.component';

const routes: Routes = [
  {
    path: '',
    component: LspDashboardComponent,
    data: {
      title: 'Dashboard'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class LspDashboardRoutingModule { }
