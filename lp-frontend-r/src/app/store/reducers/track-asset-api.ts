import * as TrackAssetAoiAction from '../actions/track-asset-api';

/**
 * Each reducer module must import the local `State` which it controls.
 *
 * Here the `State` contains two properties:
 * @prop [asset: AssetTable[]] Adds Asset Table
 *
 */
export interface State {
  curr: number;
  id: number;
  lat: number;
  long: number;
}

/**
 * There is always a need of initial state to be passed onto the store.
 *
 * @prop: asset: Set of Asset Table quantities
 */
export const initialState: State = {
  curr: 0,
  id: null,
  lat: 37.431489,
  long: -122.163719
};

/**
 * The actual reducer function. Reducers can be thought of as the tables in the DataBase.
 * These are the functions which are responsible for maintaing, and updating the
 * `State` of the application they control.
 *
 * Here the reducer cotrols that part of the state which is shows the state of the application
 * wheather it is searching and what is it searching for.
 */
export function reducer(state = initialState, action: TrackAssetAoiAction.Actions): State {
  switch (action.type) {
    case TrackAssetAoiAction.ActionTypes.TRACK_ASSET: {
      const asset = action.payload;
      return {
        ...state,
        id: asset
      };
    }

    case TrackAssetAoiAction.ActionTypes.TRACK_ASSET_COMPLETE_SUCCESS: {
      const response = action.payload;
      let curr = state.curr;
      if (state.curr === 18) {
        curr = -1;
      }
      curr++;
      return {
        ...state,
        curr,
        lat: response.lat,
        long: response.long
      };
    }

    case TrackAssetAoiAction.ActionTypes.TRACK_ASSET_COMPLETE_FAIL: {
      const response = action.payload;
      return state;
    }

    default: {
      return state;
    }
  }
}

/**
 * Because the data structure is defined within the reducer it is optimal to
 * locate our selector functions at this level. If store is to be thought of
 * as a database, and reducers the tables, selectors can be considered the
 * queries into said database. Remember to keep your selectors small and
 * focused so they can be combined and composed to fit each particular
 * use-case.
 */

export const getCurr = (state: State) => state.curr;
export const getId = (state: State) => state.id;
export const getLat = (state: State) => state.lat;
export const getLong = (state: State) => state.long;
