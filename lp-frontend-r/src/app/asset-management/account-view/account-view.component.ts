import {
  Component,
  OnInit,
  ViewChild,
  OnDestroy,
  HostListener
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonService } from '../../services/common.service';
import { DataTableDirective } from 'angular-datatables';
import * as moment from 'moment';
import { Subject } from 'rxjs';
import Swal from 'sweetalert2';
import { BsDatepickerDirective } from 'ngx-bootstrap/datepicker';

@Component({
  selector: 'app-account-view',
  templateUrl: './account-view.component.html',
  styleUrls: ['./account-view.component.css']
})
export class AccountViewComponent implements OnInit, OnDestroy {
  filterForm: FormGroup;
  tableHeaders = [
    'Lsp',
    'Driver',
    'Amount',
    'Sales Person',
    'From Date',
    'To Date',
    'Created On'
  ];
  accountlistInfo = [];
  fromDate = new Date();
  toDate = new Date();
  maxDate = new Date();
  minDate: any;

  @ViewChild(BsDatepickerDirective) datepicker: BsDatepickerDirective;

  @HostListener('window:scroll')
  onScrollEvent() {
    this.datepicker.hide();
  }

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();

  @ViewChild(DataTableDirective) dtElement: DataTableDirective;

  constructor(private common: CommonService) {}

  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10
    };
    this.initializeForm();
    this.fromDate.setDate(this.fromDate.getDate() - 30);
    this.minDate = this.fromDate;
    this.getAccountList();
  }

  changeToDate() {
    this.filterForm.patchValue({
      toDate: ''
    });

    this.minDate = this.filterForm.get('fromDate').value;
  }

  initializeForm() {
    this.filterForm = new FormGroup({
      fromDate: new FormControl(this.fromDate, [Validators.required]),
      toDate: new FormControl(this.toDate, [Validators.required])
    });
  }

  getAccountList() {
    this.common.getAccountList({ fromDate: this.fromDate, toDate: this.toDate })
      .subscribe(data => {
        if (data.response_code == 200) {
          this.accountlistInfo = data.response;
        }
        this.dtTrigger.next();
      });
  }

  onSubmit() {
    if (this.filterForm.valid) {
      this.fromDate = this.filterForm.value.fromDate;
      this.toDate   = this.filterForm.value.toDate;

      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          dtInstance.destroy();
       });

      this.getAccountList();

    }
  }

  ngOnDestroy() {
    this.dtTrigger.unsubscribe();
  }
}
