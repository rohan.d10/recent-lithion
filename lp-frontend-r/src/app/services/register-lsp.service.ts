import { Injectable } from '@angular/core';
import { Http, Response, Headers, URLSearchParams } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RegisterLSP } from './../models/register-lsp.model';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { tap } from 'rxjs/operators';
import { APIUrls } from '../constants';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    Authorization: 'my-auth-token'
  })
};

@Injectable()
export class RegisterLSPService {
  private regUrl = APIUrls.REGISTER_LSP;
  constructor(private _http: HttpClient) {}

  public registerLSP(lsp: RegisterLSP): Observable<RegisterLSP> {
    return this._http.post<RegisterLSP>(this.regUrl, lsp, httpOptions);
  }
}
