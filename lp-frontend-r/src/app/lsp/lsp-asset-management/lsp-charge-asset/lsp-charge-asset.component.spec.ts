import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LspChargeAssetComponent } from './lsp-charge-asset.component';

describe('LspChargeAssetComponent', () => {
  let component: LspChargeAssetComponent;
  let fixture: ComponentFixture<LspChargeAssetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LspChargeAssetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LspChargeAssetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
