export interface TotalUser {
  response: User[];
  response_code: number;
}

export interface User {

  assetId: string;
  updatedAt: string;
  updatedAtDate: string;
  updatedAtTime: string;
  driver_name: string;
  current: number;
  voltage: number;
  capLeft: number;
  Kmleft: number
  KM_driven: number;
  actorId: string;
  lspName: string;
  x: number;

}
