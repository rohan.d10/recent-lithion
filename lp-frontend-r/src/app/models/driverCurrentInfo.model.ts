export class DriverCurrentInfo {
  id: string;
  driver_name: string;
  asset_id: string;
  _a_h_left: string;
  _k_m_left: string;
  driver_id: string;
  p_earn: string;
  earned_current_cycle: string;
  _k_m_driven: string;
  start_ah: string;
  used_ah: string;
}

export class DriverCurrentInfoResponse {
  response_code: any;
  response: DriverCurrentInfo[];
}

export class DriverCurrentInfoSingleResponse {
  status_code: string;
  response: DriverCurrentInfo;
}
