import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LspCollectAssetComponent } from './lsp-collect-asset.component';

describe('LspCollectAssetComponent', () => {
  let component: LspCollectAssetComponent;
  let fixture: ComponentFixture<LspCollectAssetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LspCollectAssetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LspCollectAssetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
