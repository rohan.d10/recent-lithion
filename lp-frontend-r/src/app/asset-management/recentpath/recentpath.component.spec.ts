import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecentpathComponent } from './recentpath.component';

describe('RecentpathComponent', () => {
  let component: RecentpathComponent;
  let fixture: ComponentFixture<RecentpathComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecentpathComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecentpathComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
