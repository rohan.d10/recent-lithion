import { Action } from '@ngrx/store';

/**
 * For each action type in an action group, make a simple
 * enum object for all of this group's action types.
 *
 * The 'actionTypeCheck' utility function coerces strings into string
 * literal types and runs a simple check to guarantee all
 * action types in the application are unique.
 */
export const ActionTypes = {
  CREATE_ASSET: 'Create Asset',
  FETCH_ASSET: '[Asset] Fetch asset',
  FETCH_ASSET_SUCCESS: '[Asset] Fetch asset success'
};

/**
 * Every action is comprised of at least a type and an optional
 * payload. Expressing actions as classes enables powerful
 * type checking in reducer functions.
 *
 * See Discriminated Unions: https://www.typescriptlang.org/docs/handbook/advanced-types.html#discriminated-unions
 */

export class AddAssetAction implements Action {
  type = ActionTypes.CREATE_ASSET;

  constructor(public payload: any) {}
}

export class FetchAssetAction implements Action {
  type = ActionTypes.FETCH_ASSET;
  payload: null;

  constructor() {}
}
export class FetchAssetActionSuccess implements Action {
  type = ActionTypes.FETCH_ASSET_SUCCESS;

  constructor(public payload: any) {}
}
/**
 * Export a type alias of all actions in this action group
 * so that reducers can easily compose action types
 */
export type Actions = AddAssetAction | FetchAssetAction | FetchAssetActionSuccess;
