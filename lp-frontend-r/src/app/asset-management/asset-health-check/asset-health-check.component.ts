import { Component, OnInit,ViewChild } from '@angular/core';
import { FormControl,FormGroup,Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonService } from '../../services/common.service';
import { DataTableDirective } from 'angular-datatables';
import * as moment from 'moment';
import { Subject } from 'rxjs';
import { Angular5Csv } from 'angular5-csv/Angular5-csv';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-asset-health-check',
  templateUrl: './asset-health-check.component.html',
  styleUrls: ['./asset-health-check.component.css']
})
export class AssetHealthCheckComponent implements OnInit {
  assetInfo     = [];
  thresholdData = [];
  fromDate      = moment().subtract(7,'days').format('YYYY-MM-DD');
  toDate        = moment().format('YYYY-MM-DD');
  tableHeaders1 = ['Actor','Capacity Used','Cycle Number','State','Km Driven'];
  tableHeaders2 = ['Ambient Temperature','Cell Temperature','High Cell Voltage','High Current',
  					'Low Cell Voltage','Power Temperature'];
  assetId       = '';			
  filterForm: FormGroup;		

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  dtTrigger2: Subject<any> = new Subject();

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  private getId() {
    this.assetId = this.route.url.substring(22);
  }

  constructor(private common:CommonService,private route: Router) {
    this.getId();
   }

  ngOnInit() {
  	this.dtOptions = {
        pagingType: 'full_numbers',
        pageLength: 10,
      };
    this.initializeForm();
  	this.assetHistoryInfo();
  	this.thresholdInfo();
  }

   initializeForm(){
     this.filterForm = new FormGroup({
        'fromDate' : new FormControl(this.fromDate, [Validators.required]),
        'toDate'   : new FormControl(this.toDate, [Validators.required])
      });
  }

  assetHistoryInfo(){
  	this.common.assetHistoryInfo({ assetId:this.assetId,fromDate:this.fromDate,toDate:this.toDate }).subscribe((data) => {
  		if(data.response_code == 200){
  			this.assetInfo = data.response.historyCycles;
  			this.dtTrigger.next();
  		}
  	});
  }

  downloadCSV(){
    this.fromDate = this.filterForm.get('fromDate').value;
    this.toDate   = this.filterForm.get('toDate').value;
    
    this.common.downloadCSV({fromDate:this.fromDate,toDate:this.toDate,batteryId:this.assetId}).subscribe((data) => {
        if(data.response_code == 200){
          this.downloadFile(data.response);
        }
        else
          Swal('Failure!',data.response,'error');
    })
  }

  downloadFile(data){
     var excelData = [];
     var filename = this.assetId + '_' + this.fromDate + '_' + this.toDate;
     var options  = { 
        
        showLabels       : true, 
        useBom           : true,
        headers: ["DataLogger","DL Timestamp","Ambient Temp","Asset Id","Battery Remaining","Battery Status",
                  "Capacity","Current","Ext Temp1","Ext Temp2","Ext Temp3","Ext Temp4","Ext Temp5","KM Interval",
                  "Latitude","Logged At","Longitude","Power Temp","State","System Status","v1","v2","v3","v4",
                  "v5","v6","v7","v8","v9","v10","v11","v12","v13","v14","v15","v16","Voltage"
                  ]
      };

      data.forEach(element => {
        excelData.push({
          DL_id: element.DL_id,
          DlTimestamp: element.DlTimestamp,
          ambientTemp: element.ambientTemp,
          assetId: element.assetId,
          batRemain: element.batRemain,
          batStatus: element.batStatus,
          cap:element.cap,
          current : element.current,
          extTemp1 : element.extTemp1,
          extTemp2 : element.extTemp2,
          extTemp3 : element.extTemp3,
          extTemp4 : element.extTemp4,
          extTemp5 : element.extTemp5,
          kminterval: element.kminterval,
          lat:element.lat,
          loggedAt : moment(element.loggedAt).utc().format('YYYY-MM-DD HH:mm:ss'),
          lon:element.lon,
          powerTemp:element.powerTemp,
          state:element.state,
          sysStatus:element.sysStatus,
          v1:element.v1,
          v2:element.v2,
          v3:element.v3,
          v4:element.v4,
          v5:element.v5,
          v6:element.v6,
          v7:element.v7,
          v8:element.v8,
          v9:element.v9,
          v10:element.v10,
          v11:element.v11,
          v12:element.v12,
          v13:element.v13,
          v14:element.v14,
          v15:element.v15,
          v16:element.v16,
          vol:element.vol
        });
      });
    
      new Angular5Csv(excelData,filename,options);
  }

  thresholdInfo(){
  	this.common.thresholdInfo({ assetId:this.assetId }).subscribe((data) => {
  		if(data.response_code == 200){
  			this.thresholdData = data.response;
  			this.dtTrigger2.next();
  		}
  	});
  }

  onSubmit(){
    if(this.filterForm.valid){
      this.fromDate = this.filterForm.get('fromDate').value;
      this.toDate   = this.filterForm.get('toDate').value;
      
      if(this.assetInfo.length >0){
         this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          // Destroy the table first
          dtInstance.destroy();
          // Call the dtTrigger to rerender again
        });
      }
      this.assetHistoryInfo();
    }
  }

}
