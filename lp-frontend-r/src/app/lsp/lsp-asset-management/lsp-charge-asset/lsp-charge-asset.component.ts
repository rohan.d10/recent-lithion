import { Component, OnInit, OnDestroy, EventEmitter, Output, ChangeDetectorRef } from '@angular/core';
import { AssetTable, CreateAsset } from '../../../models';
import { NgForm } from '@angular/forms';
import { Store } from '@ngrx/store';
import * as fromRoot from '../../../store/reducers';

import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-lsp-charge-asset',
  templateUrl: './lsp-charge-asset.component.html',
  styleUrls: ['./lsp-charge-asset.component.scss']
})
export class LspChargeAssetComponent implements OnInit, OnDestroy {
  private __subscriptions__: Subscription[] = new Array<Subscription>();
  @Output() reviewButtonClicked = new EventEmitter<string>();
  public assets: AssetTable[];

  constructor(private changeDetectorRef: ChangeDetectorRef, private store: Store<fromRoot.State>) {}

  ngOnInit() {}

  // this.addAssetFunction.createAssetFunction('create');

  onReview(form: NgForm) {}

  private getDataFromStore() {
    this.__subscriptions__.push(
      this.store.select(fromRoot.getAssets).subscribe(assets => {
        this.assets = assets;
        this.changeDetectorRef.detectChanges();
      })
    );
  }

  /**
   * Cleanup all the subscriptions when component is destroyed.
   */

  ngOnDestroy() {
    this.__subscriptions__.forEach(subscription => subscription.unsubscribe());
  }
}
