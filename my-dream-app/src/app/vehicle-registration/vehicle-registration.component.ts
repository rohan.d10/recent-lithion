import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import * as moment from 'moment';
import { from } from 'rxjs';
@Component({
  selector: 'app-vehicle-registration',
  templateUrl: './vehicle-registration.component.html',
  styleUrls: ['./vehicle-registration.component.scss']
})
export class VehicleRegistrationComponent implements OnInit {
  constructor(fb: FormBuilder, private http: HttpClient) { }
  VehicleRegistrationForm: FormGroup;
  maxDate = new Date();

  public val1;
  public val2;
  public minDate = new Date();
  ngOnInit() {
    this.VehicleRegistrationForm = new FormGroup({
      vehicleNumber    : new FormControl('', [Validators.required, Validators.pattern('^([A-Z]{2,2})([ ]{0,1})([\-]{0,1})([0-9]{2,2})([ ]{0,1})([\-]{0,1})([A-Z]{1,3})([ ]{0,1})([\-]{0,1})([0-9]{4,4})')]),
      RcNumber         : new FormControl('', Validators.required),
      vehicleType      : new FormControl('SelectToChoose', Validators.required),
      insurance        : new FormControl(null, Validators.required),
      financed         : new FormControl(null, Validators.required),
      motor            : new FormControl(null, Validators.required),
      controller       : new FormControl(null, Validators.required),
      dateOfPurchase   : new FormControl(null, Validators.required),
      financedBy       : new FormControl(null),
      insuranceBy      : new FormControl(null),
      lastInsuranceDate: new FormControl(null),
      grossWeight      : new FormControl(null, [Validators.required, Validators.pattern('^[0-9]*$')])

    });

    this.val1 = null;
    this.val2 = null;
  }

  choices: string[] = ['Yes', 'No'];

  checkVehicleValue(): boolean {
    if (
      this.VehicleRegistrationForm.controls.vehicleType.value != 'SelectToChoose'
    ) {
      return true;
    } else {
      return false;
    }
  }

  checkFinanceValue(): boolean {
    if (this.val2 == 'Yes') {
      this.VehicleRegistrationForm.controls.financedBy.setValidators([
        Validators.required
      ]);
      return true;
    } else {
      this.VehicleRegistrationForm.controls.financedBy.setValue(null);
      this.VehicleRegistrationForm.controls.financedBy.setValidators(
        null)

      return false;
    }
  }

  checkInsuranceValue(): boolean {
    if (this.val1 == 'Yes') {
      this.VehicleRegistrationForm.controls.insuranceBy.setValidators([Validators.required]);
      this.VehicleRegistrationForm.controls.lastInsuranceDate.setValidators([Validators.required]);
      return true;
    }
    else {
      this.VehicleRegistrationForm.controls.insuranceBy.setValue(null);
      this.VehicleRegistrationForm.controls.lastInsuranceDate.setValue(null);
      this.VehicleRegistrationForm.controls.insuranceBy.setValidators(
        null);
    }
  }
  optionChange() {
    this.VehicleRegistrationForm.controls.motor.setValue(null);
    this.VehicleRegistrationForm.controls.controller.setValue(null);
  }
  getValue() {
    this.val1 = this.VehicleRegistrationForm.controls.insurance.value;
    this.val2 = this.VehicleRegistrationForm.controls.financed.value;
  }



  checkDisabled() {
    if (this.VehicleRegistrationForm.valid) { return false; }
    else {
      return true;
    }
  }

  onSubmit() {
    if (this.VehicleRegistrationForm.valid) {

      if (this.VehicleRegistrationForm.value.dateOfPurchase) {
        this.VehicleRegistrationForm.value.dateOfPurchase = moment(
          this.VehicleRegistrationForm.value.dateOfPurchase
        ).format('YYYY-MM-DD');

      }

      if (this.VehicleRegistrationForm.value.lastInsuranceDate) {
        this.VehicleRegistrationForm.value.lastInsuranceDate = moment(
          this.VehicleRegistrationForm.value.lastInsuranceDate
        ).format('YYYY-MM-DD');

      }

      if (this.VehicleRegistrationForm.value.insurance=='Yes')
      {
        this.VehicleRegistrationForm.value.insurance = true;
      }
      else {
        this.VehicleRegistrationForm.value.insurance = false;
      }

      if (this.VehicleRegistrationForm.value.financed=='Yes')
      {
        this.VehicleRegistrationForm.value.financed = true;
      }
      else {
        this.VehicleRegistrationForm.value.financed = false;
      }


      console.log(this.VehicleRegistrationForm.value);


        // var data = {
        // "rc_no"      : this.VehicleRegistrationForm.value.RcNumber,
        // "date_of_purchase": this.VehicleRegistrationForm.value.dateOfPurchase,
        // "financed"      : this.VehicleRegistrationForm.value.financed,
        // "financed_by"    : this.VehicleRegistrationForm.value.financedBy,
        // "gross_wt"   : this.VehicleRegistrationForm.value.grossWeight,
        // "insurance"     : this.VehicleRegistrationForm.value.insurance,
        // "vehicle_no" : this.VehicleRegistrationForm.value.vehicleNumber,
        // "vehicle_type"   : this.VehicleRegistrationForm.value.vehicleType
        // }




        // return this.http.post("http://13.233.145.127/user/signup", data
        // ).subscribe(
        // (res)=>{
        // console.log(res);
        // })
      }
    }
  }

