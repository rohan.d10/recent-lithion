import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormControl,
  Validators,
  PatternValidator
} from '@angular/forms';
import { NgIf } from '@angular/common';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})
export class TestComponent implements OnInit {
  constructor() {}
  testForm: FormGroup;

  set() {
    this.testForm.reset();
  }

  ngOnInit() {
    this.testForm = new FormGroup({
      string: new FormControl(null, [
        Validators.maxLength(4),
        Validators.pattern('[a-zA-Z]{2,2}[0-9]{2,2}')
      ])
    });
  }

  onSubmit() {
    console.log(this.testForm);
  }
}
