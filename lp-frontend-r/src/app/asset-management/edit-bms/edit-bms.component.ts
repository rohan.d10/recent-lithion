import { Component, OnInit } from '@angular/core';
import { FormControl,FormGroup,Validators } from '@angular/forms';
import * as models from '../../models';
import { CommonService } from '../../services/common.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-edit-bms',
  templateUrl: './edit-bms.component.html',
  styleUrls: ['./edit-bms.component.css']
})
export class EditBmsComponent implements OnInit {
  
  public fetchBmsId : string;
  bmsForm  			: FormGroup;
  public bmsInfo    = [];
  public formData   = {};

  constructor(private common: CommonService) { }

  ngOnInit() {
  }

  fetchBmsDetails(){
  	this.common.getBmsData({ bmsId:this.fetchBmsId }).subscribe((data) => {
     
        if(data.response_code == 200){

          this.bmsInfo = data.response;
          this.bmsForm = new FormGroup({
            'bmsId'           : new FormControl(this.bmsInfo['bmsId'], Validators.required),
            'softwareVersion' : new FormControl(this.bmsInfo['softwareVersion']),
            'hardwareVersion' : new FormControl(this.bmsInfo['hardwareVersion']),
          });
        }
	    });
  }

  resetForm(){
    this.bmsForm.reset();
  }

  onSubmit(){
     if(this.bmsForm.valid) {
        this.formData = this.bmsForm.value;

        this.common.editBms(this.formData).subscribe((data) => {
          if(data.response_code == 200){
          	  this.bmsInfo = [];
              Swal(data.data,data.response,'success');  
              this.resetForm();
          }
          else{
            Swal('Failure!',data.response,'error');
          }
        });
      }
  }

}
