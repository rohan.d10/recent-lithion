import { Component, OnInit, ViewChild } from '@angular/core';
import { AssetService } from './asset.service';
import { CommonService } from '../../services/common.service';
import { AssetDataSource } from './assetDataSource';
import {MatSort} from '@angular/material';
import * as moment from 'moment';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-asset-status',
  templateUrl: './asset-status.component.html',
  styleUrls: ['./asset-status.component.scss']
})


export class AssetStatusComponent implements OnInit {

  public time = new Date();
  public lastUpdatedAt;
  public set = [];
  public diff;
  public myMoment;
  constructor(private common: CommonService, private spinnerService: Ng4LoadingSpinnerService) { }

  ngOnInit() {
    this.getAssets();
  }

  dataSource = new AssetDataSource(this.common);
  displayedColumns = ['Asset Id', 'lastUpdatedAt', 'updateDate', 'updateTime', 'current', 'voltage', 'capLeft', 'kmLeft', 'kmDriven', 'userID', 'name']
  getAssets() {
    this.spinnerService.show();
    this.common.getAssetList().subscribe(data => {
      if (data.response_code == 200) {
        this.spinnerService.hide();
        for (let i = 0; i < data.response.length; i++) {
          data.response[i].updatedAt = this.calculateUpdatedAt(data.response[i].updatedAt);
        }
        this.dataSource = data.response;
      }


    });
  }

  calculateUpdatedAt(p) {
    let m = moment(p).fromNow();
    // let result = "-";
    // let current = new Date().getTime();
    // let ourDate = new Date(p).getTime();
    // let time = (current - ourDate)/1000;

    //   let day = time / (24 * 3600);
    //   day = Math.round(day);
    //   if (day > 0) {
    //     if (day == 1) {
    //       result = "1 day ago";
    //     }
    //     else {
    //       result = day + " days ago";
    //     }
    //   }
    //   else  {
    //     time = time % (24 * 3600);
    //     let hour = time / 3600;
    //     hour = Math.round(hour);

    //     time %= 3600;
    //     let min = time / 60;
    //     min = Math.round(min);

    //     time %= 60;
    //     let sec = time;
    //     sec = Math.round(sec);

    //     if (hour > 0) {
    //       result = hour + "h " + min + "min " + sec + "s ago";
    //     }
    //     else {
    //       if (min > 0) {
    //         result = min + "min " + sec + "s ago";
    //       }
    //       else {
    //         if(sec>0)
    //         result = sec + " s ago";
    //       }
    //     }
    //   }
      return m;


  }
  }

