import { Action } from '@ngrx/store';

/**
 * For each action type in an action group, make a simple
 * enum object for all of this group's action types.
 *
 * The 'actionTypeCheck' utility function coerces strings into string
 * literal types and runs a simple check to guarantee all
 * action types in the application are unique.
 */
export const ActionTypes = {
  FETCH_TOKEN: '[TOKEN] fetch token',
  FETCH_TOKEN_SUCCESS: '[TOKEN] fetch token success',
  REMOVE_TOKEN: '[TOKEN] remove token',
  REMOVE_TOKEN_SUCCESS: '[TOKEN] remove token success',
  CHECK_TOKEN: '[TOKEN] check token',
  CHECK_TOKEN_SUCCESS: '[TOKEN] check token success'
};

/**
 * Every action is comprised of at least a type and an optional
 * payload. Expressing actions as classes enables powerful
 * type checking in reducer functions.
 *
 * See Discriminated Unions: https://www.typescriptlang.org/docs/handbook/advanced-types.html#discriminated-unions
 */

export class FetchTokenAction implements Action {
  type = ActionTypes.FETCH_TOKEN;

  constructor(public payload: any) {}
}

export class RemoveTokenAction implements Action {
  payload = null;
  type = ActionTypes.REMOVE_TOKEN;
  constructor() {}
}

export class RemoveTokenActionSuccess implements Action {
  payload = null;
  type = ActionTypes.REMOVE_TOKEN_SUCCESS;
  constructor() {}
}
export class FetchTokenActionSuccess implements Action {
  type = ActionTypes.FETCH_TOKEN_SUCCESS;

  constructor(public payload: any) {}
}
export class CheckTokenAction implements Action {
  type = ActionTypes.CHECK_TOKEN;
  payload = null;
  constructor() {}
}
export class CheckTokenActionSuccess implements Action {
  type = ActionTypes.CHECK_TOKEN_SUCCESS;
  constructor(public payload: any) {}
}
/**
 * Export a type alias of all actions in this action group
 * so that reducers can easily compose action types
 */
export type Actions =
  | FetchTokenAction
  | FetchTokenActionSuccess
  | RemoveTokenAction
  | RemoveTokenActionSuccess
  | CheckTokenAction
  | CheckTokenActionSuccess;
