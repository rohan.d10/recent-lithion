import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map,catchError } from 'rxjs/operators';
import * as jwt_decode from 'jwt-decode';
import * as moment from 'moment';
import { Jwt, loginResponse, logoutResponse, driverDetailsResponse } from '../models';
import { Observable } from 'rxjs';
import * as constants from '../constants';
import { Router } from '@angular/router';
import { APIUrls } from '../constants';

@Injectable()
export class AuthenticationService {

  private driverFetchUrl = APIUrls.FETCH_DRIVER_DETAILS;
  private httpOptions;

  constructor(private httpClient: HttpClient, private router: Router) {
    this.httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer '+ localStorage.getItem('auth_token')
      })
    };
  }
  private setSession(jwt_token: Jwt,token) {

    const expiresAt = moment.unix(parseInt(jwt_token.exp));
    localStorage.setItem('expires_at', JSON.stringify(expiresAt.valueOf()));
    localStorage.setItem('username', jwt_token.userName);
    localStorage.setItem('user_id', jwt_token.userId);
    localStorage.setItem('last_login_at', jwt_token.lastLoginAt);
    localStorage.setItem('auth_token', token);
    localStorage.setItem('user_type', jwt_token.userType[0]);

  }

  public log_saved() {
    // console.log(localStorage.getItem('expires_at'));
    // console.log(localStorage.getItem('username'));
    // console.log(localStorage.getItem('name'));
    // console.log(localStorage.getItem('user_type'));
  }

  public logout() {
    this.logoutFromServer().subscribe(response => {
      if (response.response_code === 200) {
        localStorage.removeItem('expires_at');
        localStorage.removeItem('username');
        localStorage.removeItem('user_id');
        localStorage.removeItem('user_type');
        localStorage.removeItem('last_login_at');
        localStorage.removeItem('auth_token');
        this.router.navigate(['/login']);
      }
    });
  }

  logoutFromServer(): Observable<any> {
    return this.httpClient.post(APIUrls.LOGOUT,{userId:localStorage.getItem('user_id')},this.httpOptions).pipe(
      map(this.extractData));
  }

  private extractData(res: Response) {
    let body = res;
    return body || { };
  }

  public checkAndRedirect() {
    if (this.isLoggedIn()) {
      // console.log(this.getUserType());
      if (this.getUserType() === 'GUEST' || this.getUserType() === 'ADMIN') {

        this.router.navigate(['/dashboard']);

      }
      else if(this.getUserType() === 'LSP') {

        this.router.navigate(['/lspdashboard']);

      } else if(this.getUserType() === 'FIELD') {
        this.router.navigate(['/field']);
      }
    } else {
      this.router.navigate(['/login']);
    }
  }

  public isLoggedIn() {
    return moment().isBefore(this.getExpiration());
  }

  public fetchDrivers(): Observable<driverDetailsResponse> {
    return this.httpClient.get<driverDetailsResponse>(this.driverFetchUrl);
  }

  getExpiration() {
    const exp = localStorage.getItem('expires_at');
    const expAt = JSON.parse(exp);
    return moment(expAt);
  }

  public getUsername() {
    return localStorage.getItem('username');
  }

  public getUserId() {
    return localStorage.getItem('user_id');
  }

  public getName() {
    return localStorage.getItem('name');
  }

  public getUserType() {
    return localStorage.getItem('user_type');
  }

  public isLoggedOut() {
    return !this.isLoggedIn();
  }

  public login(username: string, password: string){
    const headers = new HttpHeaders();
    headers.append('content-type', 'application/json');

    const body = {
      userId: username,
      password: password,
      //_remember_me: 'on'
    };

    return this.httpClient
      .post(APIUrls.LOGIN_CHECK, body, {
        headers: headers,
        withCredentials: true
      })
      .map((response: loginResponse) => {
        if(response['response_code'] == 200){
          const token = response['response'];

          if(token){
            const decoded = jwt_decode(token);
            decoded.status = true;
            this.setSession(decoded,token);
            return decoded;
          }
          else{
            return { status:false };
          }
        }
        else
        {
          return { status: false };
        }
      })
      .catch(this.handelError);
  }

  private handelError(error: Response) {
    return Observable.throw(error || 'server error');
  }
}
