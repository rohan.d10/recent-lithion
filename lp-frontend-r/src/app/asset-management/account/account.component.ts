import { Component, OnInit, HostListener, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import * as models from '../../models';
import { CommonService } from '../../services/common.service';
import Swal from 'sweetalert2';
import * as moment from 'moment';
import { BsDatepickerDirective } from 'ngx-bootstrap/datepicker';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {
  accountForm: FormGroup;
  public driverInfo = [];
  public lspInfo    = [];
  public formData   = [];
  maxDate           = new Date();
  minDate           = '';
  isDisabled        = true;

  @ViewChild(BsDatepickerDirective) datepicker: BsDatepickerDirective;

  @HostListener('window:scroll')
  onScrollEvent() {
    this.datepicker.hide();
  }

  constructor(private common: CommonService) {}

  ngOnInit() {
    this.initializeForm();
    this.getDriverData();
    this.getLspData();
  }

  changeToDate() {
    this.isDisabled = false;
    this.accountForm.patchValue({
      toDate: ''
    });

    this.minDate = this.accountForm.get('fromDate').value;
  }

  getDriverData() {
    this.driverInfo = [];
    this.common.getDriverProfileData().subscribe(data => {
      if (data.response_code == 200) {
        this.driverInfo = data.response;
      }
    });
  }

  getLspData() {
    this.lspInfo = [];
    this.common.getLspData().subscribe(data => {
      if (data.response_code == 200) {
        this.lspInfo = data.response;
      }
    });
  }

  initializeForm() {
    this.accountForm = new FormGroup({
      lspId:       new FormControl('-1', [Validators.required, Validators.min(0)]),
      driverId:    new FormControl('-1', [Validators.required, Validators.min(0)]),
      amount:      new FormControl('', [Validators.required, Validators.pattern('^[0-9]*$')]),
      salesPerson: new FormControl('-1', [
                   Validators.required,
                   Validators.min(0)
      ]),
      fromDate:    new FormControl('', Validators.required),
      toDate:      new FormControl('', Validators.required)
    });
  }

  resetForm() {
    this.accountForm.reset();
    this.initializeForm();
  }

  onSubmit() {
    if (this.accountForm.valid) {
      if (this.accountForm.value.fromDate) {
        this.accountForm.value.fromDate = moment(
          this.accountForm.value.fromDate
        ).format('YYYY-MM-DD');
      }
      if (this.accountForm.value.toDate) {
        this.accountForm.value.toDate = moment(
          this.accountForm.value.toDate
        ).format('YYYY-MM-DD');
      }
      this.formData = this.accountForm.value;
      this.common.saveAccountForm(this.formData).subscribe(data => {
        if (data.response_code == 200) {
          Swal('Success!', 'Form Submitted Successfully! ', 'success');
          this.resetForm();
        } else {
          Swal('Failure!', data.response, 'error');
        }
      });
    }
  }
}
