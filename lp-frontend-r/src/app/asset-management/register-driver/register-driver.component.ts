import { Component, OnInit, ViewChild } from '@angular/core';
import {
  FormControl,
  FormGroup,
  Validators,
  AbstractControl,
  ValidatorFn
} from '@angular/forms';
import * as models from '../../models';
import { CommonService } from '../../services/common.service';
import Swal from 'sweetalert2';
import * as moment from 'moment';
import { BsDatepickerDirective } from 'ngx-bootstrap/datepicker';

@Component({
  selector   : 'app-register-driver',
  templateUrl: './register-driver.component.html',
  styleUrls  : ['./register-driver.component.scss']
})
export class RegisterDriverComponent implements OnInit {
  registerDriverForm: FormGroup;
  radioOptions = [
    { label: 'Yes', value: 'true' },
    { label: 'No', value: 'false' }
  ];
  vehicleOptions = [
    { label: 'E-Rickshaw', value: 'E-Rickshaw' },
    { label: 'E-Car', value: 'E-Car' }
  ];
  batteryOptions = [
    { label: 'Lead Acid', value: 'Lead Acid' },
    { label: 'Lithion', value: 'Lithion' }
  ];
  public formData = {};
  maxDate = new Date();
  @ViewChild(BsDatepickerDirective) datepicker: BsDatepickerDirective;
  constructor(private common: CommonService) {}

  ngOnInit() {
    this.initializeForm();
  }

  initializeForm() {
    this.registerDriverForm = new FormGroup({
      name       : new FormControl('', [Validators.required]),
      addressPerm: new FormControl('', [Validators.required]),
      addressCurr: new FormControl('', [Validators.required]),
      phone      : new FormControl('', [
        Validators.required,
        Validators.pattern('^[0-9]*$'),
        this.exactLength(10)
      ]),
      estDailyEarning: new FormControl(
        '',
        Validators.pattern('[0-9]+(.[0-9][0-9]?)?')
      ),
      adharNumber: new FormControl('', [
        Validators.required,
        Validators.pattern('^[0-9]*$'),
        this.exactLength(12)
      ]),
      licenseNum : new FormControl('', [Validators.required]),
      accNumber  : new FormControl('', [Validators.required]),
      bankName   : new FormControl('', [Validators.required]),
      bankAddress: new FormControl('', [Validators.required]),
      bankIFSC   : new FormControl('', [Validators.required]),
      pan        : new FormControl('', [
        Validators.required,
        Validators.pattern('[A-Za-z]{5}[0-9]{4}[A-z]{1}')
      ]),
      dob         : new FormControl('', [Validators.required]),
      fatherName  : new FormControl(''),
      motherName  : new FormControl(''),
      spouseName  : new FormControl(''),
      noOfChildren: new FormControl('', Validators.pattern('^[0-9]*$')),
      vehicleType : new FormControl('-1', [
        Validators.required,
        Validators.min(0)
      ]),
      financeBy    : new FormControl(''),
      financeAmount: new FormControl(
        '',
        Validators.pattern('[0-9]+(.[0-9][0-9]?)?')
      ),
      vehicleOwned: new FormControl('-1', [
        Validators.required,
        Validators.min(0)
      ]),
      vehicleNo          : new FormControl(''),
      vehicleManufacturer: new FormControl(''),
      motor              : new FormControl(''),
      controller         : new FormControl(''),
      ageOfVehicle       : new FormControl(
        '',
        Validators.pattern('[0-9]+(.[0-9][0-9]?)?')
      ),
      batteryType      : new FormControl('-1'),
      batterySupplier  : new FormControl(''),
      lastPurchasedDate: new FormControl(),
      drivingTime      : new FormControl(''),
      chargingTime     : new FormControl(''),
      chargingPlace    : new FormControl(''),
      estDailyKm       : new FormControl(
        '',
        Validators.pattern('[0-9]+(.[0-9][0-9]?)?')
      ),
      drivingExp: new FormControl(
        '',
        Validators.pattern('[0-9]+(.[0-9][0-9]?)?')
      )
    });
  }

  resetForm() {
    this.registerDriverForm.reset();
    this.initializeForm();
  }

  onSubmit() {
    if (this.registerDriverForm.valid) {
      if (this.registerDriverForm.value.dob) {
        this.registerDriverForm.value.dob = moment(this.registerDriverForm.value.dob).format('YYYY-MM-DD');
      }
      if (this.registerDriverForm.value.lastPurchasedDate) {
        this.registerDriverForm.value.lastPurchasedDate = moment(this.registerDriverForm.value.lastPurchasedDate)
                                                              .format('YYYY-MM-DD');
      }
     
      this.formData = this.registerDriverForm.value;

      this.common.registerDriver(this.formData).subscribe(data => {
        if (data.response_code == 200) {
          Swal(data.data, data.response, 'success');
          this.resetForm();
        } else {
          Swal('Failure!', data.response, 'error');
        }
      });
    }
  }

  changeDateFormat(date) {
    var year  = date['year'];
    var month = date['month'];
    var day   = date['day'];

    if (month < 10) {
      month = '0' + month;
    }
    if (day < 10) {
      day = '0' + day;
    }

    return year + '-' + month + '-' + day;
  }

  exactLength(limit: Number): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } | null => {
      let phone = control.value;

      if (phone && phone.length != limit) {
        return { exactLength: true };
      }
      return null;
    };
  }
}
