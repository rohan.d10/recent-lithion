import { Component, OnInit } from '@angular/core';
import { FormControl,FormGroup,Validators, } from '@angular/forms';
import * as models from '../../models';
import { CommonService } from '../../services/common.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-register-charger',
  templateUrl: './register-charger.component.html',
  styleUrls: ['./register-charger.component.css']
})
export class RegisterChargerComponent implements OnInit {
  registerChargerForm: FormGroup;
  radioOptions= ['Yes','No'];
  public formData = {};
  public val1 = null;
  public val2 = null;

  constructor(private common: CommonService) { }

  ngOnInit() {
  	this.initializeForm();
  }

  initializeForm(){
     this.registerChargerForm = new FormGroup({
		'isAuthenticate'   : new FormControl('', [Validators.required]),
		'deeplyDischarged' : new FormControl('', [Validators.required]),
		'specs' 		   : new FormControl('' ,  [Validators.required]),
      });
  }

  resetForm(){
    this.registerChargerForm.reset();
    this.initializeForm();
  }

  getValue() {
    this.val1 = this.registerChargerForm.controls.isAuthenticate.value;
    this.val2 = this.registerChargerForm.controls.deeplyDischarged.value;
  }

    onSubmit(){
     if(this.registerChargerForm.valid) {
        this.formData = this.registerChargerForm.value;
        this.common.registerChargerForm(this.formData).subscribe((data) => {
          if(data.response_code == 200){
              Swal(data.data,data.response,'success');
              this.resetForm();
          }
          else
            Swal('Failure!',data.response,'error');

        });
      }
  }

}
