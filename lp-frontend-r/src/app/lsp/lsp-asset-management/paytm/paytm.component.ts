import { Component, OnInit } from '@angular/core';
import { paytm,APIUrls } from '../../../constants';
import { CommonService } from '../../../services/common.service';

@Component({
  selector: 'app-paytm',
  templateUrl: './paytm.component.html',
  styleUrls: ['./paytm.component.css']
})
export class PaytmComponent implements OnInit {
  mid            : string;
  orderId        : string;
  custId         : string;
  industryTypeId : string;
  channelId      : string;
  txnAmount     : number;
  website        : string;
  callBackUrl   : string;
  actorId        : string;
  mobile         : string;
  formData : any;
  paytmTxnUrl : string;
  checkSumHash : string;
  paytm_form : string;


  constructor(private common: CommonService) {
	this.mid            = paytm.PAYTM_MERCHANT_MID;
	this.industryTypeId = paytm.INDUSTRY_TYPE_ID;
	this.channelId      = paytm.CHANNEL_ID;
	this.txnAmount      = 1 ;
	this.website        = paytm.PAYTM_MERCHANT_WEBSITE;
	this.callBackUrl    = APIUrls.PAYTM_RESPONSE;
	this.paytmTxnUrl    = 'https://securegw-stage.paytm.in/theia/processTransaction';  
	this.mobile         = '9810529673';
	this.paytm_form     = 'paytm_form';
   }

  ngOnInit() {
	  console.log(this.paytmTxnUrl);
  	this.loadPaytmForm();
  	
  }

  loadPaytmForm(){
  	this.formData = {
  		mid            : this.mid,
  		industryTypeId : this.industryTypeId,
  		channelId      : this.channelId,
  		website        : this.website,
  		txnAmount      : this.txnAmount,
  		actorId        : "D9810529673",
  		mobile         : "9810529673",
  		orderId        : 'order1',
  		custId         : 'order1',
  		callBackUrl    : this.callBackUrl
  	};

  	this.common.generateChecksumHash(this.formData).subscribe((res) => {
  		if(res.response_code == 200){
  			this.common.verifyChecksum(res.data).subscribe((checkRes) => {
  				if(checkRes.response_code == 200){
  					this.orderId        = res.data['ORDER_ID'];
					this.custId         = res.data['CUST_ID'];
					this.checkSumHash   = res.data['CHECKSUMHASH'];
					this.channelId      = res.data['CHANNEL_ID'];
					this.custId         = res.data['CUST_ID'];
					this.industryTypeId = res.data['INDUSTRY_TYPE_ID'];
					this.mid            = res.data['MID'];
					this.mobile         = res.data['MOBILE_NO'];
					this.txnAmount      = res.data['TXN_AMOUNT'];
					this.website        = res.data['WEBSITE']; 
					this.callBackUrl    = res.data['CALLBACK_URL'];
		  			console.log(res.data);
					this.submitPaytmForm();	
  				}
  			});
  		}
  	});
  }

  submitPaytmForm(){
  	setTimeout(function(){this.paytm_form.submit();},5000);
  }
}
