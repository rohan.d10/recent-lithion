import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators,AbstractControl, ValidatorFn } from '@angular/forms';
import { CommonService } from '../../services/common.service';
import { HttpClient } from '@angular/common/http';
import Swal from 'sweetalert2';
import * as moment from 'moment';

@Component({
  selector: 'app-customer-interaction',
  templateUrl: './customer-interaction.component.html',
  styleUrls: ['./customer-interaction.component.scss']
})
export class CustomerInteractionComponent implements OnInit {
  public driverAssigned = [];
  public Bid;
  public Lsp;
  public maxDate;



  constructor(private common: CommonService, private http: HttpClient) { }



  customerForm: FormGroup;

  ngOnInit() {
    this.initializeForm();
    this.fetchDriverAssigned();
  }

  initializeForm() {
    this.customerForm = new FormGroup({
      'driverId': new FormControl('', [Validators.required]),
      'lspId': new FormControl('', [Validators.required]),
      'batteryId': new FormControl('', [Validators.required]),
      'date': new FormControl(''),
      'hrsDriven': new FormControl('',[Validators.pattern('^[0-9]*([\.]{0,1})[0-9]*$')]),
      'kmDriven': new FormControl('',[Validators.pattern('^[0-9]*([\.]{0,1})[0-9]*$')]),
      'earning': new FormControl('',[Validators.pattern('^[0-9]*([\.]{0,1})[0-9]*$')]),
      'chargingCycle': new FormControl('0'),
      'attendedBy': new FormControl('',[Validators.required]),
      'issue': new FormControl(''),


    })
    this.maxDate = new Date();
  }
  private fetchDriverAssigned() {
    this.common.fetchAssignedDrivers().subscribe(data => {
      if(data.response_code == 200)
        this.driverAssigned = data.response;
        this.customerForm.get('driverId').setValidators(this.validateDriver(this.driverAssigned));
    });
  }

  resetForm() {
    this.customerForm.reset();
    this.initializeForm();
  }

  getBidLsp(driverId) {
    this.common.getCustomerInteractionData({driverId:driverId}).subscribe((res) => {
      if (res.response_code == 200) {
        this.customerForm.patchValue({
          'lspId': res.data[0].Lsp,
          'batteryId': res.data[0].assetId
            });
      }
    });
  }

  valuechange(){
    var driverId = this.customerForm.controls.driverId.value;
    var withoutSpace = driverId.replace(/ /g,"");
    var length = withoutSpace.length;

    if(length >= 11){
      this.getBidLsp(driverId);
    }
  }


  validateDriver(driverList: any): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } | null => {

       let driver = control.value;
       let flag = 0;

       driverList.forEach(element => {
         if(driver == element.driverId && flag == 0){
           flag = 1;
         }
       });
        if (flag == 0) {
            return { 'validateDriver': true };
        }
        return null;
    };
  }

  checkDisabled() {
    if (this.customerForm.valid) { return false; }
    else {
      return true;
    }
  }


  onSubmit() {
    if (this.customerForm.valid) {

      if (this.customerForm.value.date) {
        this.customerForm.value.date = moment(
          this.customerForm.value.date
        ).format('YYYY-MM-DD');
      }
      console.log(this.customerForm);

       return this.common.registerCustomerData(this.customerForm.value).subscribe((res) => {
        if (res.response_code == 200) {
          Swal('Success!', res.response, 'success');
          this.resetForm();
        } else Swal('Failure!', res.response, 'error');
        })
      }
    }
  }


