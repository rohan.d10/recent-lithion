import { Injectable } from '@angular/core';
import * as models from '../models';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { tap } from 'rxjs/operators';

import * as constants from '../constants';
import { APIUrls } from '../constants';

@Injectable()
export class FetchBatteryService {
  private batteryUrl = APIUrls.FETCH_BATTERY_CURRENT_CYCLE_DETAILS + '?assetid=';
  constructor(private http: HttpClient) {}

  public getCurrentBatteryById(assetId: string): Observable<models.BatteryCurrentSingleResponse> {
    return this.http.get<models.BatteryCurrentSingleResponse>(this.batteryUrl + assetId);
  }

  public logError() {
    console.log('error occurred');
  }
}
