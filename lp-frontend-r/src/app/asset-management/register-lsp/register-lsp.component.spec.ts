import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterLspComponent } from './register-lsp.component';

describe('RegisterLspComponent', () => {
  let component: RegisterLspComponent;
  let fixture: ComponentFixture<RegisterLspComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RegisterLspComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterLspComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
