import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LspReportIssueComponent } from './lsp-report-issue.component';

describe('LspReportIssueComponent', () => {
  let component: LspReportIssueComponent;
  let fixture: ComponentFixture<LspReportIssueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LspReportIssueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LspReportIssueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
