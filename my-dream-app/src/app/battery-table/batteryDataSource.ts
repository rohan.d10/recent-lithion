import { Observable } from 'rxjs';
import { BatteryService } from 'src/app/battery-table/battery.service';
import { DataSource } from '@angular/cdk/collections';
import { User } from 'src/app/battery-table/battery.model';
import { ActivatedRoute } from '@angular/router';

export class BatteryDataSource extends DataSource<any> {
  constructor(private batteryService: BatteryService, private p: string) {
    super();
  }
  public id;
  connect(): Observable<User[]> {

    return this.batteryService.getData(this.p);
  }
  disconnect() {}
}
