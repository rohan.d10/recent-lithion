export class AssetRecentPathLocation{
  loggedAt: string
  latitude: any
  longitude: any
}

export class AssetRecentpath {
  response_code: number;
  response: AssetRecentPathLocation[];
}
