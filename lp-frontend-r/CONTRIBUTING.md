* ### NEVER push directly to main repo even if you have the access.
* First fetch and merge the commits from upstream (main repo).
* Resolve the merge conflicts locally.
* Push your commits to forked repo and create the merge request.
* Wait for the maintainer to merge your commits in the main repo.
* Fetch and merge upstream master to your local master.