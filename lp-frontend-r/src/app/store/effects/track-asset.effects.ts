import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/skip';
import 'rxjs/add/operator/takeUntil';

import { TrackAssetService } from '../../services';
import * as apiAction from '../actions/track-asset-api';
import * as fromRoot from '../reducers';

/**
 * Effects offer a way to isolate and easily test side-effects within your
 * application. StateUpdates is an observable of the latest state and
 * dispatched action. The `toPayload` helper function returns just
 * the payload of the currently dispatched action, useful in
 * instances where the current state is not necessary.
 *
 * A simple way to think of it is that ngrx/effects is an event listener of sorts.
 * It listens for actions being dispatched to the store. You can then tell `ngrx/effects`
 * that when a particular action is dispatched, to take another, new action as a result.
 * At the end, what’s really happening is `ngrx/effects` is an `action generator` that dispatches
 * a `new action` as a result of a different action.
 */

@Injectable()
export class ApiTrackAssetEffects {
  private lat = [
    28.625243,
    28.62522,
    28.62515,
    28.625,
    28.624965,
    28.624876,
    28.624765,
    28.62462,
    28.625243,
    28.62522,
    28.62515,
    28.625243,
    28.62522,
    28.62515,
    28.625243,
    28.62522,
    28.62515,
    28.625243
  ];
  private long = [
    77.15449,
    77.154515,
    77.154444,
    77.1539,
    77.1543,
    77.1542,
    77.154067,
    77.153999,
    77.1539,
    77.15449,
    77.154515,
    77.154444,
    77.15449,
    77.154515,
    77.154444,
    77.15449,
    77.154515,
    77.154444
  ];
  //
  //   @Effect()
  //   search$: Observable<Action>
  //     = this.actions$
  //           .ofType(apiAction.ActionTypes.TRACK_ASSET)
  //           .map((action: apiAction.TrackAssetAction) => action.payload)
  //           .withLatestFrom(this.store$)
  //           .map(([action, state]) => {
  //             return {
  //               curr: state.trackAsset.curr,
  //               id: state.trackAsset.id
  //             };
  // })
  //           .switchMap(asset => {
  //             const nextSearch$ = this.actions$.ofType(apiAction.ActionTypes.TRACK_ASSET);
  //
  //             return this.apiTrackAssetService.trackAsset(asset.id)
  //                         .takeUntil(nextSearch$)
  //                         .map(response => {
  //                           return new apiAction.TrackAssetSuccessAction({lat: this.lat[asset.curr], long: this.long[asset.curr]});
  //                         })
  //                         .catch(() => of(new apiAction.TrackAssetFailAction('')));
  //             });

  @Effect()
  nextTrackAssetAction$ = this.actions$
    .ofType(apiAction.ActionTypes.TRACK_ASSET_COMPLETE_SUCCESS)
    .debounceTime(1000)
    .withLatestFrom(this.store$)
    .map(([action, state]) => {
      return new apiAction.TrackAssetAction(state.trackAsset.id);
    });

  @Effect()
  nextTrackAssetActionAfterFail$ = this.actions$
    .ofType(apiAction.ActionTypes.TRACK_ASSET_COMPLETE_FAIL)
    .debounceTime(1000)
    .withLatestFrom(this.store$)
    .map(([action, state]) => {
      return new apiAction.TrackAssetAction(state.trackAsset.id);
    });

  constructor(
    private actions$: Actions,
    private apiTrackAssetService: TrackAssetService,
    private store$: Store<fromRoot.State>
  ) {}
}
