import { Component, OnInit,ViewChild,OnDestroy } from '@angular/core';
import { FormControl,FormGroup,Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonService } from '../../services/common.service';
import { DataTableDirective } from 'angular-datatables';
import * as moment from 'moment';
import { Subject } from 'rxjs';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-view-checklist',
  templateUrl: './view-checklist.component.html',
  styleUrls: ['./view-checklist.component.css']
})

export class ViewChecklistComponent implements OnInit,OnDestroy {
  
  tableHeaders  = ['Battery','LSP','Checked By','Checked On'];
  checklistInfo = [];
  modaldata     = [];
  fromDate      = moment().subtract(30,'days').format('YYYY-MM-DD');
  toDate        = moment().format('YYYY-MM-DD');
  batteryNum    = '';
  filterForm    : FormGroup;
  closeResult   : String;

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

 
  constructor(private common:CommonService,private route: Router,public modalService: NgbModal) {
   }

  ngOnInit() {
  	this.dtOptions = {
        pagingType: 'full_numbers',
        pageLength: 10,
      };
    this.getChecklistData();  
    this.initializeForm();
  }


  public showModalConfirmation(content,objectId) {
 	  for(let check of this.checklistInfo){
        if(objectId == check._id){
          this.modaldata = check;
          break;
        }
      }

    this.modalService.open(content).result.then(
      result => {
        this.closeResult = `${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }

  private getDismissReason(reason: any): string {
      if (reason === ModalDismissReasons.ESC) {
        return 'by pressing ESC';
      } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
        return 'by clicking on a backdrop';
      } else {
        return  `with: ${reason}`;
      }
  }

   initializeForm(){
     this.filterForm = new FormGroup({
        'fromDate'   : new FormControl(this.fromDate, [Validators.required]),
        'toDate'     : new FormControl(this.toDate, [Validators.required]),
        'batteryNum' : new FormControl('')
      });
  }

  zoomImage(imgUrl,description){
     Swal({
        title: 'Image!',
        text: 'Battery Checklist Image',
        imageUrl: imgUrl,
        imageWidth: 500,
        imageHeight: 250,
        imageAlt: 'Battery image',
        animation: false
        });
  }

  getChecklistData(){
  	this.common.getChecklistData({ fromDate:this.fromDate,toDate:this.toDate,batteryNum:this.batteryNum })
  																			.subscribe((data) => {
  		if(data.response_code == 200){
  			this.checklistInfo = data.response;
      }
  		this.dtTrigger.next();
  	});
  }

  onSubmit(){
    if(this.filterForm.valid){
      this.fromDate   = this.filterForm.get('fromDate').value;
      this.toDate     = this.filterForm.get('toDate').value;
      this.batteryNum = this.filterForm.get('batteryNum').value;
     
      if(this.checklistInfo.length > 0){
         this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          // Destroy the table first
          dtInstance.destroy();
          // Call the dtTrigger to rerender again
        });
      }
      this.getChecklistData();
    }
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

}
