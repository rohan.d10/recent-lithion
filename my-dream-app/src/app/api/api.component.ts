import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from './api.service';
import { MatTableDataSource, MatPaginator, PageEvent } from '@angular/material';
import { ApiDataSource } from 'src/app/api/apiDataSource';
import { User } from 'src/app/api/api.model';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-api',
  templateUrl: './api.component.html',
  styleUrls: ['./api.component.scss']
})
export class ApiComponent implements OnInit {
  dataSource = new ApiDataSource(this.apiService);
  displayedColumns = ['name', 'driver id', 'asset id'];
  public elements: [];

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private apiService: ApiService) {}

  ngOnInit() {
    this.apiService.getData().subscribe(data => {
      if (data.response_code == 200) {
        this.elements = data.response;
        this.dataSource = data.response;
      }
    });
  }
}
