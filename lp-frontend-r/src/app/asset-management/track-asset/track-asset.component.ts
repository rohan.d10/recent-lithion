import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Http } from '@angular/http';
import { DatePipe } from '@angular/common';

import { Observable } from 'rxjs/Observable';
import { TrackAssetService } from '../../services/track-asset.service';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import 'rxjs/add/operator/map';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-track-asset',
  templateUrl: './track-asset.component.html',
  styleUrls: ['./track-asset.component.scss']
})
export class TrackAssetComponent implements OnInit, OnDestroy {
  // private __subscriptions__: Subscription[] = new Array<Subscription>();
  title = 'Tracking ID:';
  lat$: Observable<number>;
  long$: Observable<number>;
  lat = 28.749472;
  lng = 77.056534;
  zoom = 14;
  public id: string;
  parms: any[];

  markers: any[];
  mlat = 0.0;
  mlong = 0.0;
  timerData;
  timerMarker;
  public spinner = 'show';
  prevAssetHeaders = ['Server Time','Battery Time','Voltage','Current','Latitude','Longitude','Capacity',
                         'KM Done in Last Interval','KM Left'];
  prevVolatgeHeaders = ['V1','V2','V3','V4','V5','V6','V7','V8','V9','V10','V11','V12','V13','V14','V15','V16',];

  showXAxis = true;
  btnStatus = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = '';
  showYAxisLabel = true;
  yAxisLabel = 'Cell Voltages';
  colorScheme = {
    domain: [
      '#dd6031',
      '#68c3d4',
      '#726e97',
      '#7f557d',
      '#673c4f',
      '#d05353',
      '#e58f65',
      '#698f3f',
      '#0a122a',
      '#804e49',
      '#104547',
      '#7e998a',
      '#0031ca',
      '#004ecb',
      '#009624',
      '#b4004e'
    ]
  };
  autoScale = true;

  multi = [];
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  onSelect(event) {}

  private getId() {
    this.id = this.route.url.substring(22);
  }

  constructor(private service: TrackAssetService, private route: Router, private http: Http) {}

  ngOnInit() {
    this.getId();
    this.dtOptions = {
        pagingType: 'full_numbers',
        pageLength: 10,
      };
      
    this.getBatteryData();
  }

  refreshData(){
    this.btnStatus= true;
    this.spinner= 'show';
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.getBatteryData();
    });
  }

  getBatteryData(){
    this.parms = [];
    this.service.getBatteryData({"battery":this.id,"limit":20}).subscribe((data) => {
      if(data.response_code == 200){
          this.parms = data.response;
          this.dtTrigger.next();
          this.spinner = 'hide';
          this.btnStatus= false;
          this.mlat=parseFloat(this.parms[0]['lat']);
          this.mlong=parseFloat(this.parms[0]['lon']);
      }
      else
      {
        this.spinner = 'hide';
        this.btnStatus= false;
          Swal(
                'Failure!',
                data.response,
                'error'
              );
      }
      
    });
  }

  private getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }

  ngOnDestroy() {
    clearInterval(this.timerMarker);
    clearInterval(this.timerData);
    this.dtTrigger.unsubscribe();
  }
}
