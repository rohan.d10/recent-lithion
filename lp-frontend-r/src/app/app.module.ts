import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { reducer } from './store/reducers';
import { LaddaModule } from 'angular2-ladda';
import { MaterialModule } from './material.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DataTablesModule } from 'angular-datatables';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
// Import Effects Module

import { AppComponent } from './app.component';
import { DashboardModule } from './dashboard/dashboard.module';

import { AppRoutingModule } from './app-routing.module';
import { ApiTrackAssetEffects, ApiCurrentAssetEffects } from './store/effects';
import { AssetTableViewComponent } from './asset-management/asset-table-view/asset-table-view.component';
// import { RegisterDriverComponent } from './asset-management/register-driver/register-driver.component';
// import { UserRegisterComponent } from '../user/user-register/user-register.component';
import * as service from './services';
import { TokenManagementEffects } from './store/effects/token-management.effects';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { MapDataService } from './services/mapData.service';

// root
@NgModule({
  declarations: [AppComponent, AssetTableViewComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    HttpClientModule,
    DashboardModule,
    FormsModule,
    LaddaModule.forRoot({
      style: 'contract',
      spinnerSize: 20,
      spinnerColor: 'red',
      spinnerLines: 12
    }),
    MaterialModule,
    CommonModule,
    ReactiveFormsModule,
    ModalModule,
    AngularFontAwesomeModule,
    AngularMultiSelectModule,
    NgbModule.forRoot(),
    StoreModule.forRoot(reducer),
    EffectsModule.forRoot([ApiTrackAssetEffects, ApiCurrentAssetEffects, TokenManagementEffects]),
    // AgmCoreModule.forRoot({
    //   // dummy API key for testing
    //   apiKey: 'AIzaSyDd91n_BIQk7CBMOGq3rgW_lLXIl_LdxBQ'
    // }),
    BrowserAnimationsModule,
    DataTablesModule
  ],
  providers: [
    service.TrackAssetService,
    service.AssetDataService,
    service.ApiService,
    service.LspFetchService,
    service.DataLoggerCreateService,
    service.TokenManagementService,
    service.RegisterDriverService,
    service.RegisterLSPService,
    service.AuthenticationService,
    service.FetchBatteryService,
    service.DriverAssignmentService,
    service.FetchDriverService,
    service.AllocateAssets,
    service.ReportIssueService,
    service.AssetRecentPathService,
    service.BmsRegService,
    service.ResetPasswordService,
    service.CreateBatteryPackService,
    service.DetachComponentService,
    service.RestService,
    service.CommonService,
    service.MapDataService


  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [AppComponent]
})
export class AppModule {}
