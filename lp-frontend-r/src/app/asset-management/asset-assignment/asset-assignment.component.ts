import { Component, OnInit, OnDestroy,ViewChild } from '@angular/core';
import { FormControl,FormGroup,Validators,AbstractControl,ValidatorFn } from '@angular/forms';
import { CommonService } from '../../services/common.service';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import Swal from 'sweetalert2';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-asset-assignment',
  templateUrl: './asset-assignment.component.html',
  styleUrls: ['./asset-assignment.component.scss']
})
export class AssetAssignmentComponent implements OnInit, OnDestroy {
  assignForm : FormGroup;
  lspList    = [];
  batteryList = [];
  public dropdownList:any = [];
  public dropdownSettings = {};
  public selectedItems    = [];

  
  constructor(private common: CommonService,private spinnerService: Ng4LoadingSpinnerService) {}

  ngOnInit() {
    this.fetchNewBatteries();
    this.initializeForm();
  }

   initializeForm(){
    this.dropDownSettings();
    this.selectedItems = [];
    this.assignForm = new FormGroup({
      'from'    : new FormControl('Lithion Power',Validators.required),
      'toLsp'   : new FormControl('L9999499864',Validators.required),
      'battery' : new FormControl([],[Validators.required,Validators.min(1)]) 
    });
  }

  dropDownSettings(){
    this.dropdownSettings = { 
                              singleSelection    : false, 
                              text               : "Select Batteries",
                              selectAllText      : 'Select All',
                              unSelectAllText    : 'UnSelect All',
                              enableSearchFilter : true,
                              classes            : "myclass custom-class",
                              primaryKey         : "assetId",
                              labelKey           : "assetId",
                            }; 
                          
  }

  public fetchNewBatteries() {
    this.common.fetchNewBatteries().subscribe(data => {
      if(data.response_code == 200){
        this.dropdownList = data.response;
      }
    });
  }

   resetForm(){
    this.assignForm.reset();
    this.initializeForm();
  }

  onSubmit(){
    if(this.assignForm.valid){
      this.spinnerService.show();

      this.common.batteryPrimaryAssignment(this.assignForm.value).subscribe((data)=> {
          if(data.response_code == 200){
            this.spinnerService.hide();
            Swal('Success!',data.response,'success');  
            this.resetForm();
          }else{
            this.spinnerService.hide();
            Swal('Failure!',data.response,'error');
          }
      });
    }
  }

  ngOnDestroy() {
     
  }
}