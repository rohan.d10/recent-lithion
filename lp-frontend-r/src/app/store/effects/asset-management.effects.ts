import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { Store, Action } from '@ngrx/store';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/skip';
import 'rxjs/add/operator/takeUntil';

import { ApiService } from '../../services';
import * as apiAction from '../actions/asset-management';
import * as fromRoot from '../reducers';

/**
 * Effects offer a way to isolate and easily test side-effects within your
 * application. StateUpdates is an observable of the latest state and
 * dispatched action. The `toPayload` helper function returns just
 * the payload of the currently dispatched action, useful in
 * instances where the current state is not necessary.
 *
 * A simple way to think of it is that ngrx/effects is an event listener of sorts.
 * It listens for actions being dispatched to the store. You can then tell `ngrx/effects`
 * that when a particular action is dispatched, to take another, new action as a result.
 * At the end, what’s really happening is `ngrx/effects` is an `action generator` that dispatches
 * a `new action` as a result of a different action.
 */

@Injectable()
export class ApiCurrentAssetEffects {
  @Effect()
  nextTrackAssetAction$ = this.actions$
    .ofType(apiAction.ActionTypes.FETCH_ASSET)
    .switchMap(action => this.fetchApiService.getCurrentAssets())
    .map((response: any) => {
      return new apiAction.FetchAssetActionSuccess(response.response);
    });

  constructor(private actions$: Actions, private fetchApiService: ApiService, private store$: Store<fromRoot.State>) {}
}
