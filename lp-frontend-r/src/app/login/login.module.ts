import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { LoginComponent } from './login.component';
import { LoginRoutingModule } from './login-routing.module';
import { LaddaModule } from 'angular2-ladda';
import { ToasterModule } from 'angular2-toaster';
@NgModule({
  imports: [
    ChartsModule,
    CommonModule,
    RouterModule,
    FormsModule,
    ToasterModule.forRoot(),
    LoginRoutingModule,
    LaddaModule.forRoot({
      style: 'expand-right',
      spinnerSize: 25,
      spinnerColor: 'white',
      spinnerLines: 10
    })
  ],
  declarations: [LoginComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class LoginModule {}
