import { Observable, BehaviorSubject, of } from 'rxjs';
import { AssetService } from 'src/app/asset-status/asset.service';
import { DataSource } from '@angular/cdk/collections';
import { User } from 'src/app/asset-status/asset.model';

export class AssetDataSource extends DataSource<any> {
  constructor(private assetService: AssetService) {
    super();
  }
  connect(): Observable<User[]> {
    return this.assetService.getData();
  }
  disconnect() {}
}
