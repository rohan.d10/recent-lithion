import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
// import {coordinates} from 'src/app/map-data/map-data.model';

@Injectable()
export class MapDataService {
  constructor(private http: HttpClient) {}
  private _url = 'http://api.lithion.in/api/getdata?dlid=B001&limit=100';

  getData(path: string): Observable<any> {
    return this.http.get(path);
  }
}
