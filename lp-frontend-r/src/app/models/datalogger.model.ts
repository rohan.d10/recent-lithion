export class Datalogger {
  dataLoggerId: string;
  vendorId: string;
  version: string;
  mNumber: string;
  sNumber: string;
}
export class RegDataloggerResponse {
  response_code: number;
  message: string;
}

export class FetchDLResponse {
  response_code: number;
  response: SingleDLResponse[];
}

export class SingleDLResponse {
  type: string;
  time: string;
  dataLoggerId: string;
  vendorId: string;
  version: string;
  mNumber: string;
  sNumber: string;
  createdDate: string;
  isactive: string;
  isAssociated: string;
}
