import {
  Component,
  OnInit
} from '@angular/core';
import {
  FormControl,
  FormGroup,
  Validators,
  AbstractControl,
  ValidatorFn
} from '@angular/forms';
import {
  HttpEventType,
  HttpEvent
} from '@angular/common/http';
import * as models from '../../models';
import {
  CommonService
} from '../../services/common.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-checklist',
  templateUrl: './checklist.component.html',
  styleUrls: ['./checklist.component.css']
})

export class ChecklistComponent implements OnInit {
  genders = ['Male', 'Female'];
  drySwitch = [{
    'label': 'Internal',
    'value': 'internal'
  }, {
    'label': 'External',
    'value': 'external'
  }];
  bmsType = [{
    'label': 'WBMS',
    'value': 1
  }, {
    'label': 'BMS NQ',
    'value': 2
  }];
  radioOptions = [{
    'label': 'Yes',
    'value': 'true'
  }, {
    'label': 'No',
    'value': 'false'
  }];
  signUpForm: FormGroup;

  public driverInfo: models.DriverInfo[] = [];
  public lspInfo: models.LspInfo[] = [];
  public dataLoggerInfo: models.DataLoggerInfo[] = [];
  public apiResponse = [];
  public formData = [];

  public isDisabled = true;
  public selectedFile: File = null;
  public maxFileSize = 10 * 1024 * 1024; // 10MB
  public fileAllowedTypes = ['jpeg', 'png', 'jpg'];
  showProgessBar = false;
  disableUploadBtn = false;
  progPercentage = 0;
  public batteryStatus = 0;



  constructor(private common: CommonService) {}

  ngOnInit() {
    this.initializeForm();
    this.getDriverData();
    this.getLspData();
  }

  initializeForm() {
    this.isDisabled = true;
    this.signUpForm = new FormGroup({
      'lspId': new FormControl('-1'),
      'driverId': new FormControl('-1'),
      'batteryNum': new FormControl('', [Validators.required, Validators.maxLength(7), Validators.minLength(4)]),
      'dataLoggerId': new FormControl('', Validators.required),
      'batteryStatus': new FormControl('', Validators.required),
      'bmsType': new FormControl('', Validators.required),
      'bmsNumber': new FormControl('', Validators.required),
      'dlHardwareVersion': new FormControl(''),
      'dlSoftwareVersion': new FormControl(''),
      'simSerialNum': new FormControl('', Validators.required),
      'switch': new FormControl('', Validators.required),
      'sdCard': new FormControl('', Validators.required),
      'chargerAuthentication': new FormControl('', Validators.required),
      'ntc': new FormControl('', Validators.required),
      'cellHolder': new FormControl('', Validators.required),
      'coinCell': new FormControl('', Validators.required),
      'gsmWorking': new FormControl('', Validators.required),
      'gpsCoordinates': new FormControl('', Validators.required),
      'spaccer': new FormControl('', Validators.required),
      'remoteTested': new FormControl('', Validators.required),
      'glued': new FormControl('', Validators.required),
      'terminalCheck': new FormControl('', Validators.required),
      'bmsSenseWireConnector': new FormControl('', Validators.required),
      'bmsEeprom': new FormControl('', Validators.required),
      'bmsMcu': new FormControl('', Validators.required),
      'socWorking': new FormControl('', Validators.required),
      'socStatus': new FormControl('-1', [Validators.required, Validators.min(0)]),
      'batteryLidAlign': new FormControl('', Validators.required),
      'smartLock': new FormControl('', Validators.required),
      'location': new FormControl('', Validators.required),
      'checkedBy': new FormControl('', Validators.required),
      'dlContactNum': new FormControl('', Validators.required),
      'image': new FormControl(null),
      'imageUrl': new FormControl('')
    });
    this.signUpForm.get('lspId').disable();

  }

  onFileSelected(event) {
    if (event.target.files.length > 0) {
      this.disableUploadBtn = true;
      const checkImageExt = this.checkImageExt(event.target.files[0].name);

      if (checkImageExt) {
        if (event.target.files[0].size > this.maxFileSize) {
          this.patchImageValue();
          this.disableUploadBtn = false;
          Swal('Failure!', 'Please selected file which is less than 10MB', 'error');
        } else
          this.selectedFile = < File > event.target.files[0];
      } else {
        this.patchImageValue();
        this.disableUploadBtn = false;
        Swal('Failure!', 'Invalid MIME Type,only JPEG,JPG and PNG files are allowed', 'error');
      }

    }
  }

  checkValue(event: Event) {
    const ctrl = this.signUpForm.get('lspId');
    if (this.batteryStatus == 10) {
      ctrl.enable();
    } else {
      ctrl.disable();
    }
  }


  checkImageExt(fileName) {
    const fileExt = fileName.split('.').pop();
    return this.fileAllowedTypes.includes(fileExt);
  }

  patchImageValue() {
    this.signUpForm.patchValue({
      'image': null,
      'imageUrl': ''
    });
  }



  zoomImage() {
    Swal({
      title: 'Image Selected !',
      text: 'Recently Uploaded Image',
      imageUrl: this.signUpForm.get('imageUrl').value,
      imageWidth: 500,
      imageHeight: 250,
      imageAlt: 'Battery image',
      animation: false
    });
  }

  uploadFile() {
    if (this.selectedFile) {
      const imageData = new FormData();
      imageData.append('image', this.selectedFile, this.selectedFile.name);

      this.common.imageUpload(imageData).subscribe(event => {
        if (event.type === HttpEventType.UploadProgress) {
          this.showProgessBar = true;
          this.progPercentage = Math.round(event.loaded / event.total * 100);
        } else if (event.type === HttpEventType.Response) {
          this.showProgessBar = false;
          if (event.status == 200 && event.body.response_code == 200) {
            this.signUpForm.patchValue({
              'imageUrl': event.body.response.imageUrl
            });
            Swal({
              position: 'center',
              type: 'success',
              title: 'File uploaded successfully',
              showConfirmButton: false,
              timer: 1500
            });
          } else {
            Swal('Failure!', 'Error uploading file', 'error');
          }
        }
      });
    }
  }

  getDriverData() {
    this.driverInfo = [];
    this.common.getDriverProfileData().subscribe((data) => {
      if (data.response_code == 200) {
        this.driverInfo = data.response;

      }
    });
  }


  getLspData() {
    this.lspInfo = [];
    this.common.getLspData().subscribe((data) => {
      if (data.response_code == 200) {
        this.lspInfo = data.response;
      }
    });
  }

  ValidateBattery(control: FormControl): {
    [key: string]: boolean
  } {
    if (this.batteryStatus == 10 || this.batteryStatus == 13) {
      return null;
    }
    return {
      'validateBattery': true
    };

  }

  valuechange() {

    var batteryNum = this.signUpForm.controls.batteryNum.value;
    var withoutSpace = batteryNum.replace(/ /g, "");
    var length = withoutSpace.length;


    if (length == 4 || length ==7) {

      this.common.getBatteryData({
        batteryId: batteryNum
      }).subscribe((data) => {
        if (data.response_code == 200) {
          this.apiResponse = data.response[0];
          this.batteryStatus = this.apiResponse['batteryStatus'];

          this.signUpForm.patchValue({
            'batteryStatus': this.batteryStatus
          });

          if (this.batteryStatus == 10 || this.batteryStatus == 13 || this.batteryStatus == 14)
            this.signUpForm.controls['batteryNum'].setErrors(null);
          else
            this.signUpForm.controls['batteryNum'].setErrors({
              'validateBattery': true
            });

          this.checkDisabled();



          this.signUpForm.patchValue({
            'dataLoggerId': this.apiResponse['associatedDataloggerId'],
            'bmsNumber': this.apiResponse['associatedBmsId'],
            'dlHardwareVersion': this.apiResponse['dlHardwareVersion'],
            'dlSoftwareVersion': this.apiResponse['dlSoftwareVersion'],
            'simSerialNum': this.apiResponse['simSerialNum'],
            'bmsType': this.apiResponse['bmsType']
          });
        }
      });

    } else {
      this.signUpForm.get('lspId').disable();
      this.signUpForm.get('lspId').setValue('-1');

      this.signUpForm.patchValue({
        'dataLoggerId': '',
        'bmsNumber': '',
        'dlHardwareVersion': '',
        'dlSoftwareVersion': '',
        'simSerialNum': '',
        'bmsType': ''
      });
    }
  }

  resetForm() {
    this.signUpForm.reset();
    this.initializeForm();
  }

  checkDisabled() {
    if (this.batteryStatus == 10) {
      this.signUpForm.get('lspId').enable();
      this.signUpForm.get('lspId').setValidators([Validators.required]);
      this.signUpForm.get('lspId').setValue('-2');

    } else {
      this.signUpForm.get('lspId').disable();
      this.signUpForm.get('lspId').setValidators(null);
      this.signUpForm.get('lspId').setValue('-3');

    }
  }
  onSubmit() {
    if (this.signUpForm) {
      this.formData = this.signUpForm.value;

      this.common.saveChecklist(this.formData).subscribe((data) => {
        if (data.response_code == 200) {
          Swal('Success!', 'Checklist Submitted Successfully! ', 'success');
          this.resetForm();
        } else {
          Swal('Failure!', data.response, 'error');
        }
      });
    }
  }

}
