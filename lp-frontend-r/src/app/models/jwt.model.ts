export class Jwt {
  public exp: string;
  public iat: string;
  public userType: string[];
  public userName: string;
  public userId: string;
  public lastLoginAt: string;
}

export class CheckJwtResponse {
  public code: number;
  public message: string;
  public roles: string[];
  public username: string;
  public response: string;
}

export class loginResponse {
  public token: string;
}

export class driverDetailsResponse {
  response_code: number;
  response: driverDetailsResponse[];
}
