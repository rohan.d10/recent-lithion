import { MarkerLabel } from '@agm/core';

export class AssetCurrentInfo {
  assetId: string;
  mNumber: string;
  updatedAtDate: string;
  updatedAtTime: string;
  kmLeft: string;
  kmDriven: string;
  status: string;
  capUsed : number;
  markerLabel: MarkerLabel;
  lat: string | number;
  lon: string | number;
}

export class AssetCurrentResponse {
  response_code: string;
  response: AssetCurrentInfo[];
}

export class BatteryCurrentSingleResponse {
  response_code: string | number;
  response: BatteryCurrentSingle;
}
export class BatteryCurrentSingle {
  asset_id: string;
  updated_at_date: string;
  updated_at_time: string;
  current: string;
  voltage: string;
  cap_left: string;
  cap_used: string;
  km_left: string;
  km_driven: string;
  status: string;
  actor_id: string;
}
