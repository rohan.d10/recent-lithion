import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LspAllocateAssetComponent } from './lsp-allocate-asset.component';

describe('LspAllocateAssetComponent', () => {
  let component: LspAllocateAssetComponent;
  let fixture: ComponentFixture<LspAllocateAssetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LspAllocateAssetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LspAllocateAssetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
