import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import * as constants from '../constants';
import { Bms, BmsResponse } from '../models';
import { Observable } from 'rxjs';
import { APIUrls } from '../constants';
const postOption = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable()
export class BmsRegService {
  constructor(private httpClient: HttpClient) {}

  private regBmsUrl = APIUrls.REGISTER_BMS;

  public registerBms(bms: Bms): Observable<BmsResponse> {
    return this.httpClient.post<BmsResponse>(this.regBmsUrl, bms, postOption);
  }
}
