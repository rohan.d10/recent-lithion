import { Component, OnInit, Input } from '@angular/core';
import { BatteryService } from './battery.service';
import { BatteryDataSource } from './batteryDataSource';
import {MatSort} from '@angular/material';
import * as moment from 'moment';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-battery-table',
  templateUrl: './battery-table.component.html',
  styleUrls: ['./battery-table.component.scss']
})
export class BatteryTableComponent implements OnInit {
  public id;
  public Dl_id;
  constructor(private batteryService: BatteryService, private route: ActivatedRoute) { }

  ngOnInit() {

    this.id = this.route.snapshot.params['assetId']
    this.getBatteries();
  }

  displayedColumns = ["loggedAt",	"DlTimestamp",	"assetId",	"DL_id",	"state",	"current",	"batRemain",	"cap",	"vol",	"lat",	"lon",	"kminterval",	"v1",	"v2",	"v3",	"v4",	"v5",	"v6",	"v7",	"v8",	"v9",	"v10",	"v11",	"v12",	"v13",	"v14",	"v15",	"v16",	"extTemp1",	"extTemp2",	"extTemp3",	"extTemp4",	"extTemp5",	"powerTemp",	"ambientTemp",	"sysStatus","batStatus"]
  dataSource = new BatteryDataSource(this.batteryService,this.id);



  getBatteries() {
    this.batteryService.getData(this.id).subscribe(data => {
      this.dataSource = data;
      this.Dl_id = data.Dl_id;
    });
  }
}

