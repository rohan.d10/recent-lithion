import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl,FormGroup,Validators, } from '@angular/forms';
import * as models from '../../../models';
import { CommonService } from '../../../services/common.service';
import { AuthenticationService } from '../../../services/authentication.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-lsp-collect-asset',
  templateUrl: './lsp-collect-asset.component.html',
  styleUrls: ['./lsp-collect-asset.component.scss']
})
export class LspCollectAssetComponent implements OnInit, OnDestroy {
  collectAssetForm: FormGroup;
  paymentModes          = [{'label':'WBMS','value':'1'},{'label':'BMS NQ','value':'2'}];
  public driverAssigned = [];
  public formData       = {};
  public lspId          = '';

  dropdownList = [];
    selectedItems = [];
    dropdownSettings = {};
 
  constructor(private common: CommonService, private auth: AuthenticationService,
                    private router: Router) {
    this.lspId  = this.auth.getUserId();
  }

  initializeForm(){
     this.collectAssetForm = new FormGroup({
        'driverId'    : new FormControl(''    , [Validators.required]),
        'batteryId'   : new FormControl(''    , [Validators.required]),
        'kmDriven'    : new FormControl(''    , [Validators.required]),
        'ahUsed'      : new FormControl(''    , [Validators.required]),
        'amount'      : new FormControl(''    , [Validators.required]),
        'paymentMode' : new FormControl('Cash', [Validators.required])
      });
  }

  ngOnInit() {
    this.initializeForm();
    this.fetchDriverAssigned();
  }

  valuechange(){
    var driverId = this.collectAssetForm.controls.driverId.value;
    var withoutSpace = driverId.replace(/ /g,"");
    var length = withoutSpace.length;
   
    if(length >= 11){
      for(let driver of this.driverAssigned){
        if(driverId == driver.driverId){
            this.collectAssetForm.patchValue({
              'batteryId' : driver.assetId,
              'kmDriven'  : Math.round(driver.KM_driven),
              'ahUsed'    : driver.usedAh,
              'amount'    : Math.round(driver.earnedCurrCycle)
            });
          break;
        }
      }
    }
  }

  private fetchDriverAssigned() {
    this.common.fetchAssignedDrivers().subscribe(data => {
      if(data.response_code == 200)
        this.driverAssigned = data.response;
    });
  }

    resetForm(){
    this.collectAssetForm.reset();
    this.initializeForm();
  }

  onSubmit(route: string){
    
     if(this.collectAssetForm.valid) {
        this.formData = {
          "assetId"  : this.collectAssetForm.get('batteryId').value,
          "driverId" : this.collectAssetForm.get('driverId').value,
          "lspId"    : this.lspId
        }
      this.common.assetDeassignment(this.formData).subscribe((data) => {
        if(data.response_code == 200){
            if(route){
              this.router.navigate(['/lspdashboard/allocateasset']);
            }else{
              Swal('Success!',data.response,'success');  
              this.resetForm();
            }
        }
        else
          Swal('Failure!',data.response,'error');
      });
    }
    else
      Swal('Failure!','Something went wrong.Please try again later','error');
  }

 

  ngOnDestroy() {
   // this.__subscriptions__.forEach(subscription => subscription.unsubscribe());
  }
}
