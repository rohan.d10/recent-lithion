import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { tap } from 'rxjs/operators';

import * as constants from '../constants';
import { DriverCurrentInfoResponse, DeassignParam, UnAssignedBatteriesResponse } from '../models';
import { postOption } from './lspFetch.service';
import { APIUrls } from '../constants';

@Injectable()
export class DriverAssignmentService {
  private DriverAssignUrl        = APIUrls.DRIVER_ASSIGN;
  private DriverAssignFetchUrl   = APIUrls.DRIVER_FETCH_ASSIGNED;
  private DriverUnassignFetchUrl = APIUrls.DRIVER_FETCH_UNASSIGNED;
  private DriverDeassignUrl      = APIUrls.DRIVER_DEASSIGN;
  private getBatteriesUrl        = APIUrls.FETCH_BATTERY_LIST;
  
  constructor(private http: HttpClient) {}

  public assignDriver(body): Observable<any> {
    return this.http.post<any>(this.DriverAssignUrl,body,postOption);
  }

  public deassignDriver(driverDet: DeassignParam): Observable<any> {
    return this.http.post<any>(this.DriverDeassignUrl, driverDet, postOption);
  }

  public fetchUnassignedDrivers(): Observable<DriverCurrentInfoResponse> {
    return this.http.get<DriverCurrentInfoResponse>(this.DriverUnassignFetchUrl);
  }

  public logError() {
    console.log('error occurred');
  }

  public fetchAssignedDrivers(): Observable<DriverCurrentInfoResponse> {
    return this.http.get<DriverCurrentInfoResponse>(this.DriverAssignFetchUrl);
  }

  public fetchBatteries(lspId: string): Observable<UnAssignedBatteriesResponse> {
    return this.http.get<UnAssignedBatteriesResponse>(this.getBatteriesUrl + '?lspId=' + lspId);
  }
}
