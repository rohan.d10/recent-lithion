import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LspTrackAssetComponent } from './lsp-track-asset.component';

describe('LspTrackAssetComponent', () => {
  let component: LspTrackAssetComponent;
  let fixture: ComponentFixture<LspTrackAssetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LspTrackAssetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LspTrackAssetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
