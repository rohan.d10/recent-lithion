import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { APIUrls } from '../constants';
import * as constants from '../constants';

@Injectable({
	providedIn: 'root'
})
export class RestService {
	driverData = APIUrls.DRIVER_LIST;
	dlData     = APIUrls.FETCH_DL_DETAILS;

	httpOptions = {
		headers: new HttpHeaders({
			'Content-Type':  'application/json',
			'Authorization': 'Bearer '+ localStorage.getItem('auth_token')
		})
	};

	constructor(private http: HttpClient) { }

	private extractData(res: Response) {
		let body = res;
		return body || { };
	}

	getDriverProfileData(): Observable<any> {
		return this.http.get(this.driverData).pipe(
			map(this.extractData));
	}


	getDLData(body): Observable<any> {
		return this.http.post(this.dlData,body,this.httpOptions).pipe(
			map(this.extractData));
	}

}
