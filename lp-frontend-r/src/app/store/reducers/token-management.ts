import * as TokenActions from '../actions/token-management';
import * as models from '../../models';

/**
 * Each reducer module must import the local `State` which it controls.
 *
 * Here the `State` contains two properties:
 * @prop [asset: AssetTable[]] Adds Asset Table
 *
 */
export interface State {
  token: models.Jwt;
}

/**
 * There is always a need of initial state to be passed onto the store.
 *
 * @prop: asset: Set of Asset Table quantities
 */
export const initialState: State = {
  token: {
    exp: '',
    iat: '',
    userType: [''],
    userName: '',
    userId: '',
    lastLoginAt: ''
  }
};

/**
 * The actual reducer function. Reducers can be thought of as the tables in the DataBase.
 * These are the functions which are responsible for maintaing, and updating the
 * `State` of the application they control.
 *
 * Here the reducer cotrols that part of the state which is shows the state of the application
 * wheather it is searching and what is it searching for.
 */
export function reducer(state = initialState, action: TokenActions.Actions): State {
  switch (action.type) {
    case TokenActions.ActionTypes.FETCH_TOKEN: {
      return state;
    }

    case TokenActions.ActionTypes.FETCH_TOKEN_SUCCESS: {
      const payloadToken = action.payload;
      return {
        ...state,
        token: {
          ...state.token,
          userType: payloadToken.userType,
          userName: payloadToken.userName
        }
      };
    }

    case TokenActions.ActionTypes.CHECK_TOKEN_SUCCESS: {
      const payloadToken = action.payload;
      return {
        ...state,
        token: {
          ...state.token,
          userType: payloadToken.userType,
          userName: payloadToken.userName
        }
      };
    }

    case TokenActions.ActionTypes.REMOVE_TOKEN: {
      return state;
    }

    case TokenActions.ActionTypes.CHECK_TOKEN: {
      return state;
    }

    case TokenActions.ActionTypes.REMOVE_TOKEN_SUCCESS: {
      return {
        ...state,
        token: {
          ...state.token,
          userType: [''],
          userName: ''
        }
      };
    }

    default: {
      return state;
    }
  }
}

/**
 * Because the data structure is defined within the reducer it is optimal to
 * locate our selector functions at this level. If store is to be thought of
 * as a database, and reducers the tables, selectors can be considered the
 * queries into said database. Remember to keep your selectors small and
 * focused so they can be combined and composed to fit each particular
 * use-case.
 */

export const getToken = (state: State) => state.token;
